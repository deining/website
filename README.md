# Website

[![Pipeline Status](https://gitlab.com/c2platform/website/badges/master/pipeline.svg)](https://gitlab.com/c2platform/website/-/commits/master)
[![Latest Release](https://gitlab.com/c2platform/website/-/badges/release.svg)](https://gitlab.com/c2platform/website/-/releases)

This project contains the sources and content for the website at https://c2platform.org. This project uses [Hugo](https://gohugo.io/) with the [Docsy Theme](https://github.com/google/docsy). This project is created as a copy of the [Docsy Example](https://github.com/google/docsy-example) project.

You can find detailed theme instructions in the [Docsy user guide](https://www.docsy.dev/docs/).

- [Pipeline](#pipeline)
- [Running the website locally](#running-the-website-locally)
- [Running a container locally](#running-a-container-locally)
- [Verify that the service is working.](#verify-that-the-service-is-working)
- [PlantUML support](#plantuml-support)
- [Validate HTML pre-commit](#validate-html-pre-commit)
- [Test on GitLab Runner](#test-on-gitlab-runner)
- [Snippets](#snippets)
- [Troubleshooting](#troubleshooting)
- [Links](#links)


## Pipeline

The CI/CD pipeline currently builds two versions of the website:

1. A development version on `development` branch will be published to https://c2platform.org/dev
2. The production version on `master` branch is published to https://c2platform.org

## Running the website locally

Building and running the site locally requires a recent `extended` version of [Hugo](https://gohugo.io).

```bash
sudo apt install snapd -y
sudo snap install hugo
```

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
nano ~/.bashrc
source ~/.bashrc
nvm install 18
git clone git@gitlab.com:c2platform/website.git
cd website
npm install
```

Once you've made your working copy of the site repo, from the repo root folder, run:

```bash
hugo server
```

**Note**: the `hugo` snap alone is not enough. Without `npm install` you will get an error message:

> Error: error building site: POSTCSS: failed to transform "css/search.css" (text/css). Check your PostCSS installation; install with "npm install postcss-cli". See https://gohugo.io/hugo-pipes/postcss/: this feature is not available in your current Hugo version, see https://goo.gl/YMrWcn for more information

You can find out more about how to install Hugo for your environment in our
[Getting started](https://www.docsy.dev/docs/getting-started/#prerequisites-and-installation) guide.

## Running a container locally

Alternatively you can run this website inside a [Docker](https://docs.docker.com/) container, the container runs with a volume bound to the `docsy-example`
folder. This approach doesn't require you to install any dependencies other than `podman`.

```bash
sudo apt install podman -y
```

After that, you can run

```bash
podman run --rm -it   -v $(pwd):/src   -p 1313:1313   docker.io/klakegg/hugo:ext-ubuntu   server
```

For more information see the [Docker Compose documentation](https://docs.docker.com/compose/gettingstarted/).
To stop the container, on your terminal window, press **Ctrl + C**.

## Verify that the service is working.

Open your web browser and type `http://localhost:1313` in your navigation bar. You can now make changes to the website content and those changes will immediately show up in your browser after you save.

## PlantUML support

PlantUML diagrams are supported in website by adding [Hugo Shortcode](https://gohugo.io/templates/shortcode-templates/) file [layouts/shortcodes/plantuml.html](./layouts/shortcodes/plantuml.html) and Javascript library [static/js/rawdeflate.js](./static/js/rawdeflate.js) downloaded from [johan/js-deflate](https://github.com/johan/js-deflate).

```
{{< plantuml id="eg" >}}
Alice -> Bob: test
{{< /plantuml >}}
```

## Validate HTML pre-commit

```bash
#!/bin/sh
rm -rf public || true
hugo
cd public
find . -name "*.html" -type f -exec sed -i -E 's/(<[^>]* id="plantuml-[^"]*")(.*?>)/\1 data-proofer-ignore\2/g' {} \;
find . -name "*.html" -type f -exec sed -i 's/<a class="td-offset-anchor"><\/a>/<a class="td-offset-anchor" data-proofer-ignore><\/a>/g' {} \;
find . -name "*.html" -type f -exec sed -i 's/src="https:\/\/vsmarketplacebadge.apphb.com/data-proofer-ignore src="https:\/\/vsmarketplacebadge.apphb.com/g' {} \;
htmlproofer --allow-missing-href --ignore-empty-alt --ignore-missing-alt --no-enforce-https --ignore-urls https://vsmarketplacebadge.apphb.com/*  2>&1 | tee ../htmlproofer.log
if grep -q "following failures were found" ../htmlproofer.log; then
    echo "Failures found. Exiting."
    exit 1
fi
```

## Test on GitLab Runner

```bash
export IMAGE_HTML_PROOFER=registry.gitlab.com/c2platform/docker/htmlproofer:5.0.7
docker run --rm -i -v $(pwd):/src ${IMAGE_HTMLPROOFER} --allow-missing-href --ignore-empty-alt --ignore-missing-alt --no-enforce-https 2>&1 | tee ../htmlproofer.log
```

## Snippets

Via `Ctrl+Space` access markdown snippets like `relref`, and `external-link`. See [markdown.code-snippets](./.vscode/markdown.code-snippets)

## Troubleshooting

See [Docsy Example](https://github.com/google/docsy-example) project.

## Links

* [Pipelines](https://gitlab.com/c2platform/website/-/pipelines)
* [Docsy user guide](https://docsy.dev/docs)
* [Docsy](https://github.com/google/docsy)
* [example.docsy.dev](https://example.docsy.dev)
* [Hugo theme module](https://gohugo.io/hugo-modules/use-modules/#use-a-module-for-a-theme)
