---
title: "GitLab Projects"
linkTitle: "GitLab"
weight: 15
description: >
  Deze sectie geeft een overzicht van C2 Platform GitLab-projecten die onderdeel zijn van het
  {{< external-link
  url="https://about.gitlab.com/solutions/open-source/join/" text="GitLab Open Source Program"
  htmlproofer_ignore="false" >}}.
  Deze projecten profiteren daarmee van de volledige mogelijkheden van
  [GitLab Ultimate]({{< relref path="/docs/concepts/gitlab" >}})
---

{{% pageinfo %}}
De technische documentatie van het platform is in het Engels. Voor een compleet overzicht van beschikbare documentatie is de Engelstalige versie meer geschikt.
<p><a class="btn btn-lg btn-primary mr-3 mb-4" href="{{< relref path="/docs/gitlab" lang="en" >}}">
  Documentation <i class="fas fa-arrow-alt-circle-right ml-2"></i>
</a></p>
{{% /pageinfo %}}

