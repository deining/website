---
title: "Instructies"
linkTitle: "Instructies"
weight: 3
description: >
    Stap-voor-stap instructies voor het uitvoeren van een bepaalde taak of activiteit.
---

{{% pageinfo %}}
De technische documentatie van het platform is in het Engels. Voor een compleet overzicht van beschikbare documentatie is de Engelstalige versie meer geschikt.
<p><a class="btn btn-lg btn-primary mr-3 mb-4" href="../../en/docs/howto/">
  Documentation <i class="fas fa-arrow-alt-circle-right ml-2"></i>
</a></p>
{{% /pageinfo %}}
