---
title: "Documentatie"
linkTitle: "Documentatie"
weight: 20
menu:
  main:
    weight: 20
---

{{% pageinfo %}}
De technische documentatie van het platform is in het Engels. Voor een compleet overzicht van beschikbare documentatie is de Engelstalige versie meer geschikt.
<p><a class="btn btn-lg btn-primary mr-3 mb-4" href="{{< relref path="/docs/" lang="en" >}}">
  Documentation <i class="fas fa-arrow-alt-circle-right ml-2"></i>
</a></p>
{{% /pageinfo %}}




