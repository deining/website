---
title: "Richtlijnen"
linkTitle: "Richtlijnen"
weight: 5
description: >
  Instructies en aanbevelingen die begeleiding en richting bieden om een gewenst resultaat te behalen of om bepaalde normen of principes te volgen.
---

{{% pageinfo %}}
De technische documentatie van het platform is in het Engels. Voor een compleet overzicht van beschikbare documentatie is de Engelstalige versie meer geschikt.
<p><a class="btn btn-lg btn-primary mr-3 mb-4" href="../../en/docs/guidelines/">
  Documentation <i class="fas fa-arrow-alt-circle-right ml-2"></i>
</a></p>
{{% /pageinfo %}}