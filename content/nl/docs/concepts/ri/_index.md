---
categories: ["Concept"]
tags: []
title: "Referentie Implementaties"
linkTitle: "Referentie Implementaties"
weight: 3
description: >
  Ontgrendel de kracht van de ontwikkelomgeving en creëer volledig geïntegreerde,
  allesomvattende systemen. Met slechts een paar eenvoudige commando's kunt u
  bijvoorbeeld de automatisering verkennen die de implementatie en het beheer van
  een GIS-platform bij Rijkswaterstaat (RWS) aanstuurt.
---

In het domein van systeemtechniek en infrastructuur-als-code (IaC) gaat de term
"referentie-implementatie" verder dan de status van een eenvoudige blauwdruk.
Het evolueert tot een volledig functioneel, naadloos geïntegreerd systeem. Dit
voorbeeld dient niet alleen als model, maar ook als gestandaardiseerd, goed
gedocumenteerd voorbeeld, waarin wordt gedemonstreerd hoe een specifiek systeem
efficiënt kan worden geïmplementeerd en beheerd, met
[Ansible]({{< relref path="/docs/concepts/ansible" >}}) in de hoofdrol.

Ansible, een spil in elke
[automatiserings]({{< relref path="/docs/concepts/automation" >}})
initiatief, speelt een cruciale rol in deze context.


Binnen het kader van het C2-platform verbindt een referentie-implementatie twee
essentiële componenten: het
[Ansible Inventory Project]({{< relref path="/docs/concepts/ansible/projects/inventory" lang="en">}})
en het
[Vagrant Project]({{< relref path="/docs/concepts/dev/vagrant" >}}).
De synergie van Vagrant en Ansible is bijzonder krachtig en biedt een
allesomvattende oplossing. Vagrant regisseert de creatie van virtuele machines,
inclusief netwerkconfiguraties en schijftoewijzingen, terwijl Ansible
verantwoordelijk is voor het installeren en beheren van diverse knooppunten en
virtuele machines. In het geval van het GIS-platform omvat dit bijvoorbeeld de
implementatie van componenten zoals ArcGIS Enterprise Web Adaptor, Portal,
Server, Datastore, FME en meer.

Zoals afgebeeld in het onderstaande diagram, kan de C2-platformingenieur
moeiteloos complete omgevingen opbouwen via Vagrant, gebruikmakend van diverse
cloudservices:

1. [Vagrant Cloud]({{< relref path="/docs/concepts/dev/vagrant/vagrant-cloud" >}})
   herbergt C2 images die compatibel zijn met LXD, VirtualBox en meer.
1. [GitLab.com]({{< relref path="/docs/concepts/gitlab" >}})
   dient als het repository voor alle C2-projecten en biedt tal van middelen.
1. [Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}})
   fungeert als het repository voor open-source automatiseringsrollen en
   collecties, inclusief die ontworpen zijn voor het C2-platform in het domein
   van GIS.

Dit alles is mogelijk dankzij een
["open, tenzij"]({{< relref path="/docs/concepts/oss" >}}) benadering.
Deze benadering staat de verkenning en het testen van het GIS-platform toe,
waarbij het de implementatie in het RWS Data Center weerspiegelt, door gebruik
te maken van het open-source tegenhanger referentie-implementatie.

In de context van een open-source Ansible-project staat een
referentie-implementatie als een voornaam voorbeeldproject, dat inzicht biedt in
hoe
[Ansible-collecties]({{< relref path="/docs/concepts/ansible/projects/collections" >}}) en
[Ansible-rollen]({{< relref path="/docs/concepts/ansible/projects/roles" >}})
zijn geconfigureerd binnen een
[Ansible Inventory Project]({{< relref path="/docs/concepts/ansible/projects/inventory" lang="en">}}),
vaak aangeduid als een playbook-project. Deze configuratie rust gebruikers uit
om een volledig functioneel systeem te implementeren. De
referentie-implementatie dient niet alleen als een abstracte referentie, maar
als een praktisch raamwerk, openlijk toegankelijk voor soortgelijke
implementaties door klanten en organisaties.

Laten we ingaan op de kernattributen van dit concept:

1. **Uitgebreide Implementatie:** Een referentie-implementatie omvat de
   volledigheid van een functioneel en geïntegreerd systeem. Het behandelt
   zorgvuldig alle configuraties, instellingen en onderlinge afhankelijkheden
   die nodig zijn voor naadloze werking. Deze volledigheid verzekert dat de
   referentie-implementatie het beoogde systeem volledig weerspiegelt.
1. **Pariteit met Productie:** Bijvoorbeeld, de Referentie-implementatie van het
   GIS-platform dient als de open-source tegenhanger van het GIS-platform binnen
   Rijkswaterstaat (RWS). Dit impliceert dat het de productieomgeving nauwkeurig
   nabootst en zorgt voor afstemming met real-world opstellingen.
1. **Gedetailleerde Documentatie en Beste Praktijken:** Net als conventionele
   referentie-implementaties beschikt een complete referentie-implementatie over
   uitgebreide documentatie. Deze documentatie verduidelijkt het doel van het
   systeem, de configuraties, beste praktijken en beheerrichtlijnen. Het dient
   als een onschatbare bron voor teamleden die betrokken zijn bij de
   implementatie en het onderhoud van het systeem.
1. **Training en Consistentie:** De referentie-implementatie dient ook als een
   waardevolle bron voor training en leren, waarbij teamleden worden geholpen om
   te begrijpen hoe het hele systeem efficiënt kan worden geïmplementeerd en
   beheerd. Bovendien zorgt het voor consistentie binnen uw infrastructuur,
   waardoor het risico op fouten of verkeerde configuraties wordt verminderd.

In de context van Ansible en Vagrant kan een uitgebreide
referentie-implementatie Ansible-playbooks bevatten voor systeemconfiguratie en
beheer, samen met Vagrant-bestanden die de VM-infrastructuur beschrijven. Deze
bronnen zijn grondig gedocumenteerd en rigoureus getest om ervoor te zorgen dat
ze het beoogde systeem nauwkeurig vertegenwoordigen.

Samengevat, een referentie-implementatie is geen eenvoudige schets, maar een
volledig functioneel, volledig geïntegreerd systeem. Het garandeert efficiënt en
consistent beheer van infrastructuurcomponenten terwijl het in lijn is met de
realiteit van productieomgevingen. Dit is met name belangrijk in situaties zoals
de Referentie-implementatie van het GIS-platform, waar het tot doel heeft een
bestaand systeem binnen organisaties zoals Rijkswaterstaat (RWS) te repliceren.

Het doel van de referentie-implementatie strekt zich uit tot het verduidelijken
van hoe software kan worden toegepast en geconfigureerd, en dient als een
lanceerplatform voor het aanpassen en configureren van software om aan
specifieke eisen te voldoen. Door gebruik te maken van open-source bouwstenen,
zoals Ansible-rollen, kunnen organisaties het werk van anderen benutten,
waardoor de ontwikkeling en implementatie van software wordt versneld. Dit
bevordert uniformiteit in implementaties en stimuleert samenwerkingsinspanningen
tussen organisaties die dezelfde software gebruiken.

Een voorbeeld van een referentie-implementatie is het
[`ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis/" lang="en" >}})
configuratie- en playbook-project voor het Rijkswaterstaat GIS-platform. Dit
project vergemakkelijkt de lokale implementatie van een uitgebreide, functionele
omgeving voor het Rijkswaterstaat GIS-platform. Om dit te bereiken maakt het
project gebruik van Ansible-collecties zoals
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis/" lang="en" >}}) en
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore/" lang="en" >}}).

{{< plantuml id="vagrant-ansible-gsd" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
!include <cloudogu/tools/ansible>
!include <logos/kubernetes>
!include <cloudinsight/docker>
!include <tupadr3/devicons/github>
!include <office/Devices/device_laptop>
!include <tupadr3/font-awesome/users>
!include <tupadr3/devicons2/vagrant>
!include <tupadr3/devicons2/git>
!include <logos/gitlab>

sprite $arcgis [100x100/8] {
000000000000000000000000000000000000000124577FFNNNLLDC4200000000000000000000000000000000000000000000
000000000000000000000000000000000257Nd_ywvumWGG80000000000000000000000000000000000000000000000000000
000000000000000000000000000036Nl-xveO800000012345566666655432210000000000000000000000000000000000000
00000000000000000000000026V__xeG000000001d__sgWWWWeemuuvvwyz____lVF742000000000000000000000000000000
00000000000000000000027d_xmQ_I00000000001q____VEDCCCC554321108Oeuwy____dF520000000000000000000000000
00000000000000000005d_zmG00H_R0000134CSaaaiz-___tkbaaaaaaaaaaKC43100GWvx___tN51000000000000000000000
00000000000000001Ft_xW000009_T24Kaaaaaaaaaaaiqz___tkbaaaaaaaaaaaaaSC41008Ovz___d62000000000000000000
000000000000002N__wG00000014__aaaaaaaaaaaaaaaaaiz___tcaaaaaaaaaaaaaaaaUE3000Gmy___V51000000000000000
0000000000001F__wG0000014Saas_iaaaaaaaaaaaaaabbbcl____tltttlllllllldccj__lE2000Gv-__tF20000000000000
000000000005t_xO000002Caaaaaj_jaaaaZaabcddlt_____________zzzzzzzz----_______dF6310ez___N200000000000
0000000001V_-W000002KaaaaZZZa_sSTUdl_____-zyyyqiiiaaar___lbaaaaaaaZRRRRRZZp-__--_lNEj____E0000000000
000000004t_w800002KaaZZRRRSTd______zyxpZZRRRZZaaaaaaaaiz__tjaaaaZRRRRRRRRRRZy_tTZhnwz_____l300000000
0000000E__i00000BaaZRRSTdl_____zpZRRRRRRRRRRRRRRZaaaaaaar___kaZQJRRRRRRRRRRRRh-_dSR208Ww____D0000000
000000V___b0002SaZRSUl____yxhi_tRRRRRRRRRRRRRRRRRRRZaaaaai___kJIRRRRRRRRRRRRRRZz_tSRB000Gy___M000000
00000V_-__s003aaTdl___zxhRRRRZ__aRQQQQRRRRRRRRRRRRRRZZaaZZhz__cRRRRRRRRRRRRRRRRRq__bRR1000n___V00000
0000V_zG-__1Badt___ypZRRRRRRRQq_tJIIIIIIIIQQRRRRRRRRRQQQIIIQz__cRRRRRRRRRRRRRRRRRq__bRS1000X___V0000
000D_-G0r__Ul___yhZRRRRRRRRRIIR__SIIIIIIIIIIIQRRRRRQIIIIIIIIR-__cRRRRRRRRRRRRRRRRRi__bSS1000X___M000
001__P00a____zpZRRRRRRRRRRQIIIIq_tJIIIIIIIIIIIIQRRIIIIJJJKKTTc___dUUUTTSSSRRRRRRRRRi__baS1000p___200
00c_q01E___-hRRRRRRRRRRRRIIIIIIQ__cIHHHIIIIIIIJKLVVdll_____________________tlddVUTSSq_taaK0008-__t00
01__Q3l_-___SRRRRRRRRRRQIIIIIIIHq__J9999IKMVdt______zyyxxpphhZZq___aZZhhppxyyzz____tl__laaA000Q___B0
0T__V-vOI___bRRRRRRRRRQIIIIIIIH9Q__lCNVt____-yxwgYRRRRRRRRRRRRRRz__lRRRRRRRRRRRRZhiyy-___ld2000z__l0
0t_r8000Rr__tRRRRRRRRRIIIIIIIIABEt_____-xwXQIIIIIJRRRRRRRRRRRRRRZ-__bRRRRRRRRRRRRSaaaai__qyyh90Y___1
1__i0001ai___aRRRRRRRRIIIIIJMVt______hH999IIIIIIIRRRRRRRRRRRRRRRRi___SRRRRRRRRRRaaaaaaa-_kaR0009___A
A__a0009aa-__lSRRRRRRRRJKVl____ywXh__lA9AIIIIIIIIRRRRRRRRRRRRRRRRRr__lRRRRRRRRSaaaaaaaai__aa0000___J
J__a000Iaai___bRRRRRRTdt___zxYIIIAIq__UIIIIIIIIIIRRRRRRRRRRRRRRRRRZ-__bRRRRRSaaaaaaaaaaa-_ja0000s__R
R__j000Iaaaz__taRSUd____zpYIIIIIIIIQ-__SIIIIIIIIRRRRRRRRRRRRRRRRRRRi__tSRRSaaaaaaaaaaaaaj_ka0000s__R
Q___000Iaaai___ll____zpRRRJIIIIIIIIIZ__tJIIIIJJRRRRRRRRRSSSSSRRRRRRRz__jSaaaaaaaaaaaaaaaj_tj0000s__I
H___900Haaaas_____-qZRRRRRRRJJIIIIIIIq__lJIJRRRRRRRRRRRaaaaaaabSSSSSb___aaaaaaaaaaaaaaaaa__i0000___8
8___J008bcl______jaSRRRRRRRRRRRRJJJJJRz__cRRRRRRRRRSTTddlltt______________ttllddcbbaaaaaa-_Z0009__-0
0-__c00C____-z___taaSSRRRRRRRRRRRRRRRRZ-__cRRSTUdlt_________--zzzzyyyy___zzz-_______tldcbs_H000K__Z0
0Y___A0f--ziaar___laSRRRRRRRRRRRRRRRRRRZ___dl______-zyyqiiiaaaaaaaaaaar__kaaaaaaiqqyz-_____E201t__80
00___U000Yaaaaar___kaaSRRRRRRRRRRRRRRSUd______-yqiaaaaaaaaaaaaaaaaaaaai___aaaaaaaaaaaaiiq-_yv8K__h00
00X___A008aaaaaaz___caaaSSRRRRRRRSTdt________kaaaaaaaaaaaaaaaaaaaaaaaaas__jaaaaaaaaaaaaaar_901t_-800
000y__l100Gaaaaaa-___caaaaaSSRSUd____-ypZRi___caaaaaaaaaaaaaaaaaaaaaaaaj__taaaaaaaaaaaaaZZ_90T__O000
0008-__U000Paaaaaaz___caaaaacl____zqZRRRRRRi___lbaaaaaaaaaaaaaaaaaaaaaaa___aaaaaaaaaaaaZ8R_0B__g0000
0000G___M000Paaaaaaz___labd____zqaaaSSSSSRRSi-__tbaaaaaaaaaaaaaaaaaaaaaas__jaaaaaaaaaaZ80R-At_o00000
00000G___M000Gaaaaaaq_______-qaaaaaaaaaaaaaaai-__tbaaaaaaaaaaaaaaaaaaaaaj__jaaaaaaaaaY800Rtl_w000000
000000Gy__V1008Yaaaabt_____iaaaaaaaaaaaaaaaaaaiz___kaaaaaaaaaaaaaaaaaaaaj__saaaaaaaaX0000a__o0000000
00000008w__l3000Oabl___--__tcaaaaaaaaaaaaaaaaaaar___lbaaaaaabbbbcdldlllll__tlddcbbZG00004__f00000000
000000000W___N100B__-ziaiz___tbaaaaaaaaaaaaaaaaaai-__tbbcllt_________----___----___dNF5N_-O000000000
00000000008w__l5000GXaaaaaiz___lbaaaaaaaaaaaaaaaaaj________-zzzqqiiiaaaaa___aaZW0088Gj__v00000000000
000000000000Oy__d40008Xaaaaaiz___lbaaaaaaaaaaabclt_______jiaaaaaaaaaaaaaa__rYO000004d_xO000000000000
00000000000000Wy__l51000OYaaaaiz___lbaaaaaabcl____zqii-__tbaaaaaaaaaaaaaat_Z000015l_yW00000000000000
0000000000000000Wx__tF30000OXaaaiz___lbaabl___-ziiaaaaiz___kbaaaaaaaaaZWGs_R003N__xW0000000000000000
000000000000000000Ow___lF30000GWXZiz-__tt__-ziaaaaaaaaaar___lbaaaaZXO8000__UFl__wO000000000000000000
000000000000000000008ew___tN5200008GXt____tbaaaaaaaaaaaaaiz___jOG0000013F____xe800000000000000000000
000000000000000000000008Wv____lN742Dt__new__laXWWWWWWWWOG88Wz__d3025Fd____wW800000000000000000000000
000000000000000000000000000Guvz_______V543AYy_tF300000111234D________-wuG000000000000000000000000000
00000000000000000000000000000000Guuwz___________________________yvuO80000000000000000000000000000000
000000000000000000000000000000000000000GOWmuuvvvwwwxwwwvvuuuWG00000000000000000000000000000000000000
}

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtual-desktop")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddContainerTag("vagrant_container", $sprite="vagrant", $legendText="vagrant")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-awx", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
Person(engineer, "Engineer")
AddSystemTag("gis", $sprite="arcgis*.5", $legendText="gis")
AddExternalSystemTag("gitlab", $sprite="gitlab", $legendText="gitlab")
AddExternalSystemTag("ansible", $sprite="ansible", $legendText="ansible-awx")
AddExternalSystemTag("vagrant", $sprite="vagrant", $legendText="vagrant cloud")
AddExternalContainerTag("gis_inventory", $sprite="arcgis*.4", $legendText="gis")

Person(engineer, "Engineer")

Boundary(laptop, "high-end developer laptop", $type="ubuntu-22") {
  System(gsd, "GIS Platform", "", "", $tags="gis") {
  }
  Container(vagrant, "Vagrant", "", $tags="vagrant_container")
  Container(ansible_local, "Ansible", "", $tags="ansible_container")
}

System_Ext(galaxy_website, "Ansible Galaxy\ngalaxy.ansible.com", "Stores all open source Ansible automation \n( collections, roles )", $tags="ansible")
System_Ext(gitlab, "GitLab Ultimate\ngitlab.com", "Stores all C2 Platform Git\nrepositories and projects", $tags="gitlab") {
  Container_Ext(gis_inventory, "GIS Platform Referentie\nImplementatie", "", $tags="gis_inventory")
}
System_Ext(vagrant_cloud, "Vagrant Cloud\napp.vagrantup.com", "Stores all C2 VM images", $tags="vagrant")

Rel(ansible_local, gsd, "Provision ArcGIS Portal, WebAdaptor, Server, Datastore, FME, etc", "")
Rel(vagrant, gsd, "Create VM's\nnetwork, disks etc", "")
Rel(engineer, vagrant, "Create VM's, provision services using Vagrant CLI", "")
Rel_D(vagrant, ansible_local, "", "")
Rel_R(ansible_local, galaxy_website, "Download GIS automation / collection", "")
Rel(ansible_local, gitlab, "", "")
Rel_R(vagrant, vagrant_cloud, "Download Vagrant \nboxes ( images )", "")
{{< /plantuml >}}
