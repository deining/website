---
categories: ["Concept"]
tags: [ansible, galaxy]
title: "Ansible Collections"
linkTitle: "Ansible Collections"
draft: false
weight: 3
description: >
  Verzameling van modules, plugins en rollen die functionaliteit toevoegen en de Ansible automatiseringstaal uitbreiden.
---

<!-- TODO remove all draft pages -->

Een Ansible Collection is een verzameling van modules, plugins en rollen die functionaliteit toevoegen en de automatiseringstaal uitbreiden voor het beheer van infrastructuren. Het is een distributieformaat voor Ansible-content. Deze collections worden gedeeld via de [Galaxy]({{< relref path="../../galaxy.md" >}}) website.

* [C2 Platform Collections]({{< ref path="/tags/collection/" lang="en" >}})
*  {{< external-link url="https://galaxy.ansible.com/c2platform" text="C2 Platform Collections op Galaxy website" htmlproofer_ignore="true" >}}
* {{< external-link url="https://docs.ansible.com/ansible/latest/dev_guide/developing_collections.html" text="Developing collections - Ansible Documentation" htmlproofer_ignore="true" >}}
