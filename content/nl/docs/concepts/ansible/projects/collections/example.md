---
title: "Voorbeeld Ansible role"
linkTitle: "Voorbeeld Ansible role"
categories: ["Voorbeeld"]
tags: [ansible, role, collection, java]
weight: 6
description: >
   Een voorbeeld van een eenvoudige Ansible role in een Ansible collection.
---

De Ansible rol {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/tree/master/roles/java" text="c2platform.core.java" htmlproofer_ignore="false" >}} is een voorbeeld van een eenvoudige rol voor het installeren van Java op Linux systemen.

De rol is onderdeel van de Ansible collection {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/" text="c2platform.core" htmlproofer_ignore="false" >}}. Een dergelijke collectie wordt standaard geleverd met een {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/pipelines" text="CI/CD Pipeline" htmlproofer_ignore="false" >}}. Er is een Engelstalige  "tutorial" op de website met meer informatie over [CI/CD Pipelines]({{< relref path="/docs/tutorials/git-workflow/6-cicd/gitcicd/collections/" lang="en" >}}).
