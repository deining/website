---
categories: ["Concept"]
tags: [ansible, galaxy, ansible-galaxy, collection, role]
title: "Ansible Roles"
linkTitle: "Ansible Roles"
draft: false
weight: 4
description: >
    Gestructureerde en herbruikbare verzameling van taken, variabelen en configuraties die specifieke functionaliteit.
---

Een Ansible-role is een gestructureerde en herbruikbare eenheid binnen het Ansible-framework. Het groepeert en beheert taken, variabelen en configuraties op een georganiseerde manier, waardoor complexe systeemconfiguraties eenvoudiger worden. Een typische role-structuur bevat mappen zoals "tasks" voor taken, "defaults" voor standaardwaarden van variabelen, en optioneel andere mappen zoals "handlers" en "templates". Met roles kunnen systeembeheerders en ontwikkelaars configuraties consistent en herhaalbaar maken, fouten verminderen en het beheer van complexe systemen vergemakkelijken.

Roles kunnen worden gecombineerd en geïntegreerd in playbooks om complexe systeemconfiguraties op te bouwen. Hierdoor kunnen configuraties gemakkelijk worden onderhouden en uitgebreid naarmate de systeemvereisten veranderen. Het gebruik van roles bevordert herbruikbaarheid, consistentie en schaalbaarheid van configuraties, waardoor systeembeheerders en ontwikkelaars efficiënter kunnen werken en de automatisering van het configuratiebeheer kunnen verbeteren.

{{< alert title="Note:" >}}Tegenwoordig worden rollen vaker via [Ansible Collections]({{< relref path="./collections" >}}) gedistribueerd. Collections zijn een latere toevoeging aan Ansible en zijn nuttig omdat we vaak veel kleinere rollen produceren. Zonder collections zouden we voor elke individuele rol een apart Git-repository, pipeline, enzovoort moeten ontwikkelen.{{< /alert >}}
