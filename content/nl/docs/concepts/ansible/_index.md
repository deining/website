---
categories: ["Concept"]
tags: [ansible, concept, aap]
title: "Ansible Automation Platform (AAP)"
linkTitle: "Ansible"
weight: 5
description: >
  Uitgebreide open source oplossing voor IT-automatisering.
---

Ansible Automation Platform ( AAP ) is een uitgebreide oplossing voor IT-automatisering, orchestratie en governance, waarmee efficiënt beheer en schaalvergroting van infrastructuur mogelijk wordt gemaakt.

{{% pageinfo %}}
Meer informatie is in beschikbaar in de Engelstalige versie van deze website
<p><a class="btn btn-lg btn-primary mr-3 mb-4" href="{{< relref path="/docs/concepts/ansible" lang="en" >}}">
  Documentation <i class="fas fa-arrow-alt-circle-right ml-2"></i>
</a></p>
{{% /pageinfo %}}