---
categories: ["Concept"]
tags: [concept, automation, ansible, engineering, operating]
title: "Ontwikkelen versus configureren"
linkTitle: "Ontwikkelen versus configureren"
weight: 1
draft: false
description: >
  Het configureren van Ansible, het ontwikkelen van rollen voor Ansible en het ad-hoc gebruik van Ansible zijn aparte disciplines.
---

1. **Ansible Rol/Collectie Ontwikkelaar (Ansible Engineering)**: Deze rol omvat het ontwikkelen, testen en publiceren van Ansible rollen en collecties. Het vereist softwareontwikkelingsvaardigheden en kennis van de structuur, syntaxis en beste praktijken van Ansible rollen en collecties. Ansible rollen en collecties zijn herbruikbare eenheden van automatisering die gedeeld kunnen worden met de gemeenschap via platforms zoals Ansible Galaxy. Ontwikkelaars in deze discipline richten zich op het creëren en onderhouden van hoogwaardige rollen en collecties die specifieke functionaliteiten of configuraties bieden voor verschillende systemen.
2. **Ansible Operator (Ansible Gebruiker)**: Deze rol houdt in dat Ansible wordt gebruikt om playbook-projecten te maken en te beheren, inclusief plays, inventaris, group_vars, host_vars en afhankelijkheden zoals collecties en rollen. Ansible Operators zijn verantwoordelijk voor het schrijven van playbooks die de gewenste staat van de systemen definiëren, taken orkestreren, configuraties beheren en verschillende operaties automatiseren. Ze kunnen ook Ansible Automation Platform job templates en workflows configureren voor complexere automatiseringsworkflows. Deze discipline richt zich op het gebruik van Ansible om specifieke automatiseringsdoelen te bereiken zonder noodzakelijkerwijs de onderliggende rollen en collecties te ontwikkelen of te engineeren.
3. **Ansible Ad-hoc**: Deze discipline omvat het gebruik van Ansible ad-hoc commando's. Ad-hoc commando's stellen gebruikers in staat om snel eenmalige taken uit te voeren zonder de noodzaak van het schrijven en onderhouden van playbooks. Ad-hoc commando's worden vaak gebruikt voor het verzamelen van systeeminformatie, het uitvoeren van eenvoudige commando's of het uitvoeren van ad-hoc bewerkingen op externe systemen. Deze discipline vereist een goede kennis van de syntaxis van Ansible commando's, de beschikbare modules en hun opties, evenals de configuratie van de doelsystemen.

Het is belangrijk om op te merken dat deze rollen niet strikt gedefinieerd zijn en dat individuen vaak overlappende vaardigheden en verantwoordelijkheden hebben. De Ansible-gemeenschap erkent echter het onderscheid tussen degenen die zich voornamelijk richten op het ontwikkelen en publiceren van rollen/collecties en degenen die zich voornamelijk bezighouden met het automatiseren van taken en het beheren van infrastructuur.
