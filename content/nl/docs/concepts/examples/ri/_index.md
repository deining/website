---
categories: ["Concept"]
tags: []
title: "Referentie implementatie"
linkTitle: "Referentie implementatie"
weight: 6
description: >
  Functionele open source-implementaties die dienen als open referentie frameworks en sjablonen. Ze versnellen de ontwikkeling van open source bouwstenen, zoals Ansible-rollen, en vergemakkelijken tevens de ontwikkeling en uitwisseling van kennis.
---

{{< under_construction_nl >}}

<!-- TODO finish -->

Een referentie-implementatie in de context van een open source Ansible-project is een voorbeeldproject dat laat zien hoe [Ansible Collections]({{< relref path="../../ansible/projects/collections/" >}}) en [Ansible Roles]({{< relref path="../../ansible/projects/roles/" >}}) worden geconfigureerd in een Ansible-configuratieproject, ook wel bekend als een playbook-project. Deze configuratie stelt gebruikers in staat om een volledig werkend systeem uit te rollen. De referentie-implementatie fungeert als een functionerende versie van dit systeem en dient als een open referentiekader voor vergelijkbare implementaties bij klanten of organisaties.

Het doel van de referentie-implementatie is om anderen te laten zien hoe de software kan worden gebruikt en geconfigureerd, en dient als startpunt voor het aanpassen en configureren van de software op basis van specifieke behoeften. Door gebruik te maken van open source bouwblokken, zoals Ansible-rollen, kunnen organisaties het werk van anderen hergebruiken en versnellen ze de ontwikkeling en implementatie van software. Dit bevordert ook consistentie in implementaties en stimuleert betere samenwerking tussen verschillende organisaties die dezelfde software gebruiken.

Een voorbeeld van een referentie implementatie is het [ansible-gis]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis/" lang="en" >}}) configuratie-/playbookproject van het GIS Platform van Rijkswaterstaat. Met behulp van dit project kan een complete en functionele omgeving van het Rijkswaterstaat GIS Platform lokaal worden uitgerold. Voor dit doel maakt het project gebruik van de Ansible-collecties [c2platform.gis]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis/" lang="en" >}}) en [c2platform.wincore]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore/" lang="en" >}}).
