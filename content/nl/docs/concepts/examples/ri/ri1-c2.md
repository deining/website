---
categories: ["Referentie implementaties"]
tags: [c2, ri, ri1]
title: "C2"
linkTitle: "C2"
weight: 1
description: >
  C2 Platform zelf
---

## Instructies

* [Set Up a Development Environment on Ubuntu 22]({{< relref path="/docs/howto/dev-environment" lang="en" >}})
