---
categories: ["Concept"]
tags: [lxd, vagrant, image, box]
title: LXD
linkTitle: Linux Containers ( LXD )
weight: 4
description: >
    Lichtgewicht en snellere virtualisatie van VM's - VM's met de voordelen van Docker containers.
---

Zie [Vagrant Cloud]({{< relref path="../vagrant/vagrant-cloud.md" >}}) voor beschikbare images / boxes.
