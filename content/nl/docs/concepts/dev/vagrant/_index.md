---
categories: ["Concept"]
tags: [vagrant, ansible]
title: Vagrant
linkTitle: Vagrant
weight: 2
description: >
  Vagrant biedt een gebruiksvriendelijk platform voor het creëren en beheren van
  eenvoudige lokale ontwikkelomgevingen, waardoor het installatieproces wordt
  gestroomlijnd.
---

Vagrant maakt de creatie en configuratie van lichte, reproduceerbare en
draagbare ontwikkelomgevingen mogelijk. Met Vagrant kunnen we effectief en
productief productie-omgevingen nabouwen met hetzelfde besturingssysteem,
pakketten, gebruikers en configuraties.

* {{< external-link url="https://www.vagrantup.com/" text="Vagrant" htmlproofer_ignore="true" >}}
* Zie [C2 voorbeeld project]({{< relref path="/docs/gitlab/c2platform/ansible/" lang="en" >}}) voor een voorbeeld van aan Vagrant project met [LXD]({{< relref "../lxd" >}} "Concept: Linux Containers (LXD)"), [VirtualBox]({{< relref "../virtualbox" >}} "Concept: VirtualBox") en [Ansible]({{< relref "../../ansible" >}} "Concept: Ansible Automation Platform").
* {{< external-link url="https://docs.ansible.com/ansible/latest/scenario_guides/guide_vagrant.html" text="Vagrant Guide - Ansible Documentation" htmlproofer_ignore="true" >}}
