---
categories: ["Concept"]
tags: [vagrant, image]
title: Vagrant Cloud
linkTitle: Vagrant Cloud
weight: 2
description: >
  Vagrant images voor LXD, VirtualBox.
---

C2 Platform-images worden gepubliceerd en gedistribueerd via de {{< external-link url="https://app.vagrantup.com/c2platform/" text="Vagrant Cloud" htmlproofer_ignore="true" >}}.
