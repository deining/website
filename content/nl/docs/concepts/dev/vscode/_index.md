---
categories: ["Concept"]
tags: [laptop, ubuntu, ide]
title: Visual Studio Code
linkTitle: VS Code
weight: 3
description: >
  Syntax highlighting, linting, debugging, integratie met Git en een groot aantal
  extensies voor het vereenvoudigen van taken zoals het schrijven van playbooks en
  het beheren van "inventory".
---

{{< under_construction_nl >}}
