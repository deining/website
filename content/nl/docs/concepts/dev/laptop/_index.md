---
categories: ["Concept"]
tags: [laptop, ubuntu]
title: High-end Developer Laptop
linkTitle: Laptop
weight: 1
description: >
  Een geavanceerde ontwikkelaarslaptop met Ubuntu 22.04 als besturingssysteem
  vormt de basis voor de lokale ontwikkelomgeving. De mogelijkheden ervan stellen
  engineers in staat om veeleisende taken efficiënt aan te pakken.
---

De [open aanpak]({{< relref "../../oss" >}} "Concept: ODM / OSS") en het geavanceerde en uitgebreide [softwareontwikkelingsplatform]({{< relref "../../gitlab" >}} "Concept: GitLab Ultimate") worden gecombineerd met een lokale ontwikkelomgeving die draait op een krachtige developer-laptop met Ubuntu 22. Dit wordt meestal gerealiseerd door het gebruik van een extra laptop naast de officiële bedrijfslaptop. Een Dell Precision 7670 met minimaal 32 GB intern geheugen is bijvoorbeeld zeer geschikt voor het nabootsen van complete omgevingen.

Een officiële bedrijfslaptop is zelden geschikt, omdat deze meestal op MS Windows draait en vaak streng beveiligd is, wat het gebruik van Ansible-engineering bemoeilijkt.

Natuurlijk kunnen ook andere Linux-distributies worden gebruikt. De [instructies]({{< relref "../../../howto" >}} "Instructies") zijn echter altijd gebaseerd op Ubuntu 22.04. Bijvoorbeeld de [instructie voor het opzetten]({{< relref path="/docs/howto/dev-environment" lang="en" >}}
"How-to: Set Up a Development Environment on Ubuntu 22") van de laptop is gericht op Ubuntu 22.

<br/>

![Dell precision 7670](laptop-ubuntu-2.png?width=300px)
![Dell precision 7670](laptop-cgi.png?width=200px)