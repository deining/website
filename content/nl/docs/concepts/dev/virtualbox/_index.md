---
categories: ["Concept"]
tags: [laptop, ubuntu, vagrant, virtualbox]
title: VirtualBox
linkTitle: VirtualBox
weight: 5
description: >
  In gevallen waar LXD niet haalbaar is, biedt VirtualBox een betrouwbaar
  alternatief. Het levert vooraf geconfigureerde virtuele machine-images,
  inclusief opties voor Microsoft Windows doelsystemen, en zorgt voor
  compatibiliteit in diverse omgevingen.
---

[LXD]({{< relref path="../lxd/" >}}) is de standaard virtualisatietechniek voor
de ontwikkelomgeving, maar in gevallen waarin LXD niet beschikbaar is, zoals bij
MS Windows, zijn er ook VirtualBox-images beschikbaar in de
[Vagrant Cloud]({{< relref path="../vagrant/vagrant-cloud.md" >}}).
