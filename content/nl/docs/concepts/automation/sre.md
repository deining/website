---
categories: ["Concept"]
tags: [paradigm, sre, tab, devops]
title: "Site Reliability Engineering"
linkTitle: "SRE"
weight: 1
description: >
  Fundamentele transformatie van IT-servicelevering en -beheer: Technische Applicatiebeheer (TAB) wordt een software discipline.
---

Site Reliability Engineering (SRE) markeert een fundamentele transformatie in de wereld van IT-servicelevering en -beheer. Het traditionele technische applicatiebeheer, ook wel bekend als TAB, evolueert naar een software discipline. De TAB'er van de toekomst is niet alleen verantwoordelijk voor systeembeheer, maar ook een volwaardige software engineer. Dit houdt in dat ze vertrouwd zijn met een breed scala aan softwareontwikkelingstools en -praktijken.

Site Reliability Engineers (SREs) beheren niet langer uitsluitend systemen; ze
hebben zich ontwikkeld tot bedreven software-engineers, goed thuis in een scala
aan ontwikkelingstools en -praktijken. Deze transformerende reis plaatst de
[development environment]({{< relref path="/docs/concepts/dev" lang="en">}})
als een cruciaal onderdeel binnen de C2 Platform-methodologie.

Een integraal aspect van SRE omvat het de integratie van de Integrated Development Environment (IDE) in het beheerproces. Voor het C2 Platform is de IDE naar keuze
[Visual Studio Code]({{< relref path="/docs/concepts/dev/vscode" >}}).
SRE-teams maken gebruik van Continuous Integration (CI), Continuous Deployment (CD)-pipelines en grondige unit testing om de kwaliteit en betrouwbaarheid van het systeem te waarborgen.

Google, een pionier op het gebied van SRE, stelt dat een SRE-team idealiter voor de helft bestaat uit mensen met een softwareachtergrond en voor de andere helft uit mensen met een systeemengineeringachtergrond. Deze diverse samenstelling stelt het team in staat om de complexiteit van moderne IT-systemen aan te pakken met behulp van software engineering principes. Alle teamleden delen een passie voor programmeren en streven naar geautomatiseerde oplossingen.

Er is een sterke relatie tussen SRE en DevOps. Terwijl DevOps zich richt op de samenwerking tussen ontwikkeling en operations, gaat SRE een stap verder. SRE-teamleden zijn verantwoordelijk voor het ontwerpen en implementeren van softwaregedreven oplossingen die de betrouwbaarheid, schaalbaarheid en efficiëntie van IT-systemen vergroten. Een vaak geciteerde uitspraak in de DevOps-wereld is "automation is the key to DevOps", en deze geldt ook voor SRE.

Het artikel "Love DevOps? Wait until you meet SRE" van Atlassian biedt diepgaande inzichten in de concepten en voordelen van SRE, en hoe het zich verhoudt tot DevOps.

"In Conversation with Ben Treynor" is een boeiend gesprek met Ben Treynor, een van de grondleggers van Site Reliability Engineering bij Google. Hij bespreekt de oorsprong van SRE en de impact ervan op de IT-branche.

> Fundamentally, it’s what happens when you ask a software engineer to design an operations function.
> </br>{{< external-link url="https://sre.google/in-conversation/" text="\"In Conversation with Ben Treynor\"" htmlproofer_ignore="false" >}}

> Automation is the key to DevOps ... and SRE
> </br>{{< external-link url="pahttps://www.atlassian.com/incident-management/devops/sreh" text="Love DevOps? Wait until you meet SRE | Atlassian" htmlproofer_ignore="false" >}}


