---
categories: ["Referentie"]
tags: [odm, oss]
title: "“Open source werken vraagt om vertrouwen en lef”"
linkTitle: "Open source werken vereist vertrouwen en moed"
weight: 2
description: >
    Artikel door Boris van Hoytema (BZK) over het verkennen van het transformerende potentieel
    van open source in de overheid en de uitdagingen die het met zich meebrengt.
---

In dit artikel van Digitale Overheid bespreekt Boris van Hoytema, de
Kwartiermaker OSPO (Open Source Programma Bureau) bij het Ministerie van
Binnenlandse Zaken en Koninkrijksrelaties (BZK), het belang van open source in
de overheid. Hij benadrukt de rol van open source bij het vergroten van
autonomie, samenwerking en het creëren van publieke waarde, terwijl risico's
worden verminderd. Boris benadrukt dat hoewel open source wijdverbreid wordt
gebruikt, er uitdagingen zijn bij de adoptie ervan binnen de overheid, waaronder
de noodzaak van een verandering in denkwijze. Hij legt uit hoe open source
overheden kan helpen om autonomie en soevereiniteit terug te winnen en biedt
inzicht in de waarde van transparantie in de dienstverlening aan het publiek. De
rol van Boris is om deze uitdagingen aan te pakken en de adoptie van open source
binnen de overheid te bevorderen.

{{< external-link url="https://www.digitaleoverheid.nl/achtergrondartikelen/open-source-werken-vraagt-om-vertrouwen-en-lef/" text="“Open source werken vraagt om vertrouwen en lef”" htmlproofer_ignore="false" >}}
