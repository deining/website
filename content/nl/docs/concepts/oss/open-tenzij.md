---
categories: ["Referentie"]
tags: [odm, oss]
title: "Kabinetsbeleid: open, tenzij"
linkTitle: "Kabinetsbeleid"
weight: 1
description: >
    De overheid streeft ernaar om software open source te maken, tenzij dit om
    wettelijke redenen niet mogelijk is. Dit vergroot transparantie, samenwerking en
    softwareveiligheid, bevordert efficiëntie en innovatie, en vermindert
    afhankelijkheid van leveranciers.
---

Het kabinetsbeleid van de overheid, genaamd 'Open, tenzij', heeft tot doel om
software die door of voor de overheid wordt ontwikkeld, zoveel mogelijk open
source te maken. Dit betekent dat de broncode van de software voor iedereen
toegankelijk en herbruikbaar moet zijn, tenzij er wettelijke beperkingen zijn
volgens de Wet Open Overheid.

Het open source beleid van de overheid heeft verschillende voordelen. Ten eerste
vergroot het de transparantie en het vertrouwen in de overheid, omdat iedereen
inzicht kan krijgen in hoe de software werkt. Ten tweede bevordert het
samenwerking, zowel binnen de overheid als met externe partners, wat leidt tot
effectiever en efficiënter openbaar bestuur. Bovendien draagt open source bij
aan de veiligheid van software door anderen de mogelijkheid te geven om de code
te controleren op fouten en risico's.

Daarnaast stimuleert open source innovatie, verlaagt het licentiekosten, en
versnelt het ontwikkelingsprocessen. Het vergroot de flexibiliteit in het
gebruik van software en vermindert de afhankelijkheid van grote commerciële
leveranciers.

In essentie bevordert het 'Open, tenzij' beleid van de overheid de
transparantie, samenwerking, veiligheid, efficiëntie, innovatie, en
onafhankelijkheid binnen de softwareontwikkelingsprocessen van de overheid.

{{< external-link url="https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/open-source/beleid/" text="Beleid Open Source - Digitale Overheid" htmlproofer_ignore="false" >}}
