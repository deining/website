---
categories: ["Referentie"]
tags: [odm, oss]
title: "Avoiding Abandon-Ware: Getting to Grips with the Open Development Method"
linkTitle: "Avoiding Abandon-Ware"
weight: 3
description: >
    De Open Development Method: een samenwerkings- en gemeenschapsgerichte aanpak
    voor softwareontwikkeling en de mogelijke invloed ervan op onderwijs, met
    nadruk op de noodzaak van langetermijnbetrokkenheid en aanpassing aan nieuwe
    praktijken.
---

Het artikel bespreekt het concept van de Open Ontwikkelingsmethode (ODM) in de
context van open source softwareontwikkeling en de mogelijke gevolgen voor
onderwijs. Het benadrukt dat ODM zich richt op transparantie, samenwerking en
betrokkenheid van de gemeenschap, en gaat verder dan licentie- en
distributiekwesties. Het artikel benadrukt ook de uitdagingen en kansen die ODM
biedt voor onderwijs, en suggereert dat het de manier waarop kennisproducten
worden gecreëerd en gedeeld, kan revolutioneren.

{{< external-link url="http://oss-watch.ac.uk/resources/odm"
text="Avoiding Abandon-Ware: Getting to Grips with the Open Development Method"
htmlproofer_ignore="false" >}}
