---
title: "RWS GIS Platform ( 2023 t/m heden )"
linkTitle: "RWS"
weight: 6
description: >
  [![Does Not Apply](https://img.shields.io/badge/Capability-N.A.-lightgrey)](https://c2platform.org/en/docs/guidelines/project/capability/ "Guideline: Ansible Role Capability Labeling")
  Rijkswaterstaat ( RWS ) GIS Platform
---

{{< under_construction_nl >}}

|Categorie||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|    |           |
|Orchestration          |    |  |
|Code Pipelines         | ✔  | Azure DevOps  |
|Policy-As-Code         |   | |
|Configuration-As-Code  | ✔  | [Ansible, AAP]({{< relref path="/docs/concepts/ansible" >}})   |
|Infrastructure-As-Code | ✔  | [Ansible, AAP]({{< relref path="/docs/concepts/ansible" >}})   |
