---
title: "Politie ( 2018 t/m 2020 )"
linkTitle: "Politie"
weight: 2
description: >
  [![Full Lifecycle](https://img.shields.io/badge/Capability-Full%20Lifecycle-green)](https://c2platform.org/en/docs/guidelines/project/capability/ "Guideline: Ansible Role Capability Labeling")
  Bij de Politie wordt een open aanpak gevolgd
  waarbij met krachtige ontwikkellaptops in
  het open source-domein ( op
  {{< external-link
  url="https://github.com/tpelcm/ansible"
  text="GitHub"
  htmlproofer_ignore="false" >}}
   ) kan worden gewerkt.
---

{{< under_construction_nl >}}

<!-- TODO deze naar Project / Automation taxonomy -->

|Categorie||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|    |           |
|Orchestration          |    |           |
|Code Pipelines         |✔   |Jenkins    |
|Policy-As-Code         |✔   |Ansible    |
|Configuration-As-Code  |✔   |Ansible    |
|Infrastructure-As-Code |✔   |Ansible    |

[![Full Lifecycle](https://img.shields.io/badge/Capability-Full%20Lifecycle-green)](https://c2platform.org/en/docs/guidelines/project/capability/ "Guideline: Ansible Role Capability Labeling")
<!--
Net als het
{{< external-link url="https://scaledagileframework.com/system-team/" text="SAFe system team" htmlproofer_ignore="false" >}}
van [UWV ASC]({{< relref path="./uwv" >}}),
zet het **Tool en Process Engineer (TPE) Team** van de Politie de eerste stappen richting een
["open, tenzij"]({{< relref path="/docs/concepts/oss" >}}) ontwikkelaanpak.

Het TPE-team heeft de verantwoordelijkheid voor het beheer en de levenscyclus
van tools zoals Jira, Confluence, Jenkins, Nexus, SonarQube en Bitbucket, die
worden gebruikt door meer dan 90 Agile/Scrum-teams bij de Politie.

Openbare GitHub-projecten, zoals
{{< external-link url="https://github.com/tpelcm/ansible" text="tpelcm/ansible" htmlproofer_ignore="false" >}}
, worden ingezet om de ontwikkeling van Ansible-automatisering te ondersteunen.
Bovendien zijn de teamleden uitgerust met krachtige Dell Latitude 5591-laptops
om de productiviteit en efficiëntie van de Ansible-ontwikkeling aanzienlijk te
verbeteren. Het gebruik van
[dergelijke laptops]({{< relref path="/docs/concepts/dev/laptop" >}})
maakt integraal deel uit van de C2 Platform-aanpak met betrekking tot de
[ontwikkelomgeving]({{< relref path="/docs/concepts/dev/laptop" >}})

Dit betekent dat een groot deel van het software- en systeemtechnisch werk, dat
normaal gesproken plaatsvindt binnen het "veilige" maar rigide domein van de
Politie op beveiligde Politie-systemen met alle begrijpelijke beperkingen die
daar gelden, nu wordt verschoven naar het open source-domein. Dit open
source-domein staat bekend om zijn hoge productiviteit en flexibiliteit en is nu
een tastbare realiteit geworden.

[![Full Lifecycle](https://img.shields.io/badge/Capability-Full%20Lifecycle-green)](https://c2platform.org/en/docs/guidelines/project/capability/ "Guideline: Ansible Role Capability Labeling")
[![Advanced Monitoring](https://img.shields.io/badge/Capability-Advanced%20Monitoring-brightgreen)](https://c2platform.org/en/docs/guidelines/project/capability/ "Guideline: Ansible Role Capability Labeling")



---

## Overview

{{< plantuml id="kubernetes" width="800px">}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_WITH_LEGEND()
'SHOW_LEGEND(false)

HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/web_server>
!include <office/Servers/application_server>
!include <office/Servers/database_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
!include <cloudogu/tools/ansible>
!include <cloudinsight/docker>
!include <tupadr3/devicons/github>
!include <logos/kubernetes>
!include <office/Devices/device_laptop>
!include <tupadr3/font-awesome/users>
!include <tupadr3/devicons2/vagrant>
'!include <cloudogu/tools/virtualbox>
!include <tupadr3/devicons2/git>
!include <logos/bitbucket>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")

AddRelTag("light", $textColor="#93c7fb")
AddRelTag("red", $lineColor="red")
AddRelTag("ansible", $sprite="ansible", $legendText="ansible provision")
AddContainerTag("virtual_server", $sprite="virtual_server", $legendText="virtual-server", $bgColor="#1168bd")
AddContainerTag("application_server", $sprite="application_server", $legendText="application-server", $bgColor="#1168bd")
AddContainerTag("web_server", $sprite="web_server", $legendText="web-server", $bgColor="#1168bd")
AddContainerTag("database_server", $sprite="database_server", $legendText="database-server", $bgColor="#1168bd")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-galaxy", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("docker_container", $sprite="docker", $legendText="docker-server", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddContainerTag("workstation", $sprite="workstation", $legendText="virtual-desktop", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddSystemTag("laptop", $sprite="device_laptop", $legendText="dev-laptop")
' , $bgColor="#8fc5fa", $fontColor="#0e3962"
AddContainerTag("vagrant", $sprite="vagrant", $legendText="vagrant")
AddContainerTag("ansible_cli", $sprite="ansible", $legendText="ansible")
'AddContainerTag("virtualbox", $sprite="virtualbox", $legendText="virtualbox")
AddContainerTag("git", $sprite="git", $legendText="git")
AddSystemTag("bitbucket", $sprite="bitbucket", $legendText="bitbucket")
AddExternalSystemTag("github", $sprite="github", $legendText="github.com")
', $bgColor="#8fc5fa", $fontColor="#0e3962"

UpdateRelStyle("#042a4f", "#042a4f")

Person(engineer, "TPE Engineers", "", $sprite="users")
Person(agile_team, "Scrum / Agile Teams", "+90 teams that use \nCI/CD Platform", $sprite="users")
Rel_R(engineer, agile_team, "Supports")

Enterprise_Boundary(politie, "Politie" ) {
  Container(werkplek, "Politie Werkplek", $tags="workstation")
  System(ci, "CI/CD Platform", "Staging and Production Environment")
  System(bitbucket, "Bitbucket", "", $tags="bitbucket") {
    Container(bitbucket_ansible, "Ansible Inventory for CI/CD Platform Environments\n( Staging, Production)", $tags="ansible_cli")
  }
  Container(ansible_control_node, "Ansible Control Node", $tags="ansible_cli")
}
'Rel(agile_team, werkplek, "")
Rel(engineer, ansible_control_node, "")
Rel(ansible_control_node, ci, "Manages")

System(ansible_automation, "Ansible Automation", "OSS Ansible Roles, Collections and Reference Implementation", $tags="github")
Boundary(local, "Local" ) {
  System(laptop, "High-End\nDeveloper Laptop", $tags="laptop")
  System(cid, "CI/CD Platform", "OSS Referentie Implementatie")
}
'Rel(engineer, laptop, "Equiped with")
'Rel(engineer, werkplek, "")
Rel(engineer, cid, "Develops reference implementation and Ansible automation")
Rel(cid, ansible_automation, "Developed using")
Rel(ci, ansible_automation, "Managed using")

'Boundary(local2, "Local Development") {
'  'System(laptop, "High-End\nDeveloper Laptop", $tags="laptop")
'  ' Container(laptop_virtualbox, "VirtualBox", $tags="virtualbox")
'  Container(git, "Git", $tags="git")
'  Container(laptop_vagrant, "Vagrant", $tags="vagrant")
'  Container(laptop_ansible, "Ansible", $tags="ansible_cli")
'  System(cid, "CI/CD Platform", "Confluence, Jira, Bitbucket, Nexus, SonarQube, Jenkins")
'}
'Rel(engineer, git, "Commit, pull, push changes")
'Rel(laptop, cid, "")
'Rel(laptop_vagrant, laptop_ansible, "Uses a provisioner")
'Rel(laptop_vagrant, cid, "")
'Rel_R(laptop_vagrant, git, "Uses Ansible automation\n( Collections / Roles )")
'Lay(laptop, laptop_vagrant)
'Lay(laptop, laptop_ansible)






' System_Boundary(local, "cicd.int.politie") {
'   Boundary(ansible_managed, "ansible managed") {
'     Boundary(ansible, "ansible controller node", "") {
'       Container(bastion, "Bastion", "cid-bastion1", $tags="application_server") {
'         Boundary(guacamole, "vdi", "guacamole,docker") {
'           Container(gateway, "Gateway", "guacamole,docker", $tags="docker_container")
'           Container(desktop, "Shared\nDesktop", "guacamole,docker", $tags="docker_container") {
'             Container(ansible_cli, "Ansible CLI")
'           }
'           Container(desktop2, "Desktop", "guacamole,docker", $tags="docker_container") {
'             Container(ansible_cli2, "Ansible CLI")
'           }
'         }
'       }
'     }
'     Container(rproxy, "Reverse Proxy", "cid-rproxy1", $tags="rproxy")
'     Container(confluence, "Confluence", "cid-confluence1", $tags="application_server")
'     Container(jira, "Jira", "cid-jira1", $tags="application_server")
'     Container(bitbucket, "Bitbucket", "cid-bitbucket1", $tags="application_server")
'     Container(nexus, "Nexus", "cid-nexus1", $tags="application_server")
'     Container(sonarqube, "SonarQube", "cid-sonarqube1", $tags="application_server")
'     Container(jenkins, "Jenkins", "cid-jenkins1", $tags="application_server")
'
'     ContainerDb(postgresql, "PostgreSQL", "cid-postgresql1", $tags="database_server")
'     ContainerDb(oracle, "Oracle", "cid-oracle1", $tags="database_server")
'   }
' }
'
' Rel(rproxy, confluence, "", "")
' Rel(rproxy, jira, "", "")
' Rel(rproxy, bitbucket, "", "")
' Rel(rproxy, nexus, "", "")
'Rel(rproxy, sonarqube, "", "")
'Rel(rproxy, jenkins, "", "")
'Rel(rproxy, gateway, "", "", $tags="red")
'
'Rel(gateway, desktop, "", "", $tags="red")
'Rel(gateway, desktop2, "", "", $tags="red")
'
'Rel(sonarqube, postgresql, "", "")
'Rel(jira, oracle, "", "")
'Rel(confluence, oracle, "", "")
'Rel(bitbucket, oracle, "", "")
'
'System_Ext(galaxy_website, "galaxy.ansible.com", "Stores all open source Ansible automation \n( collections, roles )", '$tags="ansible")
'System_Ext(github, "github.com", "Version controls open source Ansible roles and reference implementation for Dutch 'police CI/CD system", $tags="github")
'
'Rel_D(ansible_cli, galaxy_website, "Download Ansible collections / roles")
'Rel(engineer, rproxy, "Access Jira\nConfluence\nEtc...", "cicd.int.politie/jira\ncicd.int.politie/confluence")
'Rel(user, rproxy, "Access Jira\nConfluence\nEtc...", "cicd.int.politie/jira\ncicd.int.politie/confluence")
'Rel(engineer, rproxy, "Connect to\nvirtual ndesktop", "cicd.int.politie/desktop", $tags="red")
'
''Rel(desktop, ansible_managed, "", "")

{{< /plantuml >}}

-->