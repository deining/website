---
title: "SZW Venus ( 2023 t/m heden)"
linkTitle: "SZW Venus"
weight: 5
description: >
  [![Does Not Apply](https://img.shields.io/badge/Capability-N.A.-lightgrey)](https://c2platform.org/en/docs/guidelines/project/capability/ "Guideline: Ansible Role Capability Labeling")
  Ministerie van Social Zaken en Werkgelegenheid ( SZW )
---

{{< under_construction_nl >}}

|Categorie||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|    |           |
|Orchestration          | ✔   | Rancher |
|Code Pipelines         | ✔  | GitLab ( OSS, self-hosted) [^1]  |
|Policy-As-Code         |   | |
|Configuration-As-Code  |   |    |
|Infrastructure-As-Code |   |    |

[^1]: self-hosted free / community edition