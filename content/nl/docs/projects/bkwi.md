---
title: "Bureau Keteninformatisering Werk en Inkomen ( BKWI ) ( 2020 t/m 2022)"
linkTitle: "BKWI"
weight: 3
description: >
  [![Enhanced Configuration](https://img.shields.io/badge/Capability-Enhanced%20Configuration-orange)](https://c2platform.org/en/docs/guidelines/project/capability/ "Guideline: Ansible Role Capability Labeling")
  Bij Bureau Keteninformatisering Werk en Inkomen ( BKWI ) volgt er een verfijning
  van de aanpak en wordt voortgebouwd op automatisering van de Politie.
---

{{< under_construction_nl >}}

|Categorie||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|✔   |Zabbix     |
|Orchestration          |✔   |Kubernetes |
|Code Pipelines         |✔   |GitLab     |
|Policy-As-Code         |✔   |Ansible, AWX [^1] |
|Configuration-As-Code  |✔   |Ansible, AWX |
|Infrastructure-As-Code |✔   |Ansible, AWX |

[^1]: Oudere, open source versie van Ansible Automation Platform ( AAP )