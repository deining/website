---
title: "Projecten"
linkTitle: "Projecten"
weight: 3
description: >
  Een overzicht van projecten vanaf 2016 tot nu die in zekere mate gebruik hebben
  gemaakt van het C2 Platform. Van de allereerste kleine stappen tot de
  momenteel lopende meer geavanceerde aanpak bij Rijkswaterstaat.
---

{{< under_construction_nl >}}
