---
categories: ["How-to"]
tags: [ansible, vagrant]
title: "Aan de Slag"
linkTitle: "Aan de Slag"
weight: 1
description: >
  Leer hoe je een lokale ontwikkelomgeving kunt opzetten en je eerste virtuele
  machine kunt maken met behulp van Vagrant en Ansible.
---

De [ontwikkelomgeving]({{< relref "/docs/concepts/dev" >}} "Concept:
Ontwikkelomgeving") dient als een fundamenteel element binnen de C2
Platform-aanpak. Door gebruik te maken van de kracht van Vagrant, LXD en
VirtualBox (ideaal voor VM's op basis van MS Windows), stelt het je in staat om
realistische infrastructuur lokaal te simuleren, vergelijkbaar met de visie van
Jeff Geerling in zijn boek, [Ansible for DevOps]({{< relref
path="/docs/concepts/dev/vagrant/ansible-for-devops" >}}).

Hoewel de initiële configuratie van deze ontwikkelomgeving ingewikkeld kan
lijken, leidt het overwinnen van deze eerste uitdaging tot een veelzijdig en
krachtig hulpmiddel voor de ontwikkeling en test van automatiseringsoplossingen.

Bijvoorbeeld, het uitvoeren van een enkele opdracht zo eenvoudig als deze:

```bash
vagrant up c2d-awx1
```

zal een volledig functionele Kubernetes-cluster op je lokale werkstation creëren en
doorgaan met het implementeren en configureren van de [Ansible Automation Controller (Galaxy
NG)]({{< relref path="/docs/concepts/ansible/aap" lang="en">}}) erop.

Toch is het doel niet alleen eenvoud; het gaat er ook om de middelen te
verstrekken om automatiseringsprojecten te begrijpen, implementeren en
optimaliseren. De documentatie speelt een cruciale rol bij het bereiken van dit
doel. Neem bijvoorbeeld de handleiding [Setup the Automation Controller (AWX)
met Ansible]({{< relref path="/docs/howto/awx/awx" lang="en">}}), die verder
gaat dan het documenteren van eenvoudige stappen (zoals één opdracht, `vagrant
up c2d-awx1`, de taak volbrengt) om ervoor te zorgen dat ook begrepen wordt hoe
deze enkele opdracht het gewenste resultaat bereikt.

Uitgebreide instructies in het Engelstalige gedeelte van deze website,
begeleiden bij het opzetten, configureren en gebruiken van de ontwikkelomgeving:
<div class="mx-auto">
	<a class="btn btn-lg btn-primary mr-3 mb-4" href="{{< relref path="/docs/howto/dev-environment/setup" lang="en" >}}">
		Setup a Development Environment on Ubuntu 22<i class="fas fa-arrow-alt-circle-right ml-2"></i>
	</a>
</div>

{{< alert titel="Opmerking:" >}}
Hoewel we ervan uitgaan dat je een
[high-end ontwikkelaarslaptop]({{< relref "/docs/concepts/dev/laptop" >}} "Concept: High-end Developer Laptop")
gebruikt met Ubuntu 22.04, is het ook volledig haalbaar om het proces te starten binnen een
VirtualBox VM met Ubuntu 22. De standaardconfiguratie van het referentieproject neigt naar
[Linux Containers (LXD)]({{< relref "/docs/concepts/dev/lxd" >}} "Concept: Linux Containers (LXD)"),
en deze werken naadloos binnen een VirtualBox VM.
{{< /alert >}}
