---
title: "UWV TAM  ( 2022 to present )"
linkTitle: "UWV TAM"
weight: 4
description: >
  [![Does Not Apply](https://img.shields.io/badge/Capability-N.A.-lightgrey)](https://c2platform.org/en/docs/guidelines/project/capability/ "Guideline: Ansible Role Capability Labeling")
  UWV Technical Application Management ( TAM )
---

{{< under_construction >}}

|Category||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|    |           |
|Orchestration          |    | |
|Code Pipelines         |   | Azure DevOps [^1]   |
|Policy-As-Code         |   |Ansible CLI, |
|Configuration-As-Code  |   |Ansible CLI, Azure DevOps [^1]   |
|Infrastructure-As-Code |   |Ansible    |

[^1]: Self-hosted Azure DevOps.