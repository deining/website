---
title: "Dutch Police ( 2018 to 2020 )"
linkTitle: "Dutch Police"
weight: 2
description: >
  [![Full Lifecycle](https://img.shields.io/badge/Capability-Full%20Lifecycle-green)](https://c2platform.org/en/docs/guidelines/project/capability/ "Guideline: Ansible Role Capability Labeling")
  At the Police, an open approach is followed, allowing for work in the open-source domain (on
  {{< external-link
  url="https://github.com/tpelcm/ansible"
  text="GitHub"
  htmlproofer_ignore="false" >}}
  ) with powerful development laptops.
---

{{< under_construction >}}

|Category||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|    |           |
|Orchestration          |    |           |
|Code Pipelines         |✔   |Jenkins    |
|Policy-As-Code         |✔   |Ansible    |
|Configuration-As-Code  |✔   |Ansible    |
|Infrastructure-As-Code |✔   |Ansible    |