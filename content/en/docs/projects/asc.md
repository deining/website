---
title: "UWV ASC ( 2016 to 2018 )"
linkTitle: "UWV ASC"
weight: 1
description: >
    [![Does Not Apply](https://img.shields.io/badge/Capability--lightgrey)](https://c2platform.org/en/docs/guidelines/project/capability/ "Guideline: Ansible Role Capability Labeling")2016-2018
    At the Agile Systems Development Center (ASC) of UWV, it has been found that it should be ["open, unless."]({{< relref path="docs/concepts/oss" >}}) Otherwise, there is a risk of "accidental IP," with all the associated consequences.
---

{{< under_construction >}}

|Category||Tools|
|-----------------------|----|-----------|
|Event-Driven Automation|    |           |
|Orchestration          |    |           |
|Code Pipelines         |    |           |
|Policy-As-Code         |✔   |Chef       |
|Configuration-As-Code  |✔   |Chef       |
|Infrastructure-As-Code |✔   |Chef       |