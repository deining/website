---
categories: ["How-to"]
tags: [ansible, vagrant]
title: "Getting Started"
linkTitle: "Getting Started"
weight: 1
description: >
  Learn how to set up a local development environment and create your first
  virtual machine using Vagrant and Ansible.
---

The [development environment]({{< relref "/docs/concepts/dev" >}} "Concept:
Development Environment") serves as a foundational element within the C2
Platform approach. By harnessing the power of Vagrant, LXD, and VirtualBox
(ideal for MS Windows-based VMs), it empowers you to simulate real-world
infrastructure locally, akin to Jeff Geerling's vision in his book, [Ansible for
DevOps]({{< relref path="/docs/concepts/dev/vagrant/ansible-for-devops" >}}).

While the initial setup of this development environment may appear intricate,
conquering this initial challenge unlocks a versatile and potent tool for the
development and testing of automation solutions.

For instance, executing a single command as straightforward as this:

```bash
vagrant up c2d-awx1
```

will conjure a fully functional Kubernetes Cluster on your local workstation and
proceed to deploy and configure the
[Ansible Automation Controller ( AWX)]({{< relref path="/docs/concepts/ansible/aap" >}})
on it.

Yet, the goal is not just simplicity; it's also about equipping you with the
resources to comprehend, implement, and optimize your automation projects. The
documentation plays a pivotal role in achieving this objective. Take, for
instance, the how-to guide [Setup the Automation Controller ( AWX ) using
Ansible]({{< relref path="/docs/howto/awx/awx" >}}), which goes beyond
documenting mere steps (as one command, `vagrant up c2d-awx1`, accomplishes the
task) to ensure you understand how this single command achieves the desired
outcome.

Comprehensive instructions await you in the English section of our website,
guiding you through setting up, configuring, and harnessing the development
environment:

<div class="mx-auto">
	<a class="btn btn-lg btn-primary mr-3 mb-4" href="{{< relref path="/docs/howto/dev-environment/setup" lang="en" >}}">
		Setup a Development Environment on Ubuntu 22<i class="fas fa-arrow-alt-circle-right ml-2"></i>
	</a>
</div>

{{< alert title="Note:" >}}
While we assume you are utilizing a
[high-end developer laptop]({{< relref "/docs/concepts/dev/laptop" >}} "Concept: High-end developer laptop")
with Ubuntu 22.04, it's also entirely feasible to initiate the process within a
VirtualBox VM running Ubuntu 22. The reference project's default configuration
leans towards
[Linux Containers (LXD)]({{< relref "/docs/concepts/dev/lxd" >}} "Concept: Linux Containers (LXD)"),
and these operate seamlessly within a VirtualBox VM.
{{< /alert >}}
