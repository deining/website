---
categories: ["Guideline"]
tags: [clone, git]
title: "Clone script"
linkTitle: "Clone script"
weight: 2
description: >
  Automate setup of the development environment with multiple Git repositories.
---

---
 > Use a clone script to automate setup of the development environment with multiple Git repositories in Ansible projects, including standardizing repository location, configuring Git settings, and handling pre-commit hooks.

---

## Problem

Setting up a development environment with multiple Git repositories can be time-consuming and error-prone. It requires creating repositories in a specific location, configuring Git settings, and performing other repetitive tasks. Additionally, ensuring that all developers have repositories in the same structured location can improve collaboration and make it easier to work together.

## Solution

Create a clone script that automates the process of setting up the development environment for your Ansible playbook/configuration projects. The script should perform the following tasks:

1. Set the necessary environment variables:
   * `REPO_DIR`: The directory where the Git repositories will be created.
   * `REPO_FLDR`: The folder name for the main repository.
2. Check if the `GIT_USER` and `GIT_MAIL` environment variables are set. If not, display an error message and exit.
3. Create the `REPO_DIR` directory if it does not already exist.
4. Define a function, `clone_repo`, to clone a repository, configure Git settings, and perform additional tasks:
    * Change to the `REPO_DIR/REPO_FLDR` directory.
    * Check if the repository folder does not exist.
    * If the folder does not exist, clone the repository, fetch the specified branch, checkout the branch, set the upstream branch, and pull the latest changes.
    * Configure the Git user name and email using the `GIT_USER` and `GIT_MAIL` environment variables.
    * If linting is enabled (`C2_LINTERS` set to "Y"), copy the pre-commit hook script to the repository's `.git/hooks` directory.
    * If linting is disabled (`C2_LINTERS` set to "N"), remove the pre-commit hook script from the repository's `.git/hooks` directory.
5. Call the `clone_repo` function for each repository you want to clone, providing the repository URL, destination folder path, and branch name.

## Examples and implementation

For examples of the clone script implementation, you can refer to `clone.sh` files in the project repositories:

* [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible/" >}})
* [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis/" >}})

These examples demonstrate how to use the clone script to automate the setup of development environments with multiple Git repositories for Ansible projects.

The project [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible/" >}}) for example can be setup using command:

```
curl -L https://gitlab.com/c2platform/ansible/-/raw/master/clone.sh | bash
```

To setup linters you can run the commands:

```
export C2_LINTERS=Y
curl -L https://gitlab.com/c2platform/ansible/-/raw/master/clone.sh | bash
```

To remove pre-commit hooks use `export C2_LINTERS=N`.
