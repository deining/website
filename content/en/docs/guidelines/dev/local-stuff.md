---
categories: ["Guideline"]
tags: [group_vars]
title: "Local Stuff"
linkTitle: "Local Stuff"
weight: 4
description: >
  Ansible configuration that should be local and ignored by Git.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}})

---
> In [Ansible Inventory Projects]({{< relref path="/docs/concepts/ansible/projects/inventory" >}})
> create a file `group_vars/all/local_stuff.yml` for Ansible configuration you don't want to
> share with others. This file is in `.gitignore` so you won't accidentally add
> it to the git repo.


---

## Problem

Local configuration is accidentally shared with team members.

## Context

During development we sometimes want to configure something locally and not add
configuration to version control and share with team members. For example we
want to configure Ansible to use a local proxy server.

## Solution

Create a file `group_vars/all/local_stuff.yml`. This file is in `.gitignore` so
you won't accidentally add it to the git repo.
