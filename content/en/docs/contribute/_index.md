---
title: "Community"
linkTitle: "Community"
weight: 100
draft: true
menu:
  main:
    weight: 30
---

## Welcome to the C2 Platform Community!

We are thrilled to invite you to join our open source initiative, the C2
Platform, designed to revolutionize IT service delivery and management for the
Dutch government. Our platform is a robust, open automation solution that
supports government organizations on their journey from Infrastructure-as-Code
to Event-driven Automation.

## How Can You Get Involved?

There are numerous ways to become an active part of the C2 Platform community:

* **Explore and Experiment:** Start by setting up a development environment and
  experience firsthand what C2 Platform has to offer. Dive into automation
  solutions that can streamline your IT processes.
  <div class="mx-auto">
	<a class="btn btn-lg btn-primary mr-3 mb-4" href="{{< relref path="/docs/howto/dev-environment/setup" lang="en" >}}">
		Setup a Development Environment on Ubuntu 22<i class="fas fa-arrow-alt-circle-right ml-2"></i>
	</a>
  </div>
* **Resource Hub:** Browse our website at
  {{< external-link
  url="https://c2platform.org"
  text="c2platform.org"
  htmlproofer_ignore="false" >}} to access a wealth of
  information on automation solutions, best practices, real-world examples, and
  templates.
* **Utilize Ansible Collections:** Leverage our [Ansible collections]({{< relref path="/categories/ansible-collection/" lang="en" >}}) to build your
  own Ansible automation platform. Our collections are designed to provide you
  with a solid foundation for your automation projects.
* **Contribute on GitLab:** Get involved by submitting and creating tickets in one
  of our many GitLab projects. Report bugs, share your insights, or offer
  suggestions to help improve our platform.
  <div class="mx-auto">
	<a class="btn btn-lg btn-secondary mr-3 mb-4" href="https://gitlab.com/c2platform/">
		GitLab <i class="fab fa-gitlab ml-2 "></i>
	</a>
  </div>
* **Collaborate and Enhance:** Fork our GitLab projects and issue merge requests to
  contribute to the community by fixing bugs, enhancing existing features, or
  introducing new ones.
* **Become a C2 Platform Engineer:** For those seeking even greater involvement,
  consider filling out a form to explore opportunities to become a C2 Platform
  engineer. Gain development access to our GitLab projects, where you can
  actively work on and contribute to open source reference implementations, such
  as the C2 Reference Implementation or the RWS GIS Platform Reference
  Implementation.
* **Full Adoption:** If your organization is interested in adopting the C2 Platform
  approach, you can also fill out a form to discuss the creation of new open
  source reference implementations. These implementations serve as the
  foundation for developing existing C2 Platform automation solutions, including
  Infrastructure-as-Code, Configuration-As-Code, Policy-as-Code, Ci/CD pipelines
  Orchestration (K8s, operators), and Event-driven automation. By embracing this
  approach, you'll be part of a collaborative effort to drive innovation within
  your organization.

Join us on this exciting journey of automation and improvement. Let's work
together to shape the future of IT service delivery and management in the Dutch
government. Together, we can achieve more!

For any inquiries or to get started, please don't hesitate to contact us. We
look forward to having you as part of the C2 Platform community!

<div class="mx-auto">
	<a class="btn btn-lg btn-primary mr-3 mb-4" href="{{< relref path="/docs/contribute/form" >}}">
		Contact Us<i class="fas fa-arrow-alt-circle-right ml-2"></i>
	</a>
</div>