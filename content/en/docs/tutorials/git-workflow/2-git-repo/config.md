---
title: "Configuration Project"
linkTitle: "Configuration Project"
weight: 2
description: >
  Establish the foundation for managing your Ansible automation by creating a Ansible configuration project, also referred to as a playbook project.
---



