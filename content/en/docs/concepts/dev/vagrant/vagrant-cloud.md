---
categories: ["Concept"]
tags: [vagrant, image]
title: Vagrant Cloud
linkTitle: Vagrant Cloud
weight: 2
description: >
  Vagrant images for LXD, VirtualBox.
---

C2 Platform images are published and distributed using the {{< external-link url="https://app.vagrantup.com/c2platform/" text="Vagrant Cloud" htmlproofer_ignore="true" >}}
