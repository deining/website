---
categories: ["Concept"]
tags: [vagrant, ansible]
title: Vagrant
linkTitle: Vagrant
weight: 2
description: >
  Vagrant provides a user-friendly platform for creating and managing simple local
  development environments, streamlining the setup process.
---

Vagrant enables the creation and configuration of lightweight, reproducible, and
portable development environments. With Vagrant, we can effectively and
efficiently replicate production environments with the same operating system,
packages, users, and configurations.

* {{< external-link url="https://www.vagrantup.com/" text="Vagrant" htmlproofer_ignore="true" >}}
* See the C2 example project [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible/" lang="en" >}}) for an example of a Vagrant project using [LXD]({{< relref "../lxd" >}} "Concept: Linux Containers (LXD)"), [VirtualBox]({{< relref "../virtualbox" >}} "Concept: VirtualBox"), and [Ansible]({{< relref "../../ansible" >}} "Concept: Ansible Automation Platform").
* {{< external-link url="https://docs.ansible.com/ansible/latest/scenario_guides/guide_vagrant.html" text="Vagrant Guide - Ansible Documentation" htmlproofer_ignore="true" >}}
