---
categories: ["Concept"]
tags: []
title: "Reference Implementations"
linkTitle: "Reference Implementations"
weight: 3
description: >
  Unlock the power of the development environment and create fully
  integrated, comprehensive systems. With a few simple commands you can explore
  for example the automation driving deployment and management of a
  GIS Platform at Rijkswaterstaat (RWS).
---

In the realm of system engineering and infrastructure-as-code (IaC), the term
"reference implementation" transcends the status of a mere blueprint. It evolves
into a fully functional, seamlessly integrated system. This exemplar not only
serves as a model but also as a standardized, well-documented exemplar,
showcasing how to efficiently deploy and manage a specific system, with
[Ansible]({{< relref path="/docs/concepts/ansible" >}}) taking center stage.

Ansible, a linchpin of any
[automation]({{< relref path="/docs/concepts/automation" >}})
initiatives, plays a pivotal role in this context.

Within the context of the C2 Platform, a reference implementation conjoins two
pivotal components: the
[Ansible Inventory Project]({{< relref path="/docs/concepts/ansible/projects/inventory" >}})
and the
[Vagrant Project]({{< relref path="/docs/concepts/dev/vagrant" >}}).
The synergy of Vagrant and Ansible is particularly potent, offering a holistic
solution. Vagrant orchestrates the creation of virtual machines, including
network configurations and disk allocations, while Ansible takes charge of
installing and managing various nodes and virtual machines. For instance, in the
case of the GIS Platform, this encompasses deploying components like ArcGIS
Enterprise Web Adaptor, Portal, Server, Datastore, FME, and more.

As depicted in the diagram below, the C2 Platform engineer can effortlessly
construct complete environments via Vagrant, capitalizing on various cloud
services:

1. [Vagrant Cloud]({{< relref path="/docs/concepts/dev/vagrant/vagrant-cloud" >}})
   houses C2 images compatible with LXD, VirtualBox, and more.
1. [GitLab.com]({{< relref path="/docs/concepts/gitlab" >}})
   serves as the repository for all C2 Projects, offering a myriad of resources.
1. [Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}) functions as
   the repository for open-source automation roles and collections, including
   those designed for the C2 Platform in the realm of GIS.

This all is made possible through an
["open, unless"]({{< relref path="/docs/concepts/oss" >}}) approach.
This approach permits the exploration and test-driving of the GIS Platform,
mirroring its deployment at the RWS Data Center, by leveraging its open-source
counterpart reference implementation.

In the context of an open-source Ansible project, a reference implementation
stands as a prime example project, offering insights into how [Ansible
Collections]({{< relref path="/docs/concepts/ansible/projects/collections" >}})
and [Ansible Roles]({{< relref path="/docs/concepts/ansible/projects/roles" >}})
are configured within an
[Ansible Inventory Project]({{< relref path="/docs/concepts/ansible/projects/inventory" >}}),
often referred to as a playbook project. This configuration equips users to deploy a fully functional
system. The reference implementation doesn't merely serve as an abstract
reference but as a practical framework, openly accessible for similar
deployments by clients and organizations.

Let's delve into the core attributes of this concept:

1. **Comprehensive Implementation:** A reference implementation encapsulates the
   entirety of a functional and integrated system. It meticulously covers all
   configurations, settings, and interdependencies required for seamless
   operation. This comprehensiveness assures that the reference implementation
   mirrors the intended system in entirety.
1. **Parity with Production:** For instance, the GIS Platform Reference
   Implementation serves as the open-source counterpart to the GIS Platform
   within Rijkswaterstaat (RWS). This implies that it closely emulates the
   production environment, ensuring alignment with real-world setups.
1. **Detailed Documentation and Best Practices:** Similar to conventional
   reference implementations, a complete reference implementation features
   extensive documentation. This documentation elucidates the system's purpose,
   configurations, best practices, and management guidelines. It stands as an
   invaluable resource for team members involved in deploying and maintaining
   the system.
1. **Training and Consistency:** The reference implementation also serves as a
   vital resource for training and learning, aiding team members in grasping how
   to efficiently deploy and manage the entire system. Additionally, it enforces
   consistency within your infrastructure, mitigating the risk of errors or
   misconfigurations.

In the context of Ansible and Vagrant, a comprehensive reference implementation
may encompass Ansible playbooks for system configuration and management, along
with Vagrantfiles delineating the VM infrastructure. These resources are
thoroughly documented and rigorously tested to ensure they faithfully represent
the target system.

To sum up, a reference implementation is not a mere sketch but a fully
functional, fully integrated system. It guarantees efficient and consistent
management of infrastructure components while aligning with the reality of
production environments. This is particularly significant in scenarios like the
GIS Platform Reference Implementation, where it aims to replicate an existing
system within organizations such as Rijkswaterstaat (RWS).

The purpose of the reference implementation extends to elucidating how software
can be employed and configured, serving as a launching pad for customizing and
configuring software to meet specific requirements. By leveraging open-source
building blocks, like Ansible roles, organizations can leverage the work of
others, expediting software development and implementation. This promotes
uniformity in deployments and fosters collaborative efforts among organizations
employing the same software.

An exemplar of a reference implementation is the
[`ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis/" lang="en" >}})
configuration and playbook project for the Rijkswaterstaat GIS Platform. This
project facilitates the local deployment of a comprehensive, functional
environment for the Rijkswaterstaat GIS Platform. To accomplish this, the
project leverages Ansible collections such as
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis/" lang="en" >}})
and
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore/" lang="en" >}}).

{{< plantuml id="vagrant-ansible-gsd" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
!include <cloudogu/tools/ansible>
!include <logos/kubernetes>
!include <cloudinsight/docker>
!include <tupadr3/devicons/github>
!include <office/Devices/device_laptop>
!include <tupadr3/font-awesome/users>
!include <tupadr3/devicons2/vagrant>
!include <tupadr3/devicons2/git>
!include <logos/gitlab>

sprite $arcgis [100x100/8] {
000000000000000000000000000000000000000124577FFNNNLLDC4200000000000000000000000000000000000000000000
000000000000000000000000000000000257Nd_ywvumWGG80000000000000000000000000000000000000000000000000000
000000000000000000000000000036Nl-xveO800000012345566666655432210000000000000000000000000000000000000
00000000000000000000000026V__xeG000000001d__sgWWWWeemuuvvwyz____lVF742000000000000000000000000000000
00000000000000000000027d_xmQ_I00000000001q____VEDCCCC554321108Oeuwy____dF520000000000000000000000000
00000000000000000005d_zmG00H_R0000134CSaaaiz-___tkbaaaaaaaaaaKC43100GWvx___tN51000000000000000000000
00000000000000001Ft_xW000009_T24Kaaaaaaaaaaaiqz___tkbaaaaaaaaaaaaaSC41008Ovz___d62000000000000000000
000000000000002N__wG00000014__aaaaaaaaaaaaaaaaaiz___tcaaaaaaaaaaaaaaaaUE3000Gmy___V51000000000000000
0000000000001F__wG0000014Saas_iaaaaaaaaaaaaaabbbcl____tltttlllllllldccj__lE2000Gv-__tF20000000000000
000000000005t_xO000002Caaaaaj_jaaaaZaabcddlt_____________zzzzzzzz----_______dF6310ez___N200000000000
0000000001V_-W000002KaaaaZZZa_sSTUdl_____-zyyyqiiiaaar___lbaaaaaaaZRRRRRZZp-__--_lNEj____E0000000000
000000004t_w800002KaaZZRRRSTd______zyxpZZRRRZZaaaaaaaaiz__tjaaaaZRRRRRRRRRRZy_tTZhnwz_____l300000000
0000000E__i00000BaaZRRSTdl_____zpZRRRRRRRRRRRRRRZaaaaaaar___kaZQJRRRRRRRRRRRRh-_dSR208Ww____D0000000
000000V___b0002SaZRSUl____yxhi_tRRRRRRRRRRRRRRRRRRRZaaaaai___kJIRRRRRRRRRRRRRRZz_tSRB000Gy___M000000
00000V_-__s003aaTdl___zxhRRRRZ__aRQQQQRRRRRRRRRRRRRRZZaaZZhz__cRRRRRRRRRRRRRRRRRq__bRR1000n___V00000
0000V_zG-__1Badt___ypZRRRRRRRQq_tJIIIIIIIIQQRRRRRRRRRQQQIIIQz__cRRRRRRRRRRRRRRRRRq__bRS1000X___V0000
000D_-G0r__Ul___yhZRRRRRRRRRIIR__SIIIIIIIIIIIQRRRRRQIIIIIIIIR-__cRRRRRRRRRRRRRRRRRi__bSS1000X___M000
001__P00a____zpZRRRRRRRRRRQIIIIq_tJIIIIIIIIIIIIQRRIIIIJJJKKTTc___dUUUTTSSSRRRRRRRRRi__baS1000p___200
00c_q01E___-hRRRRRRRRRRRRIIIIIIQ__cIHHHIIIIIIIJKLVVdll_____________________tlddVUTSSq_taaK0008-__t00
01__Q3l_-___SRRRRRRRRRRQIIIIIIIHq__J9999IKMVdt______zyyxxpphhZZq___aZZhhppxyyzz____tl__laaA000Q___B0
0T__V-vOI___bRRRRRRRRRQIIIIIIIH9Q__lCNVt____-yxwgYRRRRRRRRRRRRRRz__lRRRRRRRRRRRRZhiyy-___ld2000z__l0
0t_r8000Rr__tRRRRRRRRRIIIIIIIIABEt_____-xwXQIIIIIJRRRRRRRRRRRRRRZ-__bRRRRRRRRRRRRSaaaai__qyyh90Y___1
1__i0001ai___aRRRRRRRRIIIIIJMVt______hH999IIIIIIIRRRRRRRRRRRRRRRRi___SRRRRRRRRRRaaaaaaa-_kaR0009___A
A__a0009aa-__lSRRRRRRRRJKVl____ywXh__lA9AIIIIIIIIRRRRRRRRRRRRRRRRRr__lRRRRRRRRSaaaaaaaai__aa0000___J
J__a000Iaai___bRRRRRRTdt___zxYIIIAIq__UIIIIIIIIIIRRRRRRRRRRRRRRRRRZ-__bRRRRRSaaaaaaaaaaa-_ja0000s__R
R__j000Iaaaz__taRSUd____zpYIIIIIIIIQ-__SIIIIIIIIRRRRRRRRRRRRRRRRRRRi__tSRRSaaaaaaaaaaaaaj_ka0000s__R
Q___000Iaaai___ll____zpRRRJIIIIIIIIIZ__tJIIIIJJRRRRRRRRRSSSSSRRRRRRRz__jSaaaaaaaaaaaaaaaj_tj0000s__I
H___900Haaaas_____-qZRRRRRRRJJIIIIIIIq__lJIJRRRRRRRRRRRaaaaaaabSSSSSb___aaaaaaaaaaaaaaaaa__i0000___8
8___J008bcl______jaSRRRRRRRRRRRRJJJJJRz__cRRRRRRRRRSTTddlltt______________ttllddcbbaaaaaa-_Z0009__-0
0-__c00C____-z___taaSSRRRRRRRRRRRRRRRRZ-__cRRSTUdlt_________--zzzzyyyy___zzz-_______tldcbs_H000K__Z0
0Y___A0f--ziaar___laSRRRRRRRRRRRRRRRRRRZ___dl______-zyyqiiiaaaaaaaaaaar__kaaaaaaiqqyz-_____E201t__80
00___U000Yaaaaar___kaaSRRRRRRRRRRRRRRSUd______-yqiaaaaaaaaaaaaaaaaaaaai___aaaaaaaaaaaaiiq-_yv8K__h00
00X___A008aaaaaaz___caaaSSRRRRRRRSTdt________kaaaaaaaaaaaaaaaaaaaaaaaaas__jaaaaaaaaaaaaaar_901t_-800
000y__l100Gaaaaaa-___caaaaaSSRSUd____-ypZRi___caaaaaaaaaaaaaaaaaaaaaaaaj__taaaaaaaaaaaaaZZ_90T__O000
0008-__U000Paaaaaaz___caaaaacl____zqZRRRRRRi___lbaaaaaaaaaaaaaaaaaaaaaaa___aaaaaaaaaaaaZ8R_0B__g0000
0000G___M000Paaaaaaz___labd____zqaaaSSSSSRRSi-__tbaaaaaaaaaaaaaaaaaaaaaas__jaaaaaaaaaaZ80R-At_o00000
00000G___M000Gaaaaaaq_______-qaaaaaaaaaaaaaaai-__tbaaaaaaaaaaaaaaaaaaaaaj__jaaaaaaaaaY800Rtl_w000000
000000Gy__V1008Yaaaabt_____iaaaaaaaaaaaaaaaaaaiz___kaaaaaaaaaaaaaaaaaaaaj__saaaaaaaaX0000a__o0000000
00000008w__l3000Oabl___--__tcaaaaaaaaaaaaaaaaaaar___lbaaaaaabbbbcdldlllll__tlddcbbZG00004__f00000000
000000000W___N100B__-ziaiz___tbaaaaaaaaaaaaaaaaaai-__tbbcllt_________----___----___dNF5N_-O000000000
00000000008w__l5000GXaaaaaiz___lbaaaaaaaaaaaaaaaaaj________-zzzqqiiiaaaaa___aaZW0088Gj__v00000000000
000000000000Oy__d40008Xaaaaaiz___lbaaaaaaaaaaabclt_______jiaaaaaaaaaaaaaa__rYO000004d_xO000000000000
00000000000000Wy__l51000OYaaaaiz___lbaaaaaabcl____zqii-__tbaaaaaaaaaaaaaat_Z000015l_yW00000000000000
0000000000000000Wx__tF30000OXaaaiz___lbaabl___-ziiaaaaiz___kbaaaaaaaaaZWGs_R003N__xW0000000000000000
000000000000000000Ow___lF30000GWXZiz-__tt__-ziaaaaaaaaaar___lbaaaaZXO8000__UFl__wO000000000000000000
000000000000000000008ew___tN5200008GXt____tbaaaaaaaaaaaaaiz___jOG0000013F____xe800000000000000000000
000000000000000000000008Wv____lN742Dt__new__laXWWWWWWWWOG88Wz__d3025Fd____wW800000000000000000000000
000000000000000000000000000Guvz_______V543AYy_tF300000111234D________-wuG000000000000000000000000000
00000000000000000000000000000000Guuwz___________________________yvuO80000000000000000000000000000000
000000000000000000000000000000000000000GOWmuuvvvwwwxwwwvvuuuWG00000000000000000000000000000000000000
}

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtual-desktop")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddContainerTag("vagrant_container", $sprite="vagrant", $legendText="vagrant")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-awx", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
Person(engineer, "Engineer")
AddSystemTag("gis", $sprite="arcgis*.5", $legendText="gis")
AddExternalSystemTag("gitlab", $sprite="gitlab", $legendText="gitlab")
AddExternalSystemTag("ansible", $sprite="ansible", $legendText="ansible-awx")
AddExternalSystemTag("vagrant", $sprite="vagrant", $legendText="vagrant cloud")
AddExternalContainerTag("gis_inventory", $sprite="arcgis*.4", $legendText="gis")

Person(engineer, "Engineer")

Boundary(laptop, "high-end developer laptop", $type="ubuntu-22") {
  System(gsd, "GIS Platform", "", "", $tags="gis") {
  }
  Container(vagrant, "Vagrant", "", $tags="vagrant_container")
  Container(ansible_local, "Ansible", "", $tags="ansible_container")
}

System_Ext(galaxy_website, "Ansible Galaxy\ngalaxy.ansible.com", "Stores all open source Ansible automation \n( collections, roles )", $tags="ansible")
System_Ext(gitlab, "GitLab Ultimate\ngitlab.com", "Stores all C2 Platform Git\n repositories and projects", $tags="gitlab") {
   Container_Ext(gis_inventory, "GIS Platform\nReference Implementation", "", $tags="gis_inventory")
}
System_Ext(vagrant_cloud, "Vagrant Cloud\napp.vagrantup.com", "Stores all C2 VM images", $tags="vagrant")

Rel(ansible_local, gsd, "Provision ArcGIS Portal, WebAdaptor, Server, Datastore, FME, etc", "")
Rel(vagrant, gsd, "Create VM's\nnetwork, disks etc", "")
Rel(engineer, vagrant, "Create VM's, provision services using Vagrant CLI", "")
Rel_D(vagrant, ansible_local, "", "")
Rel_R(ansible_local, galaxy_website, "Download GIS automation / collection", "")
Rel(ansible_local, gitlab, "", "")
Rel_R(vagrant, vagrant_cloud, "Download Vagrant \nboxes ( images )", "")
{{< /plantuml >}}


