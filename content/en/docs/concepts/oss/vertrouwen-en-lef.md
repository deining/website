---
categories: ["Reference"]
tags: [odm, oss]
title: "Open Source Working Demands Trust and Courage"
linkTitle: "Trust and Courage"
weight: 2
description: >
    Article by Boris van Hoytema (BZK) about exploring transformative potential of
    open source in government and the challenges it presents.
---

In this article from Digitale Overheid, Boris van Hoytema, the Kwartiermaker
OSPO (Open Source Program Office) at the Ministry of the Interior and Kingdom
Relations (BZK), discusses the importance of open source in government. He
emphasizes open source's role in enhancing autonomy, collaboration, and public
value creation while reducing risks. Boris highlights that while open source is
widely used, there are challenges to adopting it within government, including
the need for a change in mindset. He explains how open source can help
governments regain autonomy and sovereignty and offers insights into the value
of transparency in public service delivery. Boris's role is to address these
challenges and promote open source adoption within the government.

{{< external-link url="https://www.digitaleoverheid.nl/achtergrondartikelen/open-source-werken-vraagt-om-vertrouwen-en-lef/" text="Open source werken vraagt om vertrouwen en lef" htmlproofer_ignore="false" >}}


