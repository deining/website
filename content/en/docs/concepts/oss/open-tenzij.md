---
categories: ["Reference"]
tags: [odm, oss]
title: "Government Policy: Open, Unless"
linkTitle: "Government Policy"
weight: 1
description: >
    The government aims to make software open source, unless it is not possible
    for legal reasons. This increases transparency, collaboration, and software
    security, promotes efficiency and innovation, and reduces vendor dependence.
---

The government's policy, known as 'Open, Unless', aims to make software
developed by or for the government as open source as possible. This means that
the source code of the software should be accessible and reusable by everyone
unless there are legal restrictions under the Open Government Act.

The government's open-source policy offers several advantages. Firstly, it
enhances transparency and trust in the government because everyone can gain
insight into how the software operates. Secondly, it promotes collaboration,
both within the government and with external partners, leading to more effective
and efficient public administration. Furthermore, open source contributes to the
security of software by allowing others to inspect the code for errors and
risks.

Additionally, open source stimulates innovation, reduces licensing costs, and
speeds up development processes. It increases flexibility in the use of software
and reduces dependence on large commercial vendors.

In essence, the 'Open, Unless' government policy promotes transparency,
collaboration, security, efficiency, innovation, and independence within
government software development processes.

{{< external-link url="https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/open-source/beleid/" text="Beleid Open Source - Digitale Overheid ( Dutch )" htmlproofer_ignore="false" >}}
