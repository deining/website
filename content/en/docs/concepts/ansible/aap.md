---
categories: ["Concept"]
tags: [ansible,platform,awx,aap,redhat]
title: "Ansible Automation Platform ( AAP )"
linkTitle: "Automation Platform"
weight: 1
description: >
  AAP is a powerful and open-source automation platform that consists of two key
  components: the Automation Controller (AWX) and the Ansible Automation Hub
  (Galaxy NG).
---


{{< under_construction >}}

<!-- TODO finish -->

* How-to section [Ansible Automation Platform ( AAP ) and AWX]({{< relref path="/docs/howto/awx/" >}})
* {{< external-link url="https://www.redhat.com/en/technologies/management/ansible" text="Red Hat Ansible Automation Platform" htmlproofer_ignore="false" >}}
* {{< external-link url="https://www.ansible.com/products/awx-project/faq" text="The AWX Project" htmlproofer_ignore="false" >}}
