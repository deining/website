---
categories: ["Concept"]
tags: [ansible]
title: "Ansible Galaxy"
linkTitle: "Galaxy"
weight: 2
description: >
  Galaxy is a central place for discovering and sharing Ansible content.
---

The {{< external-link url="https://galaxy.ansible.com" text="Ansible Galaxy" htmlproofer_ignore="true" >}} website is used for distributing [Ansible Collections]({{< relref path="./projects/collections" >}}) and [Roles]({{< relref path="./projects/roles.md" >}}), which are the fundamental building blocks of an Ansible automation project. It allows you to easily find pre-built Collections and Roles created by the Ansible community. These reusable components provide a convenient way to leverage existing automation solutions and accelerate the development of your own project.

The Galaxy platform enables you to search for content based on various criteria such as popularity, relevance, and quality. You can explore a wide range of Collections and Roles contributed by community members, making it easier to benefit from their expertise and save time during development.

To use content from Ansible Galaxy, you can manually download it or directly integrate it into your Ansible playbooks using the ansible-galaxy CLI. This tool simplifies the installation and management of Collections and Roles, allowing you to seamlessly integrate them into your automation workflows.

By harnessing the power of Ansible Galaxy, you can take advantage of an extensive ecosystem of community-driven content, collaborate with other users, and build upon existing solutions to achieve efficient and scalable automation.
<!-- TODO link / explain ansible configuration, playbook project -->
<!-- TODO requirements.yml -->

* [C2 Platform Collections]({{< ref path="/tags/collection/" lang="en" >}})
*  {{< external-link url="https://galaxy.ansible.com/c2platform" text="C2 Platform Collections on the Galaxy website" htmlproofer_ignore="true" >}}
