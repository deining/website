---
categories: ["Term"]
tags: [ansible, inventory, playbook, configuration, secret_vars, group_vars, host_vars, requirements.yml, galaxy]
title: "Ansible Inventory Project"
linkTitle: "Inventory"
weight: 1
description: >
  An Ansible Inventory project contains inventory files, plays, host configurations, group variables, and vault files. It is also referred to as a playbook project or configuration project.
---

Examples of such projects include
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}) and
[`c2platform/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}).
These projects are structured to be used and consumed by
[AAP / AWX]({{< relref path="../aap.md" >}}).

Within
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})
, you will find:

1. `hosts-dev.ini`: a file containing host configurations.
1. `group_vars`directory: stores group variables.
1. `plays` directory: contains Ansible plays/playbooks.
1. `secret_vars` directory: a dedicated location for storing secrets. For further details on managing secrets using [Ansible Vault and AAP / AWX]({{< relref path="/docs/guidelines/setup/secrets" >}}), please refer to the documentation.
1. `collections/requirements.yml` file: utilized by [AAP / AWX]({{< relref path="../aap.md" >}}) to install [Ansible Collections]({{< relref path="./collections" >}}) from [Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}).
1. `roles/requirements.yml` file: similar to collections/requirements.yml, this file is used by [AAP / AWX]({{< relref path="../aap.md" >}}), specifically for installing [Ansible Roles]({{< relref path="./roles" >}}) from [Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}).

For further reference, explore the following guidelines:

* [Variable Prefix]({{< relref path="/docs/guidelines/coding/var-prefix" >}} "Guideline: Variable prefix")
* [Group-based environments]({{< relref path="/docs/guidelines/setup/group-based-environments" >}} "Guideline: Group-based environments")
* [Clone Script]({{< relref path="/docs/guidelines/setup/clone" >}} "Guideline: Clone Script")
* [Managing Dictionary Merging]({{< relref path="/docs/guidelines/coding/merge" >}})