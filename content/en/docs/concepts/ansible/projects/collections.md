---
categories: ["Term"]
tags: [ansible, collection, galaxy, .gitlab-ci.yml]
title: "Ansible Collection Project"
linkTitle: "Collection"
weight: 2
description: >
  An Ansible Collection project is a comprehensive unit that combines modules, plugins, roles, and documentation to enhance the automation language and manage infrastructures. It serves as a reusable and distributable package of Ansible content.
---

An Ansible Collection brings together modules, plugins, and roles to expand the
capabilities of Ansible for infrastructure management. It is a standardized
format for packaging and sharing Ansible content. These collections can be
accessed and shared through the
[Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}) website.

Examples of such projects include
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})
and
[c2platform.gis]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}).

These projects are designed to make it easy for users to consume and integrate
Ansible Collections into their workflows. For a complete list of [C2 Platform
Collections]({{< ref path="/tags/collection/" lang="en" >}}), please visit the

[Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}) website: {{< external-link url="https://galaxy.ansible.com/c2platform" text="C2 Platform Collections on the Galaxy website" htmlproofer_ignore="false" >}}.

More information about Ansible collections is available on the Ansible website see
{{< external-link url="https://docs.ansible.com/ansible/latest/dev_guide/developing_collections.html" text="Developing collections - Ansible Documentation" htmlproofer_ignore="true" >}}

The [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}) project consists of the following components:

1. `galaxy.yml`: metadata file describing the collection, including name,
   version, dependencies, and supported platforms.
1. `meta` directory: stores collection-specific metadata files, such as
   dependencies and tags.
1. `plugins` directory: houses custom plugins or modules developed for the
   collection.
1. `roles` directory: includes reusable roles that can be utilized within
   playbooks.
1. `tests` directory: encompasses tests to ensure the collection's
   functionality.
1. `CHANGELOG.md`: a file documenting the version history and changes made to
   the collection.
1. `README.md`: provides essential information about the collection, including
   installation instructions, usage examples, and additional resources.
1. `.gitlab-ci.yml`: see
    [CI/CD Pipelines for Ansible Collections]({{< relref path="/docs/tutorials/git-workflow/6-cicd/gitcicd/collections" >}})
    for more information.

These projects are designed to integrate Ansible Collections and empower users
to leverage pre-built automation content for various use cases.
