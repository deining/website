---
categories: ["Concept"]
tags: [ansible, git, project]
title: "Ansible Projects"
linkTitle: "Projects"
weight: 3
description: >
  Ansible Inventory, Ansible Collection, Ansible Role, and Ansible Execution Environment are different types of projects related to Ansible. This section provides an overview of each project type and their significance in the Ansible ecosystem.
---