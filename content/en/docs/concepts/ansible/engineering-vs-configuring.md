---
categories: ["Concept"]
tags: [concept, automation, ansible, engineering, operating]
title: "Engineering vs Operating"
linkTitle: "Engineering vs Operating"
weight: 6
description: >
  Operating Ansible, developing roles and collections Ansible and using Ansible for ad-hoc tasks are separate disciplines.
---

1. **Ansible Role/Collections Developer (Ansible Engineering)**: This role involves the development, testing, and publishing of Ansible roles and collections. It requires software development skills and knowledge of Ansible's role and collection structure, syntax, and best practices. Ansible roles and collections are reusable units of automation that can be shared with the community through platforms like Ansible Galaxy. Developers in this discipline focus on creating and maintaining high-quality roles and collections that provide specific functionalities or configurations for various systems.
2. **Ansible Operator (Ansible User)**: This role involves using Ansible to create and manage playbook projects, including plays, inventory, group_vars, host_vars, and dependencies like collections and roles. Ansible Operators are responsible for writing playbooks that define the desired state of the systems, orchestrating tasks, managing configurations, and automating various operations. They may also configure Ansible Automation Platform job templates and workflows for more complex automation workflows. This discipline focuses on utilizing Ansible to achieve specific automation goals without necessarily developing or engineering the underlying roles and collections.
3. **Ansible Ad-hoc**: This discipline involves using Ansible ad-hoc commands. Ad-hoc commands allow users to quickly perform one-time tasks without the need for writing and maintaining playbooks. Ad-hoc commands are often used for gathering system information, running simple commands, or performing ad-hoc operations on remote systems. This discipline requires a good understanding of Ansible command syntax, available modules and their options, as well as the configuration of the target systems.

It's important to note that these roles are not rigidly defined, and individuals often have overlapping skills and responsibilities. However, the Ansible community recognizes the distinction between those who primarily focus on developing and publishing roles/collections and those who primarily use Ansible to automate tasks and manage infrastructure.
