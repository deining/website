---
categories: ["How-to"]
tags: [ubuntu, grub, visudo, root, lxd]
title: "Ubuntu Configuration"
linkTitle: "Ubuntu"
weight: 1
description: >
  Disable Secure Boot, configure Grub for LXD and Enabling Root Access without Password.
---

---
This guide provides instructions for configuring Grub to support [LXD]({{< relref path="../../../concepts/dev/lxd/" >}}) and enabling root access without requiring a password. While enabling root access without a password is not strictly necessary, it can be helpful for convenience, especially on a development laptop.

---

## Secure Boot

Make sure that **Secure Boot** is disabled.

## Grub

{{< alert title="Note:" >}}This step is required only for Ubuntu 22.04. It is not necessary for Ubuntu 18.04.{{< /alert >}}

Open the `/etc/default/grub` file using a text editor.

```bash
sudo nano /etc/default/grub
```

Find the line that starts with `GRUB_CMDLINE_LINUX` and modify it as follows:

```bash
GRUB_CMDLINE_LINUX="cgroup_enable=memory swapaccount=1 systemd.unified_cgroup_hierarchy=0"
```

Save the file and exit the text editor. Update Grub to apply the changes.

```bash
sudo update-grub
```

Reboot your system for the changes to take effect.

```bash
sudo reboot
```

Without making this edit, the network on CentOS 7 boxes may not function correctly, and the containers may fail to obtain an IP address. If you encounter the following error message during Vagrant provisioning, this edit is necessary:

> The container failed to acquire an IPv4 address within 30 seconds.

## Enabling Root Access without Password

Some tasks require executing commands as the root user without entering a password. Follow these steps to enable root access without a password:

Open the `/etc/sudoers` file using the visudo command.

```bash
sudo visudo
```

Add the following line to the file, replacing `myuser` with your actual username:

```
myuser ALL=(ALL) NOPASSWD:ALL
```

Save the file and exit the text editor.

Enabling root access without a password is particularly useful for tasks that run on localhost and require root privileges. However, exercise caution when granting such access and ensure that you understand the implications.
