---
categories: ["How-to"]
tags: [gitlab, ssh]
title: "Create SSH Keys and Create and Setup a GitLab account"
linkTitle: "GitLab Account and SSH Keys"
weight: 2
description: >
    Learn how to use SSH to access C2 Platform GitLab repositories, the recommended
    and most convenient protocol for Git repositories.
---

{{< alert title="Note:" >}}
If you're not planning to contribute changes as a C2 Platform engineer and don't
have or need write access to C2 repositories, you can skip this step and use
HTTPS instead of SSH to access repositories.
{{< /alert >}}

## Create SSH Keys

Generate a pair of RSA keys (public and private) with the following command. It's
recommended to use a passphrase for extra security.

```bash
ssh-keygen -t rsa -b 4096 -C "tony.clifton@dev.c2platform.org"
```

## Create a GitLab Account

1. Navigate to
  {{< external-link
  url="https://gitlab.com"
  text="gitlab.com"
  htmlproofer_ignore="true" >}}
  and sign up for an account.
2. Add your SSH public key: go to user **Settings** →
  {{< external-link url="https://gitlab.com/-/profile/keys"
  text="SSH Keys"
  htmlproofer_ignore="true" >}}.

## Verify SSH Setup

To verify that SSH is working correctly, execute the following commands:

```bash
cd /tmp
git clone git@gitlab.com:c2platform/ansible.git
```
