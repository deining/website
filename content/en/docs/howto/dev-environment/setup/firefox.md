---
categories: ["How-to"]
tags: [proxy, firefox, profile, ssh]
title: "Configure a FireFox Profile"
linkTitle: "FireFox"
weight: 8
description: >
  Setup a FireFox profile for easy access to the environment using a browser.
---

---
This how-to guide explains how to access your development environment using the
forward proxy running on `c2d-rproxy1`. It is strongly recommended to create a
separate Firefox profile for this purpose. This setup will utilize the forward
proxy created in the previous step
[Create the Reverse Proxy]({{< relref path="/docs/howto/dev-environment/setup/rproxy1" >}}).

---

## Create a Profile

To keep your development environment separate from other profiles and your
default browsing, it's advisable to use a Firefox profile. On Ubuntu, you can
launch the **Firefox Profile Manager** by using the following command:

```bash
firefox -no-remote -P
```

Alternatively, you can enter `about:profiles` in the address bar to access the
Firefox Profile Manager.

## Configure Network Settings

Once you have created a separate Firefox profile, open Firefox using that
profile and configure the **Network Settings** as shown below:

| Property                      | Value       |
|-------------------------------|-------------|
| Manual proxy configuration    | ✔           |
| HTTP Proxy                    | `1.1.4.205` |
| Port                          | `1080`      |
| Also use this proxy for HTTPS | ✔           |


## Import the Root CA Certificate

In the **View Certificates** section, select **Import** and import the root CA
certificate `.ca/c2.crt`. If you have used the clone script mentioned in
[Setup a Development Environment on Ubuntu 22]({{< relref "/docs/howto/dev-environment/setup" >}} "How-to: Setup a Development Environment on Ubuntu 22")
, you should find the certificate in the `~/git/gitlab/c2/ansible-dev/.ca/c2` folder.

## Create a Launcher

For your convenience, you can create a separate launcher so that you don't have
to start Firefox from a terminal window. The following command will create a
separate launcher named `firefox-profile-manager.desktop` for Firefox, with an
additional menu option, **Profile Manager**, that executes the command `firefox
-no-remote -P`.

```bash
curl https://gitlab.com/c2platform/ansible/-/raw/master/doc/howto-socks-proxy/firefox-profile-manager.desktop  --output /tmp/firefox-profile-manager.desktop && sudo desktop-file-install /tmp/firefox-profile-manager.desktop
```

## Verify

Assuming you have created `c2d-rproxy` as per the previous steps outlined in
[Creating and Provisioning the Reverse and Forward Proxy]({{< relref "/docs/howto/dev-environment/setup/rproxy1" >}})
, you should be able to navigate to
{{< external-link url="https://c2platform.org/is-alive" htmlproofer_ignore="true" >}}.
This should display the following message, and you shouldn't encounter any
security or certificate warnings since the certificate should be trusted:

> Apache is alive

