---
categories: ["How-to"]
tags: [ubuntu, lxd, pre-commit, git]
title: "Setup Project Directories and Install Ansible"
linkTitle: "Project and Ansible"
weight: 3
description: >
  Clone the Git repositories and install Ansible and Ansible Galaxy Roles and Collections.
---

Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

## Customize `~/.bashrc`

Enhance your `~/.bashrc` file with the following additions:

```bash
export GIT_USER='tclifton'
export GIT_MAIL='tony.clifton@dev.c2platform.org'
alias python='python3'
alias pip='pip3'
alias c2-env='source ~/.virtualenv/c2d/bin/activate'
alias c2-home='cd ~/git/gitlab/c2/ansible-dev'
alias c2='c2-home && c2-env'
alias c2-roles='c2 && ansible-galaxy install -r roles/requirements.yml --force --no-deps -p roles/external'
alias c2-collections='c2 && ansible-galaxy collection install -r collections/requirements.yml -p .'
export C2_HTTPS_SSH=:-https  # ssh
```

If you want to use SSH and not HTTPS to access the GitLab Git repositories you
should change the line for `C2_HTTPS_SSH` for SSH access:

```bash
export C2_HTTPS_SSH=:-ssh
```

If you intend to contribute changes you should add a line that will enable the
`ansible-lint` and `yamllint` linters to run by creating a `pre-commit` hook:

```bash
export C2_LINTERS=Y
```

For additional information on linters refer to:

* [Linters]({{< relref path="/docs/howto/dev-environment/linters" >}})
* [Clone script]({{< relref path="/docs/guidelines/setup/clone" >}})

## Clone Projects

Utilize the `clone.sh` script to set up project directories:

```bash
sudo apt install git curl -y
curl -s -L https://gitlab.com/c2platform/ansible/-/raw/master/clone.sh | bash
```

{{< alert title="Note:" >}}
The clone script is idempotent, allowing you to rerun it after updating settings
like `C2_LINTERS` and the protocol via `C2_HTTPS_SSH`.

{{< /alert >}}

## Set Up Python Virtual Environment

Create a Python 3 virtual environment for
[Ansible]({{< relref path="/docs/concepts/ansible/" >}})
for stability and flexibility:

```bash
sudo apt update
sudo apt install virtualenv -y
mkdir ~/.virtualenv
DEB_PYTHON_INSTALL_LAYOUT='deb' virtualenv ~/.virtualenv/c2d -p python3
```

## Install Ansible

Install Ansible in the virtual environment `c2d`:

```bash
source ~/.bashrc
c2
pip install -r requirements.txt
```

## Install Ansible Collections / Roles

Utilize the `c2-roles` and `c2-collections` aliases to install Ansible Collections and Roles:

```bash
c2-roles
c2-collections
```
