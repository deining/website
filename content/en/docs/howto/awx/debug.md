---
categories: ["How-to"]
tags: [ansible, galaxy, kubernetes, debug]
title: "Troubleshooting Automation Hub (Galaxy NG) Kubernetes Deployments"
linkTitle: "Automation Hub Troubleshooting"
weight: 25
description: >
   Gain expertise in deploying debug containers to effectively troubleshoot and
   resolve any issues related to Ansible Automation Hub (Galaxy NG) Kubernetes
   deployments. This section equips you with the tools to address configuration
   challenges and ensure smooth operations.
---

Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}),
[`c2platform/docker/debug`]({{< relref path="/docs/gitlab/c2platform/docker/debug" >}}),
[`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})

---
The
Ansible inventory project
[`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})
 holds the configuration for the Ansible Automation Hub ( Galaxy NG ). Within this project, you'll find settings to deploy three distinct types of debug containers. These containers serve as practical examples, showcasing how, for debugging and troubleshooting purposes, we augment Kubernetes deployments with additional containers within a specific namespace. This approach enhances our efficiency when addressing various tasks related to debugging and maintenance.

---


## Overview

Visualize the architecture for deploying debug containers in the Ansible Automation Hub ( Galaxy NG ) namespace:

{{< plantuml id="kubernetes" width="700px">}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

'LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <logos/kubernetes>
!include <office/Devices/workstation>
!include <cloudogu/tools/ansible>
!include <tupadr3/devicons/terminal>
!include <tupadr3/material/dashboard>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("light", $textColor="#93c7fb")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddContainerTag("rproxy", $sprite="reverse_proxy", $legendText="reverse-proxy")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")
AddContainerTag("xtop", $sprite="workstation", $legendText="virtual-desktop")
AddContainerTag("ansible_container", $sprite="ansible", $legendText="ansible")
AddContainerTag("terminal", $sprite="terminal", $legendText="terminal")
AddContainerTag("dashboard", $sprite="dashboard", $legendText="dashboard")
Person(engineer, "Engineer")

System_Boundary(local, "c2platform.org") {
   Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1,lxd", $tags="rproxy")
   Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
      Boundary(galaxy_namespace, "galaxy", $type="namespace") {
         Container(galaxy, "Automation Hub", "", $tags="ansible_container")
         Container(debug_gui, "Debug Desktop", "", $tags="xtop")
         Container(dashboard, "K8s Dashboard", "", $tags="dashboard")
         Container(debug_cli, "Debug CLI", "", $tags="terminal")
         Container(ansible_exe, "Execution Environment", "", $tags="ansible_container")
      }
   }
}

Rel(engineer, c2d_rproxy1, "galaxy.c2platform.org", "https,port 443")
Rel(c2d_rproxy1, galaxy, "", "")
Rel(engineer, debug_gui, "Using RDP connection", "xrdp,3389", $tags="light")
Rel(engineer, dashboard, "Via Kubernetes Dashboard\ngalaxy-dashboard.c2platform.org", "https,port 443")
Rel(debug_gui, galaxy, "Debug using\nFireFox\n(and CLI utilities)", "", $tags="light")
Rel(debug_cli, galaxy, "Debug using\nCLI utilities", "", $tags="light")
Rel(ansible_exe, galaxy, "Debug using\nAnsible", "", $tags="light")
Rel(dashboard, debug_cli, "Exec into pod", $tags="light")
Rel(dashboard, ansible_exe, "", $tags="light")

SHOW_LEGEND(false)

{{< /plantuml >}}

