


|Property|Value                                                                                                                                                                                |
|--------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|Name    |`c2-api-group`                                                                                                                                                                       |
|Roles   |`galaxy.collection_admin` `galaxy.collection_curator`  `galaxy.collection_namespace_owner` `galaxy.collection_publisher` `galaxy.execution_environment_admin` `galaxy.synclist_owner` `galaxy.content_admin`|


|Property |Value                               |
|---------|------------------------------------|
|Username |`c2-api-user`                       |
|Firstname|`Onno`                              |
|Last name|`van der Straaten`                  |
|Email    |`onknows+c2d-galaxy1@c2platform.org`|
|Password |`Supersecret`                       |
|Groups   |`c2-api-group`                      |


Token aangemaakt `59069566d288f51c6e1133d0e8fc83ce981fd372`