---
categories: []
tags: []
title: "Ansible Automation Platform ( AAP )"
linkTitle: "AAP"
weight: 9
description: >
  This section provides comprehensive instructions for creating, managing, and
  harnessing the power of the Ansible Automation Platform (AAP). AAP comprises two
  key components: the Automation Controller (AWX) and the Ansible Automation Hub
  (Galaxy NG).
---


Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}})

---

{{< plantuml id="kubernetes" width="800px">}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

'LAYOUT_WITH_LEGEND()
'SHOW_LEGEND(false)

HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
!include <cloudogu/tools/ansible>
!include <logos/kubernetes>

AddElementTag("lxd", $legendText="LXD Container / VM", $bgColor="#1168bd")
AddElementTag("k8s_container", $legendText="K8s POD", $bgColor="#8fc5fa", $fontColor="#0e3962")
AddRelTag("light", $textColor="#93c7fb")
AddRelTag("ansible", $sprite="ansible", $legendText="ansible provision")
UpdateRelStyle("#042a4f", "#042a4f")

AddContainerTag("lxd2", $sprite="virtual_server", $legendText="LXD container", $bgColor="#1168bd")
AddSystemTag("ansible", $sprite="ansible", $legendText="ansible-galaxy", $bgColor="#999999")
AddContainerTag("k8s", $sprite="kubernetes", $legendText="k8s-cluster", $bgColor="#1168bd")

System_Boundary(local, "c2platform.org") {
   Boundary(aap, "Ansible Automation Platform ( AAP )") {
    Container(c2d_galaxy_k8s, "Kubernetes", "c2d-galaxy1,lxd", $tags="k8s") {
        Container(galaxy, "Automation\nHub", "", $tags="")
    }
    Container(c2d_awx_k8s, "Kubernetes", "c2d-awx1,lxd", $tags="k8s") {
        Container(awx, "Automation Controller", "", $tags="")
    }
   }
}

System_Ext(galaxy_website, "galaxy.ansible.com", "Stores all open source Ansible automation \n( collections, roles )", $tags="ansible")

Rel_R(awx, galaxy, "Download\napproved / curated\nAnsible automation", "")
Rel_R(galaxy, galaxy_website, "Download Ansible automation")

{{< /plantuml >}}
