---
categories: ["How-to"]
tags: [vagrant, ansible, certificates, ca, ownca]
title: "Managing Server Certificates as a Certificate Authority"
linkTitle: "Manage Certificates as CA"
weight: 10
description: >
  Learn how to become your own Certificate Authority (CA) and manage certificates for various services using the `cacerts2` Ansible role.
---

Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

{{< under_construction >}}

<!-- TODO finish -->

---
This guide explains the process of establishing a simple Certificate Authority (CA) and effectively managing certificates for different services. It utilizes the `cacerts2` Ansible role from the [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}) collection.

---

## Configuration

One node is assigned the **CA Server** role where certificates are created and stored. In this project **c2d-rproxy1** is assigned this role. This done in `hosts-dev.ini` in [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}).

```yaml
[cacerts_server]
c2d-rproxy1
```

The configuration for the CA server is in `group_vars/all/smallca.yml`.

```yaml
cacerts2_ca_server: "{{ groups['cacerts_server'][0] }}"
c2_cacerts2_ca_dir:
  default: /etc/ownca
  development: /vagrant/.ca
cacerts2_ca_dir: "{{ c2_cacerts2_ca_dir[c2_env]|default(c2_cacerts2_ca_dir['default']) }}"
cacerts2_ca_domain:
  common_name: c2
  cipher: auto
  passphrase: "{{ c2_cacerts2_ca_domain_passphrase }}"  # secret see vault
  create: ['key','csr', 'crt', 'p12', 'pem']
```

The variable `c2_env` is defined in `group_vars/development.yml`. Variables prefixed with `c2_` are project variables and not role variables. See [Variable prefix]({{< relref path="/docs/guidelines/coding/var-prefix.md" >}} "Guideline: Variable Prefix").


## Create CA Server

In `Vagrantfile.yml` the CA server `c2d-rproxy1` is defined as follows:

```yaml
  - name: rproxy1
    short_description: Reverse Proxy
    description: Apache based reverse proxy
    box: ubuntu18-lxd
    ip-address: 1.1.4.205
    plays:
      - core/cacerts_server
      - mw/reverse_proxy
```

Note that this node has two `plays` one of which is `core/cacerts_server`.

```bash
vagrant up c2d-rproxy1
```

Note: this project contains a `.ca` folder with the CA files as shown below. The ( CA ) keys and certificates are stored in the root of project in folder `.ca`. That way the CA key and certificate can be reused as you destroy and create nodes.

```
.ca/
└── c2
    ├── c2.crt
    ├── c2.csr
    └── c2.key
```

Note: if you remove this `.ca` folder those CA files will be recreated by the `plays/core/cacerts_server` play. This is something you might not want because for example if you imported `.ca/c2/c2.crt` in your internet browser.

## Reverse Proxy

Because `c2d-rproxy1` has two plays configured we now also have created a reverse proxy server with the `plays/mw/reverse_proxy.yml` play. Running this play created certificates signed by our small CA. These should now also be present in the `.ca` folder as shown below. Note: you can remove those certificates, the certificates in `apache` folder at any time because they will be recreated when `plays/mw/reverse_proxy.yml` play is executed. The creation of the reverse proxy servers is explained in more detail in [Setup Reverse Proxy and CA server]({{< relref path="/docs/howto/c2/reverse-proxy.md" >}} "How-to: Setup Reverse Proxy and CA server").

```
.ca/
└── c2
    ├── apache
    │   ├── c2-c2d-rproxy1.crt
    │   ├── c2-c2d-rproxy1.csr
    │   ├── c2-c2d-rproxy1.key
    │   ├── c2-c2d-rproxy1.p12
    │   └── c2-c2d-rproxy1.pem
    ├── c2.crt
    ├── c2.csr
    └── c2.key
```