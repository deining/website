---
categories: ["How-to"]
tags: [socks, proxy, firefox, profile, ssh]
title: "Setup SOCKS proxy"
linkTitle: "SOCKS proxy"
weight: 3
description: >
  Setup a SOCKS proxy and Firefox profile for easy access to the development
  environment.
---

---
This how-to guide explains how to access the development environment using a
{{< external-link url="https://en.wikipedia.org/wiki/SOCKS" text="SOCKS proxy" >}},
which offers an alternative method for accessing the environment via a web
browser. This is an alternative to using the default simple HTTP(s) forward
proxy, as described in
[Create the Reverse Proxy]({{< relref path="/docs/howto/dev-environment/setup/rproxy1" >}})
and
[Configure a FireFox Profile]({{< relref path="/docs/howto/dev-environment/setup/firefox" >}}).
The SOCKS proxy is established through an SSH connection to `c2d-rproxy1` and
provides more advanced protocol support beyond just HTTP and HTTPS.

---

<!-- include-start: howto-prerequisites.md -->
## Prerequisites

Create the reverse and forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # ensure all plays run
vagrant up c2d-rproxy1
```

For more information about the various roles that `c2d-rproxy1` performs in this project:

* [Setup Reverse Proxy and CA server]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Setup SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Managing Server Certificates as a Certificate Authority]({{< relref path="/docs/howto/c2/certs">}})
* [Setup DNS for Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})
<!-- include-end -->

## Overview

{{< plantuml id="awx-example" width="300px">}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")

Boundary(local, "local", $type="high-end dev laptop") {
    Container(ssh, "SSH", "")
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1")
    Container(c2d_gitlab, "GitLab", "c2d-gitlab")
    Container(c2d_awx, "AWX", "c2d-awx")
    Container(firefox, "FireFox", "")
}

Rel(ssh, c2d_rproxy1, "SOCKS proxy \nover SSH", "")
Rel(engineer, firefox, "https://gitlab.c2platform.org\nhttps://awx.c2platform.org\netc.", "")
Rel(engineer, ssh, "Create proxy", "")
Rel_Left(firefox, ssh, "", "")
Rel(c2d_rproxy1, c2d_gitlab,"","")
Rel(c2d_rproxy1, c2d_awx,"","")
{{< /plantuml >}}

## SSH Configuration

To set up the SOCKS proxy, add the following section to your `.ssh/config` file:

```
Host c2d_socks
  User vagrant
  IdentityFile ~/.vagrant.d/insecure_private_key
  StrictHostkeyChecking no
  UserKnownHostsFile /dev/null
  Hostname 1.1.4.205
  LogLevel INFO
  Compression yes
  DynamicForward 9903
  ServerAliveInterval 10
  ServerAliveCountMax 10
```

## Starting the SOCKS Proxy

Now, you can start the SOCKS proxy using the following command:

```bash
ssh c2d_socks
```

## Creating a Firefox Profile

Similar to
[Configure a FireFox Profile]({{< relref path="/docs/howto/dev-environment/setup/firefox" >}})
, create a Firefox profile. However, configure the **Network Settings** as shown below:

|Property                         |Value                                                                           |
|---------------------------------|--------------------------------------------------------------------------------|
|Automatic proxy configuration URL|https://gitlab.com/c2platform/ansible/-/raw/master/doc/howto-socks-proxy/c2d.pac|
|Proxy DNS when using SOCKS v5    |✔                                                                               |
|Enable DNS over HTTPS            |✔                                                                               |

## Verification

Navigate to {{< external-link url="https://c2platform.org/is-alive" htmlproofer_ignore="true" >}}.
If everything is set up correctly, you should see the following message, and
there should be no security or certificate warnings since the certificate should
be trusted:

> Apache is alive

