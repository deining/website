---
categories: ["How-to"]
tags: [gitlab, token]
title: "Setup GitLab"
linkTitle: "GitLab"
weight: 1
description: >
  Create GitLab instance `c2d-gitlab`.
---


Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})

---
This how-to describes how to create local GitLab instance `c2d-gitlab`. The play `plays/mgmt/gitlab.yml` uses the Ansible role {{< external-link url="https://galaxy.ansible.com/geerlingguy/ansible" text="geerlingguy.gitlab" >}} to create it. The configuration for this node is in `group_vars/gitlab/main.yml`. After performing the steps in this how-to you should be able to navigate to {{< external-link url="https://gitlab.c2platform.org" htmlproofer_ignore="true" >}} and login as `root` with the password in `/etc/gitlab/initial_root_password`.

---

<!-- include-start: howto-prerequisites.md -->
## Prerequisites

Create the reverse and forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # ensure all plays run
vagrant up c2d-rproxy1
```

For more information about the various roles that `c2d-rproxy1` performs in this project:

* [Setup Reverse Proxy and CA server]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Setup SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Managing Server Certificates as a Certificate Authority]({{< relref path="/docs/howto/c2/certs">}})
* [Setup DNS for Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})
<!-- include-end -->

## Overview

{{< plantuml id="awx-example" width="300px">}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")

Boundary(local, "local", $type="high-end dev laptop") {
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1")
    Container(c2d_gitlab, "GitLab", "c2d-gitlab")
}

Rel(engineer, c2d_rproxy1, "https://gitlab.c2platform.org")
Rel_Right(c2d_rproxy1, c2d_gitlab, "")
{{< /plantuml >}}

## Setup

```bash
vagrant up c2d-gitlab
```

## Root password

The GitLab installer creates a password for the `root` account in a file `/etc/gitlab/initial_root_password`. The file will be deleted after 24 hours.

```bash
vagrant ssh c2d-gitlab -c "sudo cat /etc/gitlab/initial_root_password | grep Password:"
```

Via {{< external-link url="https://gitlab.c2platform.org/-/profile/password/edit" text="Profile" htmlproofer_ignore="true" >}} change password equal to `supersecret`.

## Access token

Via **Profile** → {{< external-link url="https://gitlab.c2platform.org/-/profile/personal_access_tokens" text="Access Tokens" htmlproofer_ignore="true" >}} create an access token.

## Verify

Start SOCKS proxy

```bash
ssh c2d_socks
```

You should be able to navigate to {{< external-link url="https://gitlab.c2platform.org" htmlproofer_ignore="true" >}}  and login as `root`.

## Links

* {{< external-link url="https://about.gitlab.com/install/" text="Download and install GitLab" >}}
* {{< external-link url="https://docs.gitlab.com/ee/topics/offline/quick_start_guide.html" text="Getting started with an offline GitLab Installation" >}}
* {{< external-link url="https://galaxy.ansible.com/geerlingguy/ansible" text="geerlingguy.gitlab" >}}
