---
categories: ["How-to"]
tags: [gitlab, container, registry, ansible, vagrant]
title: "Setup GitLab Container Registry"
linkTitle: "GitLab Container Registry"
weight: 2
description: >
  Create GitLab Container Registry on node `c2d-gitlab`.
---

---
This how-to describes how create and use the [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/). This registry is not enabled by default in **GitLab CE** but it is enabled on default in this project by Ansible see `group_vars/gitlab/registry.yml`

---


## Prerequisites

* [Setup GitLab]({{< relref "gitlab.md" >}} "How-to: Setup GitLab")

## Overview

In this project we access the GitLab registry using URL {{< external-link url="https://registry.c2platform.org" htmlproofer_ignore="true" >}} via reverse proxy running on `c2d-rproxy`.

{{< plantuml id="gitlab-container-registry" width="350px">}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")

Boundary(local, "local", $type="high-end dev laptop") {
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1")
    Container(c2d_gitlab, "GitLab", "c2d-gitlab")
}

' Rel(engineer, c2d_rproxy1, "https://gitlab.c2platform.org")
Rel(engineer, c2d_rproxy1, "https://registry.c2platform.org")
Rel_Right(c2d_rproxy1, c2d_gitlab, "")
{{< /plantuml >}}

## Start GitLab

Start GitLab instance created using [Setup GitLab]({{< relref "gitlab.md" >}} "How-to: Setup GitLab").

```bash
vagrant up c2d-gitlab
```

## Verify

If create a GitLab project and navigate to **Packages and registries** you should see three options one of which is **Container Registry**.

### Login and Push

To verify that the registry is working, you can try to push an image to it. In example below we use the node `c2d-gitlab-runner` for this purpose.

```bash
vagrant up c2d-gitlab-runner
```

Now we can test if we can login to the registry:

```bash
vagrant@c2d-gitlab-runner:~$ sudo su -
root@c2d-gitlab-runner:~# docker login registry.c2platform.org
Username: root
Password:
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
root@c2d-gitlab-runner:~#
```

Download an image for example `docker pull ubuntu` tag it and then push it to a Gitlab project. For example if we created project {{< external-link url="https://gitlab.c2platform.org/c2platform/gitlab-docker-build" text="c2platform/gitlab-docker-build" htmlproofer_ignore="true" >}} as part of [Setup GitLab]({{< relref "gitlab.md" >}} "How-to: Setup GitLab").

```
docker tag ubuntu:latest registry.c2platform.org/c2platform/gitlab-docker-build:latest
docker push registry.c2platform.org/c2platform/gitlab-docker-build:latest
```

### Pipeline

If you performs the steps in [How-to Running GitLab CE pipelines in Kubernetes](https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes-gitlab-local.md) this also verifies the GitLab Container Registry is enabled and working correctly. TODO

## Links

* [plays/mgmt/gitlab.yml](https://gitlab.com/c2platform/ansible/-/blob/master/plays/mgmt/gitlab.yml)
* [geerlingguy.gitlab](https://galaxy.ansible.com/geerlingguy/ansible)
* [GitLab Container Registry | GitLab](https://docs.gitlab.com/ee/user/packages/container_registry/)
* [GitLab Container Registry administration | GitLab](https://docs.gitlab.com/ee/administration/packages/container_registry.html#configure-container-registry-under-its-own-domain)


* {{< external-link url="https://docs.gitlab.com/runner/install/linux-repository.html" text="Install GitLab Runner using the official GitLab repositories | GitLab" >}}
* {{< external-link url="https://github.com/robertdebock/ansible-role-gitlab_runner" text="robertdebock/ansible-role-gitlab_runner" >}}