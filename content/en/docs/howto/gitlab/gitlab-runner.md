---
categories: ["How-to"]
tags: [gitlab, token]
title: "Setup GitLab Runner"
linkTitle: "GitLab Runner"
weight: 3
description: >
  Create GitLab Runner node `c2d-gitlab-runner`.
---

---
This how-to describes how to create local GitLab instance `c2d-gitlab-runner`. The play `plays/dev/gitlab_runner.yml` uses the Ansible role {{< external-link url="https://galaxy.ansible.com/robertdebock/gitlab_runner" text="robertdebock.gitlab_runner" >}} to create it. The configuration for this node is in `group_vars/gitlab_runner/main.yml`

---

## Registration token

Navigate to GitLab project {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-robot" text="c2platform/examples/kubernetes/gitlab-robot" >}} and then **Settings** → **CI/CD** → **Runners** and copy the "registration token". Create or edit file `group_vars/all/local_stuff.yml` and configure the token for example:

TODO multiple projects in table

```yaml
c2_gitlab_runner_registration_token: GR13******  # https://gitlab.com/c2projects/cgi/azure/-/settings/ci_cd
```

See [Local Stuff]({{< relref "/docs/guidelines/dev/local-stuff" >}} "Guideline: Local Stuff") for more information about `local_stuff.yml`.

## Provision

```bash
vagrant up c2d-gitlab-runner
```

## Verify

```bash
root@c2d-gitlab-runner:~# gitlab-runner list
Runtime platform                                    arch=amd64 os=linux pid=4060 revision=dcfb4b66 version=15.10.1
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
c2d-gitlab-runner                                   Executor=shell Token=btPy-VV2HF2DxVxh1xzM URL=https://gitlab.com/
root@c2d-gitlab-runner:~#
```

## Unregister

```bash
gitlab-runner list
gitlab-runner unregister -c /etc/gitlab-runner/config.toml --url https://gitlab.com/ --token <token>
```

## Links

* {{< external-link url="https://docs.gitlab.com/runner/install/linux-repository.html" text="Install GitLab Runner using the official GitLab repositories | GitLab" >}}
* {{< external-link url="https://github.com/robertdebock/ansible-role-gitlab_runner" text="robertdebock/ansible-role-gitlab_runner" >}}
