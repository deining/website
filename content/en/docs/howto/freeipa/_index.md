


```bash
docker run -ti --name freeipa \
  --hostname freeipa.c2platform.org \
  -p 5353:53/udp \
  -p 5353:53 \
  -p 80:80 \
  -p 443:443 \
  -p 389:389 \
  -p 636:636 \
  -p 88:88 \
  -p 464:464 \
  -p 88:88/udp \
  -p 464:464/udp \
  -p 123:123/udp \
  --privileged \
  --read-only \
  --add-host registry.c2platform.org:1.1.4.205 \
  --add-host gitlab.c2platform.org:1.1.4.205 \
  --add-host freeipa.c2platform.org:1.1.4.205 \
  -v freeipa:/data \
  -e IPA_SERVER_INSTALL_OPTS="--realm=c2platform.org --ds-password=secret --admin-password=secret" \
  -e IPA_SERVER_IP=1.1.4.166 \
  quay.io/freeipa/freeipa-server:rocky-9
```