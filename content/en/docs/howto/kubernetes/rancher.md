---
categories: ["How-to"]
tags: [rancher, kubernetes, ansible, helm]
title: "Deploy Rancher on Kubernetes"
linkTitle: "Rancher"
weight: 20
description: >
    Deploy Rancher on MicroK8s
---

Projects: [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible" >}}), [`c2platform.mw`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-mw" >}})

---
This tutorial provides step-by-step instructions for deploying Rancher on MicroK8s Kubernetes on node `gsd-rancher1`. On this node Ansible will provision first MicroK8s and then deploy Rancher on it. The Kubernetes Dashboard will then be enabled. After provision Rancher will be available via {{< external-link url="https://rancher.c2platform.org" htmlproofer_ignore="true" >}} and the dashboard will be available via {{< external-link url="https://dashboard-rancher.c2platform.org" htmlproofer_ignore="true" >}}.

---

## Overview

<p>

{{< plantuml id="ansible-vagrant" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "")

Boundary(laptop, "laptop", $type="ubuntu-22") {

  Boundary(gis, "rancher.c2platform.org", $type="") {
    Container(rancher, "MicroK8s", "c2d-rancher 4.163") {
        Container(rancher_deploy, "Rancher", "")
        Container(dashboard, "Dashboard", "")
    }
    Container(rproxy, "Reverse Proxy", "c2d-rproxy1 4.205")
  }
}

Rel(engineer, rproxy, "", "")
Rel(rproxy, rancher, "", "https")
{{< /plantuml >}}

</p>

<!-- include-start: howto-prerequisites.md -->
## Prerequisites

Create the reverse and forward proxy `c2d-rproxy1`.

```bash
c2
unset PLAY  # ensure all plays run
vagrant up c2d-rproxy1
```

For more information about the various roles that `c2d-rproxy1` performs in this project:

* [Setup Reverse Proxy and CA server]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Setup SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Managing Server Certificates as a Certificate Authority]({{< relref path="/docs/howto/c2/certs">}})
* [Setup DNS for Kubernetes]({{< relref path="/docs/howto/kubernetes/dns" >}})
<!-- include-end -->

## Vagrant

Create and provision the node with the following command:

```bash
vagrant up c2d-rancher1
```

## Verify

With the reverse proxy `c2d-rproxy` provisioned and successfully running see [Getting Started]({{< relref path="/docs/howto/dev-environment/setup">}}) you should be able to navigate to {{< external-link url="https://rancher.c2platform.org" htmlproofer_ignore="true" >}} or {{< external-link url="https://dashboard-rancher.c2platform.org" htmlproofer_ignore="true" >}} and login.

## Links

* [Setup Kubernetes]({{< relref path="./kubernetes.md" >}})
* [Setup the Kubernetes Dashboard]({{< relref path="./dashboard.md" >}})
