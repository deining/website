---
categories: ["How-to"]
tags: ["how-to", "kubernetes"]
title: "Kubernetes"
linkTitle: "Kubernetes"
weight: 3
description: >
    Instructions for setup creating a local Kubernetes instance and managing it with {{< external-link url="https://www.ansible.com/" text="Ansible" htmlproofer_ignore="false" >}} and / or a {{< external-link url="https://about.gitlab.com/topics/gitops/" text="GitOps" htmlproofer_ignore="false" >}} workflow. The default is {{< external-link url="https://microk8s.io/" text="Microk8s" htmlproofer_ignore="false" >}} but {{< external-link url="https://www.rancher.com/" text="Rancher" htmlproofer_ignore="false" >}}is also supported.
---
