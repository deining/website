---
categories: ["How-to"]
tags: [TODO]
title: "Secret for SSL/TLS Certificate"
linkTitle: "Secret for SSL/TLS Certificate"
weight: 21
description: >
    Create Kubernetes Secret for SSL/TLS Certificate
---

## Create secret

To create such a Kubernetes secret first get base64 encoded value of the certificate.

```bash
cat .ca/c2/c2.crt | base64 -w 0 > /tmp/secret.txt
```

Create YAML file for example [cert.yml]({{< absURL "/downloads/c2/cert.yml" >}})

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: c2-root-cert
data:
  c2.crt: <base64-encoded certificate text>
```

Create the secret in namespace `kube-system` so it can be used by all pods in the cluster.

```bash
vagrant ssh c2d-ks1
kubectl config set-context --current --namespace=kube-system
kubectl apply -f /vagrant/doc/howto-kubernetes-cert/c2-cert.yml
```

<details>
  <summary><kbd>Show me</kbd></summary>

```yaml
vagrant@c2d-ks1:~/scripts/microk8s/gitlab/gitlab-runner$ kubectl get secrets
NAME                              TYPE                                  DATA   AGE
kubernetes-dashboard-certs        Opaque                                0      4d21h
microk8s-dashboard-token          kubernetes.io/service-account-token   3      4d21h
kubernetes-dashboard-csrf         Opaque                                1      4d21h
kubernetes-dashboard-key-holder   Opaque                                2      4d21h
c2-root-cert                      Opaque                                1      6s
```

</details>