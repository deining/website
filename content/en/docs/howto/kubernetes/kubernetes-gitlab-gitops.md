---
categories: ["How-to"]
tags: [gitlab, gitops, kubernetes, ansible, agent]
title: "Setup a GitLab GitOps workflow for Kubernetes"
linkTitle: "GitLab GitOps for Kubernetes"
weight: 3
description: >
    Create Kubernetes cluster on node `c2d-ks1` and manage it using GitLab Agent.
---

---
This how-to demonstrates how we can manage a [MicroK8s](https://microk8s.io/) Kubernetes cluster running on `c2d-ks` using a **GitLab Agent** running in the cluster. This GitLab Agent configuration and Kubernetes manifest files are in a separate project [`c2platform/examples/kubernetes/gitlab-gitops`]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-gitops/" >}}). Separate from the Ansible project [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible/" >}}) that is used to initially create the cluster using Ansible.

---
## Overview

This example project uses **GitOps workflow** to manage OpenShift resources in two namespaces `nja` and `njp`. The first namespace is for is the "acceptance" environment, the second for "production". See `manifests/staging.yml` and `manifests/production.yml`. The diagram below only shows `njp`. The setup for `nja` and `njp` is identical with the exception of the fact that `njp` is configured to pull only images with tag `production`. This project uses the Docker image from project [`c2platform/examples/kubernetes/gitlab-docker-build`]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-docker-build/" >}}).

{{< plantuml id="awx-example" width="700px">}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")

Boundary(kubernetes, "Kubernetes", $type="c2d-ks1") {
    Boundary(nja, "nja", $type="namespace") {
      Container(nj, "helloworld", "application")
    }
    Boundary(gitlab_agent, "gitlab_agent", $type="namespace") {
        Container(agent, "Gitlab Agent", "openshift")
    }
}
Boundary(gitlab, "gitlab.com/c2platform", $type="") {
    Boundary(gitlab_gitops, "examples/kubernetes/gitlab-gitops", $type="") {
        Container(gitlab_gitops_repo, "Repository", "git")
    }
    Boundary(docker, "examples/kubernetes/gitlab-docker-build", $type="") {
        Container(registry, "Registry", "registry")
    }
}

Rel(engineer, gitlab_gitops_repo, "Push manifest changes", "")
Rel(agent, gitlab_gitops_repo, "Pull manifest changes", "")
Rel(agent, nja, "Manage project / namespace", "")
Rel(nj, registry, "Pull production image", "")
{{< /plantuml >}}

## Prerequisites

* [Setup Kubernetes]({{< relref path="./kubernetes.md" >}})

## GitLab Agent

To register an **GitLab Agent** you will have to configure an "access token" for Ansible. In this "development" project these type of local secrets are stored using var `c2_gitlab_agent_access_token` in file `group_vars/all/local_stuff.yml`. See [Local Stuff]({{< relref path="/docs/guidelines/dev/local-stuff" >}}).

Create an access token see [Install Gitlab Agent]({{< relref path="./gitlab-agent.md" >}}) for more information. This how-to describes the manual install of an GitLab Agent.

For example in project [`c2platform/examples/kubernetes/gitlab-gitops`]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-gitops/" >}}) navigate to **Infrastructure** → **Kubernetes clusters**, click on **c2d-mk8s** and then select tab {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops/-/cluster_agents/c2d-mk8s?tab=tokens" text="Access tokens" htmlproofer_ignore="true" >}} and click **Create token**.

Create ( or update ) the file `group_vars/all/local_stuff.yml` and add

```yaml
c2_gitlab_agent_access_token: <access-token-of-gitlab-project>
```

## Create c2d-ks1

To create the Kubernetes node `c2d-ks1` perform following steps:

1. [Setup Kubernetes]({{< relref path="./kubernetes.md" >}})
2. [Setup the Kubernetes Dashboard]({{< relref path="./dashboard.md" >}})

## Gitlab Agent

See [Install Gitlab Agent]({{< relref path="./gitlab-agent.md" >}})

## Verify

If the **GitLab Agent** for {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops/" text="gitlab-gitops" >}} project is successfully created, a simple application is deployed that should be accessible {{< external-link url="http://1.1.4.12:3000/" htmlproofer_ignore="true" >}} and {{< external-link url="http://1.1.4.13:3000/" htmlproofer_ignore="true" >}}. See [`c2platform/examples/kubernetes/gitlab-gitops`]({{< relref path="/docs/gitlab/c2platform/examples/kubernetes/gitlab-gitops/" >}}) for more information.

With `c2d-rproxy1` running and provisioned you should be able to go to
{{< external-link url="https://frontend-nja.k8s.c2platform.org/" text="https://frontend-nja.k8s.c2platform.org/" htmlproofer_ignore="true" >}}
 and {{< external-link url="https://frontend-njp.k8s.c2platform.org/" text="https://frontend-njp.k8s.c2platform.org/" htmlproofer_ignore="true" >}} and see the message

> Hello World!

If you have the Kubernetes Dashboard add-on enabled you should be able to navigate to {{< external-link url="https://k8s.c2platform.org" text="Kubernetes Dashboard" htmlproofer_ignore="true" >}} and then see for example the service `frontend-service` in namespace `nja` with external endpoint {{< external-link url="http://1.1.4.12:3000/" htmlproofer_ignore="true" >}}


Using your browser, you can navigate to {{< external-link url="http://1.1.4.12:3000/" htmlproofer_ignore="true" >}} and see the text. Or use `curl`

```bash
vagrant@c2d-ks1:~$ curl http://1.1.4.12:3000/
Hello World! Version: 0.1.5vagrant@c2d-ks1:~$
```
