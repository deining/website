---
categories: ["How-to"]
tags: [ansible, kubernetes, dns, dnsmasq, kubectl, dig]
title: "Setup DNS for Kubernetes"
linkTitle: "DNS"
weight: 5
description: >
    Create a DNS server on `c2d-rproxy` so we can for example resolve `c2platform.org`.
---

---
[Dnsmasq](https://dnsmasq.org/) is used to create a DNS server on `c2d-rproxy`. See the node configuration in `Vagrantfile` which has the play `plays/mw/dnsmasq.yml`. DNS is a secondary function of `c2d-rproxy` see [Setup SOCKS proxy]({{< relref path="../c2/socks-proxy.md" >}}) for the two other functions. Typically, this project uses `/etc/hosts` file to resolve "domain names" see `group_vars/all/hosts.yml`, except for Kubernetes. For Kubernetes this Dnsmasq-based DNS server is employed. See the GitLab project [`c2platform/ansible`]({{< relref path="/docs/gitlab/c2platform/ansible/" >}}) for more information.

---

{{< under_construction >}}

<!-- TODO finish -->

## Prerequisites

* [Setup Reverse Proxy and CA server]({{< relref path="../c2/reverse-proxy.md" >}})
* [Setup Kubernetes]({{< relref path="./kubernetes.md" >}})

## Setup

```bash
export PLAY=plays/mw/dnsmasq.yml  # optional
vagrant provision c2d-rproxy1
```

## Verify Dnsmasq

[Dnsmasq](https://dnsmasq.org/) has been set up to provide DNS service for the `c2platform.org` domain. This configuration can be found in the `group_vars/dnsmasq/main.yml` file. Specifically, there is a single wildcard DNS record for `c2platform.org` that directs to the IP address `1.1.4.205`, which essentially means that any subdomain under `c2platform.org` will resolve to the same IP address.

You can verify that for example with:

```bash
dig @1.1.4.205 -p 5353 c2platform.org
dig @1.1.4.205 -p 5353 whatever.c2platform.org
```

<details>
  <summary><kbd>Show me</kbd></summary>

```bash
root@c2d-rproxy1:~# dig @1.1.4.205 -p 5353 c2platform.org

; <<>> DiG 9.11.3-1ubuntu1.18-Ubuntu <<>> @1.1.4.205 -p 5353 c2platform.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 54953
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;c2platform.org.                        IN      A

;; ANSWER SECTION:
c2platform.org.         0       IN      A       1.1.4.205

;; Query time: 0 msec
;; SERVER: 1.1.4.205#5353(1.1.4.205)
;; WHEN: Thu Mar 23 05:59:52 UTC 2023
;; MSG SIZE  rcvd: 59

root@c2d-rproxy1:~#
```

```bash
root@c2d-rproxy1:~# dig @1.1.4.205 -p 5353 whatever.c2platform.org

; <<>> DiG 9.11.3-1ubuntu1.18-Ubuntu <<>> @1.1.4.205 -p 5353 whatever.c2platform.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 27200
;; flags: qr aa rd ra ad; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;whatever.c2platform.org.       IN      A

;; ANSWER SECTION:
whatever.c2platform.org. 0      IN      A       1.1.4.205

;; Query time: 0 msec
;; SERVER: 1.1.4.205#5353(1.1.4.205)
;; WHEN: Thu Mar 23 06:04:21 UTC 2023
;; MSG SIZE  rcvd: 68

root@c2d-rproxy1:~#
```

</details>

## Configure CoreDNS

With our Dnsmasq-based DNS server running, we can now change CoreDNS pod in Kubernetes to use it. Edit the `coredns` `configmap` and configure `1.1.4.205:5353` as our only DNS server ( remove `8.8.8.8` etc ).

```bash
kubectl edit configmap coredns -n kube-system  # configure 1.1.4.205:5353
microk8s stop && microk8s start
microk8s status --wait-ready
```

## Verify CoreDNS

First we need to find the IP address of the CoreDNS pod and then we can run the `dig` tests again:

```bash
export COREDNS_IP=$(kubectl get pods -n kube-system | grep coredns- | awk '{print $1}' | xargs -I % sh -c 'kubectl get pod % -n kube-system -o jsonpath="{.status.podIP}"')
echo $COREDNS_IP
```

```bash
dig @$COREDNS_IP c2platform.org
dig @$COREDNS_IP whatever.c2platform.org
dig @$COREDNS_IP google.nl
```

<details>
  <summary><kbd>Show me</kbd></summary>

```bash
vagrant@c2d-ks1:~$ dig @$COREDNS_IP c2platform.org

; <<>> DiG 9.18.1-1ubuntu1.3-Ubuntu <<>> @10.1.211.147 c2platform.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 47079
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: a1154c86acbeba68 (echoed)
;; QUESTION SECTION:
;c2platform.org.                        IN      A

;; ANSWER SECTION:
c2platform.org.         5       IN      A       1.1.4.205

;; Query time: 0 msec
;; SERVER: 10.1.211.147#53(10.1.211.147) (UDP)
;; WHEN: Thu Mar 23 10:44:35 UTC 2023
;; MSG SIZE  rcvd: 85
```

```bash
vagrant@c2d-ks1:~$ dig @$COREDNS_IP whatever.c2platform.org

; <<>> DiG 9.18.1-1ubuntu1.3-Ubuntu <<>> @10.1.211.147 whatever.c2platform.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 56450
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 1b2532719222bf31 (echoed)
;; QUESTION SECTION:
;whatever.c2platform.org.       IN      A

;; ANSWER SECTION:
whatever.c2platform.org. 5      IN      A       1.1.4.205

;; Query time: 0 msec
;; SERVER: 10.1.211.147#53(10.1.211.147) (UDP)
;; WHEN: Thu Mar 23 10:45:04 UTC 2023
;; MSG SIZE  rcvd: 103

```

```bash
vagrant@c2d-ks1:~$ dig @$COREDNS_IP google.nl

; <<>> DiG 9.18.1-1ubuntu1.3-Ubuntu <<>> @10.1.211.147 google.nl
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 62105
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: ea0bc03c335aa625 (echoed)
;; QUESTION SECTION:
;google.nl.                     IN      A

;; ANSWER SECTION:
google.nl.              30      IN      A       142.250.179.163

;; Query time: 0 msec
;; SERVER: 10.1.211.147#53(10.1.211.147) (UDP)
;; WHEN: Thu Mar 23 10:45:23 UTC 2023
;; MSG SIZE  rcvd: 75
```

</details>
