---
categories: ["How-to"]
tags: [ansible, desktop, xrdp, ide, rhel, rdp, remmina, kerberos]
title: "Setup Ansible Control Node"
linkTitle: "Ansible Control Node"
weight: 10
description: >
  Setup an Ansible Control Node based on Red Hat 8 on `gsd-ansible`.
---

---
This tutorial provides step-by-step instructions for setting up an {{< external-link url="https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html#control-node" text="Ansible Control Node" htmlproofer_ignore="true" >}} on Red Hat 8, specifically on the `gsd-ansible` node. While primarily intended for **Ansible Ad-Hoc** tasks, this control node can also serve as a graphical user interface (GUI) for lightweight development work, accessible via RDP (Remote Desktop Protocol). By following these guidelines, you'll be able to create a versatile control node environment that supports both quick automation tasks and convenient development capabilities.

---

{{< alert title="Note" >}}Currently, there is no dedicated Ansible role available for creating an Ansible Control Node in the desired manner. Therefore, the Ansible play at `plays/mgmt/ansible.yml` incorporates various Ansible tasks to fulfill this requirement. However, it is worth noting that it is not a recommended practice to include tasks directly in a play. {{< /alert >}}

## Overview

The diagram below illustrates the default provisioning process in the development environment using Vagrant and Ansible. The following VMs are created: `gsd-ansible`, `gsd-agserver1` and `gsd-rproxy1`.

<p>

{{< plantuml id="ansible-vagrant" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "")

Boundary(laptop, "laptop", $type="ubuntu-22") {
  Boundary(mgmt, "mgmt", $type="") {
    Container(ansible, "Ansible", "gsd-ansible 8.105")
  }
  Boundary(gis, "gis.c2platform.org", $type="") {
    Container(agserver1, "ArcGIS Server", "gsd-server1 8.100")
    Container(rproxy, "Reverse Proxy", "gsd-rproxy1 5.205")
  }
  Container(vagrant, "Vagrant", "")
}

Rel(engineer, vagrant, "", "")
Rel(vagrant, ansible, "Provision", "ssh")
Rel(vagrant, rproxy, "Provision", "ssh")
Rel(vagrant, agserver1, "Provision", "winrm")
{{< /plantuml >}}

</p>

The Ansible Control Node, `gsd-ansible`, can be used separately to provision and manage the nodes using Ansible without relying on Vagrant.

<p>

{{< plantuml id="ansible-control-node" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "")

Boundary(laptop, "laptop", $type="ubuntu-22") {
  Boundary(mgmt, "mgmt", $type="") {
    Container(ansible, "Ansible", "gsd-ansible 8.105")
  }
  Boundary(gis, "gis.c2platform.org", $type="") {
    Container(agserver1, "ArcGIS Server", "gsd-server1 8.100")
    Container(rproxy, "Reverse Proxy", "gsd-rproxy1 5.205")
  }
  Container(vagrant, "Vagrant", "")
}

Rel(ansible, rproxy, "Provision", "ssh")
Rel(ansible, agserver1, "Provision", "winrm")
Rel(ansible, ansible, "Provision", "ssh")
Rel(engineer, ansible, "", "ssh\nrdp")
{{< /plantuml >}}

</p>

These diagrams demonstrate the two provisioning approaches: one using Vagrant and Ansible together, and the other using Ansible independently with the Ansible Control Node.

## Prerequisites

* Red Hat requires a subscription of course. In the RWS development environment we can use a {{< external-link url="https://developers.redhat.com/articles/getting-red-hat-developer-subscription-what-rhel-users-need-know" text="developer license" htmlproofer_ignore="true" >}}.
* [RWS Development Environment]({{< relref path="./dev-environment" >}}).

## Create and register

```bash
vagrant up gsd-ansible --no-provision
vagrant ssh gsd-ansible
sudo su -
subscription-manager register --username <username>
subscription-manager attach
```

<details>
  <summary><kbd>Show me</kbd></summary>

```bash
[vagrant@gsd-ansible ~]$ sudo su -
[root@gsd-ansible ~]# subscription-manager register --username <username>
Registering to: subscription.rhsm.redhat.com:443/subscription
Password:
The system has been registered with ID: 50b7543c-8e3d-4a80-801f-*******
The registered system name is: gsd-ansible

WARNING

The yum/dnf plugins: /etc/dnf/plugins/subscription-manager.conf were automatically enabled for the benefit of Red Hat Subscription Management. If not desired, use "subscription-manager config --rhsm.auto_enable_yum_plugins=0" to block this behavior.

[root@gsd-ansible ~]#
```

</details>

## Snapshot

To avoid the need for manual node registration, you can simplify the process by creating a snapshot. Please note that using the `vagrant destroy` command will result in the deletion of all snapshots.

To create a snapshot, use the following command:

```bash
vagrant snapshot save gsd-ansible v0
```

To restore the snapshot, execute the following command:

```bash
vagrant snapshot restore gsd-ansible v0
```

By utilizing snapshots, you can efficiently manage your node configuration without the hassle of repetitive manual registration. Just remember to exercise caution when working with the `vagrant destroy` command to prevent unintended deletion of snapshots.

## Provision

To provision the `gsd-ansible` node, run the following command:

```bash
vagrant provision gsd-ansible
```

The provisioning process for this node is determined by the contents of the `group_vars/ansible/main.yml` file, which utilizes the `bootstrap_packages` list, as well as the `plays/mgmt/ansible.yml` play. You can find the files in the Ansible project [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis/" >}}).

Here is an example of the `bootstrap_packages` configuration:

```yaml
bootstrap_packages:
  - name: python3-pip
    type: os
  - name: upgrade pip and setuptools
    cmd: pip3 install --upgrade pip setuptools
    type: cmd
    changed_when: Successfully installed
  - name:
      - xrdp
      - virtualenv
      - "@Server with GUI"
    type: os
```

## RDP

You can now create a RDP connection to this node using for example
{{< external-link url="https://remmina.org/" text="Remmina" htmlproofer_ignore="false" >}}
<!-- TODO enable htmlproofer again when website is up again -->

## Ansible environment

Each engineer must follow the instructions outlined in this section to set up their own personalized Ansible workspace.

Install {{< external-link url="https://github.com/pyenv/pyenv" text="pyenv" htmlproofer_ignore="true" >}}:

```bash
curl https://pyenv.run | bash
```

Add the following lines to `~/.bashrc`:

```bash
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
```

Install Python 3.10.6

```bash
source ~/.bashrc
pyenv install 3.10.6
pyenv global 3.10.6
```

Create Python virtual environment using `penv virtualenv`:

```bash
pyenv virtualenv rws
pyenv virtualenvs  # this will output all virtual environments available including "rws"
pyenv virtualenv-init rws
pyenv activate rws
```

In `~/.bashrc` add

```bash
eval "$(pyenv virtualenv-init -)"
pyenv activate rws
```

## Install Ansible

Install Ansible, including PIP packages for Kerberos and WinRM:

```bash
vagrant ssh c2d-ansible
pip3 install -r requirements.txt  # ansible creates this file in vagrant home
```

Alternatively, you can run the following commands:

```bash
pip3 install --upgrade pip
pip3 install ansible-core==2.11.12
pip3 install setuptools_rust
pip3 install yamllint==1.28.0 ansible-lint==6.8.6 pre-commit==2.20.0
pip3 install pywinrm==0.4.3
pip3 install requests-kerberos
pip3 install pywinrm[kerberos] requests-kerberos pykerberos
```

## Provision with Ansible

Once you have set up your Ansible environment, you can proceed with various tasks such as cloning the [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis/" >}}) project, downloading Ansible collections, and provisioning nodes using Ansible.

To ensure that your environment is functional, the easiest way however is to utilize the `/vagrant` mount. This mount contains the [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis/" >}}) project from your host. The only missing component is the `ansible-dev-collections` folder.

To incorporate this folder into `gsd-ansible`, you can create a special local and hidden file called `.sync_folders.yml` with the following contents. This file is in the git ignore list:

```yaml
---
- src: ../ansible-dev-collections/
  target: /ansible-dev-collections
```

Once you reload, you should be able to provision the `gsd` environment as follows:

```bash
vagrant reload gsd-ansible  # will create /ansible-dev-collections mount
vagrant ssh gsd-ansible
cd /vagrant
ansible-playbook plays/gis/server.yml -i hosts.ini
```

Please note that the above provision assumes that you have the `gsd-agserver1` already up and running.

With the Ansible Control Node, `gsd-ansible`, you can now manage the nodes using Ansible alone. Vagrant is no longer involved in the process.

See [Vagrant Sync folders]({{< relref path="/docs/guidelines/dev/vagrant-sync-folders" >}} "Guideline: Vagrant Sync Folders") for more information about the `.sync_folders.yml` file.