---
categories: ["How-to", "Example"]
tags: [ansible, certificates, pki, java, keystore, tomcat]
title: "Designing a Public Key Infrastructure (PKI) for RWS GIS with Ansible"
linkTitle: "Overview"
weight: 1
description: >
    Discover how to streamline the creation and management of RWS SSL/TLS
    certificates and Java KeyStores using Ansible, integrating manual steps
    seamlessly.
---

Projects:
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---
In line with common practices among Dutch government organizations, the
responsibility for SSL/TLS certificate creation is often delegated to a separate
unit. The typical procedure involves generating a Certificate Signing Request
(CSR), forwarding it to the designated unit via email, and receiving an official
RWS certificate in return.

Unfortunately, automation is often hindered in such scenarios due to the absence
of APIs. Unlike solutions such as
{{< external-link
url="https://letsencrypt.org/"
text="Let's Encrypt"
htmlproofer_ignore="false" >}},
there is no comparable automation in place.

This section elucidates the Public Key Infrastructure (PKI) design of the RWS
GIS Platform. It delineates the creation and management of SSL/TLS certificates
and Java KeyStores, with a setup designed to facilitate the creation of a fully
functional GIS platform without the need for manual intervention.

While there are some manual steps involved, they are neither essential nor
impediments to provisioning a fully automated GIS Platform. These steps can be
independently performed to comply with security and compliance standards.

The proposed methodology leverages Ansible's capabilities, utilizing modules
from the `community.crypto` collection to establish a compact Certificate
Authority (CA). Refer to the
{{< external-link
url="https://docs.ansible.com/ansible/latest/collections/community/crypto/docsite/guide_ownca.html"
text="How to create a small CA — Ansible Documentation"
htmlproofer_ignore="true" >}}
for comprehensive insights and guidance on this process.

---

## Step 1: OwnCA Certificates

The sequence diagram below illustrates the initial fully automated steps in
provisioning services, including SSL/TLS certificates and Java KeyStores. The
example employs **Tomcat** for an **FME Core** server, but the principle/idea is
applicable to the provisioning of all services. The Tomcat role utilizes
(through `include_role`) the `cacerts2` role of the
[`c2platform.core`]({{< relref path="/docs/gitlab/c2platform/ansible-collection-core" >}})
Ansible collection.This role delegates the creation of SSL/TLS certificates and
Java KeyStores to a dedicated **CA Server**.

Any Linux server can fulfill the role of the **CA Server**. Linux
is the preferred OS for these tasks due to the abundance of Ansible modules for
Linux hosts and the complete absence of such modules for MS Windows hosts. Note
that this was not the reason for creating a "central" certificate server and
delegating certificate management and creation to it. The reason is simply that
there is only one root CA certificate for a proper OwnCA solution.

This first step leads to a fully functional system without manual steps.
However, the systems are not compliant because they don't use the correct,
proper RWS certificates.

Within the RWS GIS platform, various actors are involved in certificate
management and deployment:

1. **Ansible Automation Platform ( AAP ):** AAP handles the deployment and
   management of servers, such as the **FME Core** servers. Additionally, it
   manages the **CA Server**.
2. **GIS Platform Team:**: GIS Platform management team.
3. **RWS CSP**:  RWS Certificate Service Provider (CSP) issues RWS certificates.

For the creation/configuration of a Java KeyStore on the **FME Core** Server,
the following steps are taken:

1. **AAP** initiates the deployment of Tomcat via the `c2platform.gis.tomcat`
   Ansible role.
2. The Tomcat role (`c2platform.gis.tomcat`) performs tasks from the `cacerts2`
   role, which delegates tasks to the **CA Server**.

On the **CA Server**, `cacerts2` executes the following tasks/steps:

3. Generation of certificate request.
4. Generation of the certificate.
5. Creation and import of certificates into the Java KeyStore.
6. Import of RWS/OwnCA Root and intermediate certificates into the Java KeyStore.

On the **FME Core** server, `cacerts2` executes the following tasks/steps:

7. Copying the Java KeyStore from the **CA Server** to the **FME
   Core** server.
8. The Tomcat role resumes control and configures Tomcat for HTTPS based on the
   Java KeyStore.
9. The Tomcat role initiates a restart of the Tomcat Server.

{{< plantuml id="rws-certificates-ownca" >}}
@startuml

!define ArrowColor #000000
!define BoxColor #A9DCDF
!define ActorColor #FFA500
!define ActorFontSize 14
!define BoxFontSize 14

actor "AAP" as AAP
participant "FME Core" as FME
database "CA Server" as OWNCA
actor "GIS Platform\nTeam" as GEO
actor "RWS CSP" as CSP

'autonumber
'IVP_TB_GEO->AAP: Delegeer beheer GIS\nPlatform beheer
AAP->FME: Provision Tomcat\n(c2platform.gis.tomcat)
FME->OWNCA: Delegate provision\n(c2platform.core.cacerts2)
OWNCA->OWNCA: Create OwnCA\nCSR, certificate,\nJava KeyStore
OWNCA-> FME: Java KeyStore
FME->FME: Configure HTTPS
@enduml
{{< /plantuml >}}


After ensuring the initial functionality with OwnCA certificates, the next
crucial step involves the substitution of these temporary certificates with
official RWS certificates.

## Step 2: RWS Certificates

The sequence diagram illustrates the steps necessary to ensure compliance with
the RWS security policy, which mandates the use of a secure protocols like HTTPS
with proper RWS certifictes everywhere, regardless of whether communication is
internal or external within the GIS platform. Services are required to utilize
official RWS certificates, obtainable exclusively through email requests.

10. **GIS Platform Team** sends the CSR file generated in step 3 to the **RWS CSP**.
11. The **RWS CSP** generates the certificate and sends it back to the
    **GIS Platform Team** team.
12. **GIS Platform Team** receives the RWS certificate and replaces the OwnCA
    certificate on the **CA Server** with the RWS certificate.

AAP can now install the certificate, repeating steps 1 through 9 with the
following differences:

1. Steps 3 and 4 are skipped.
2. Steps 5 and 6 result in a new keystore used in steps 7 through 9.

{{< plantuml id="rws-certificates" >}}
@startuml

!define ArrowColor #000000
!define BoxColor #A9DCDF
!define ActorColor #FFA500
!define ActorFontSize 14
!define BoxFontSize 14

actor "AAP" as AAP
participant "FME Core" as FME
database "CA Server" as OWNCA
actor "GIS Platform\nTeam" as GEO
actor "RWS CSP" as CSP

'autonumber
OWNCA->GEO: <<scp>> CSR
GEO->CSP: <<email>> CSR
CSP->GEO: <<email>> RWS Certificate
GEO->OWNCA: <<scp>> RWS Certificate
AAP->FME: Provision Tomcat\n(c2platform.gis.tomcat)
FME->OWNCA: Delegate provision\n(c2platform.core.cacerts2)
OWNCA->OWNCA: Update Java KeyStore\nwith RWS Certificate
OWNCA-> FME: Java KeyStore
FME->FME: Configure HTTPS
@enduml
{{< /plantuml >}}

With the RWS certificates in place, the system is now fully compliant and ready
for production use. This transition ensures a secure and compliant GIS platform,
with the entire process automated through Ansible.
