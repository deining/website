---
categories: ["How-to"]
tags: [reverse-proxy, forward-proxy, apache, firefox, profile, proxy]
title: "Access the RWS Development Environment with Firefox"
linkTitle: "FireFox"
weight: 6
description: >
    Learn how to access the RWS Development Environment through Firefox by
    configuring a dedicated profile, importing the C2 CA Root certificate, and
    setting up a forward proxy.
---

## Create a Firefox Profile

It is strongly recommended to create a dedicated Firefox profile to keep your
regular browsing separate from accessing the RWS Development Environment. You
can do this by following Mozilla's guide on
{{< external-link
url="https://support.mozilla.org/en-US/kb/profile-manager-create-remove-switch-firefox-profiles?redirectslug=profile-manager-create-and-remove-firefox-profiles&redirectlocale=en-US"
text="Profile Manager - Create, remove or switch Firefox profiles"
htmlproofer_ignore="false" >}}

You can access the Firefox Profile Manager in two ways:
1. By running the command `firefox -no-remote -P`.
2. Alternatively, you can simply type `about:profiles` in the Firefox address
   bar to create and manage your Firefox profiles.

Once you have your dedicated profile ready, proceed with the following settings.

## Configure Network Settings

In your dedicated profile, configure the following network settings:

| Property           | Value       |
|--------------------|-------------|
| HTTP Proxy         | `1.1.5.205` |
| Port               | `1080`      |
| Also use for HTTPS | ✔           |


## Import CA Bundle

To prevent continuous TLS/SSL certificate errors when accessing the services,
import the C2 root certificate located at
`~/git/gitlab/c2/ansible-gis/.ca/c2/c2.crt` through the Firefox Certificates
settings.

## Verify

Navigate to
{{< external-link
url="https://c2platform.org/is-alive"
htmlproofer_ignore="true" >}}.
You should see the text "Apache is alive." If you encounter this message, your
browser is correctly configured to access services from the GIS platform.
