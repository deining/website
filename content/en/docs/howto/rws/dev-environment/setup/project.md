---
categories: ["How-to"]
tags: [git]
title: "Setting Up Project Directories for RWS"
linkTitle: "Project Directory Setup"
weight: 2
description: >
  Learn how to clone RWS Git repositories using the clone script and install Ansible
  Galaxy Roles and Collections to set up your development environment efficiently.
---


## Run the Clone Script

```bash
sudo apt install git -y
export GIT_USER= # your gitlab user
export GIT_MAIL= # your gitlab mail
eval `ssh-agent -s`
ssh-add
curl -s -L https://gitlab.com/c2platform/rws/ansible-arcgis/-/raw/master/clone.sh | bash
```

## Create `rws` Alias

As part of setting up the C2 development environment see
[Setup Project Directory and Install Ansible]({{< relref path="/docs/howto/dev-environment/setup/project" >}})
we created two aliases `c2` and `c2-home`.

```
alias c2-home='cd ~/git/gitlab/c2/ansible-dev'
alias c2='c2-home && source ~/.virtualenv/c2d/bin/activate'
```

For RWS development we create the `rws` alias as
shown below.

```
alias rws='c2 && cd ~/git/gitlab/c2/ansible-gis'
```

## Install Ansible Collections and Roles

Navigate to the root of the Ansible project and activate the Ansible virtual
environment using the `rws`` alias. Then, install roles and collections as
follows:

```bash
rws # activate the RWS Ansible Virtual Environment
ansible-galaxy install -r roles/requirements.yml --force --no-deps -p roles/external
ansible-galaxy collection install -r collections/requirements.yml -p .
```
By following these steps, you'll have your project directories set up and be
ready for RWS development.
