---
categories: ["How-to"]
tags: [windows]
title: "Manage Windows Systems"
linkTitle: "Windows"
weight: 10
description: >
  Manage MS Windows systems using C2 Platform `win_core` collection.
---