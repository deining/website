---
categories: ["How-to"]
tags: [argcis, windows]
title: "Setup ArcGIS Server and Date Store using Ansible"
linkTitle: "ArcGIS Server"
weight: 1
description: >
  Install ArcGIS Server and ArcGIS Data Store on `gsd-agserver1` using Ansible.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

This how-to describes how to create the {{< external-link
url="https://enterprise.arcgis.com/en/server/latest/get-started/windows/what-is-arcgis-for-server-.htm"
text="ArcGIS Server" htmlproofer_ignore="false" >}} instance `gsd-agserver1`
using the `arcgis_server` role Ansible role that is part of the
[`c2platform.gis`]({{< relref
path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}}) Ansible
collection.

{{< plantuml id="" >}}
@startuml
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Boundary(gis, "gis.c2platform.org", $type="") {
  System(server_datastore, "ArcGIS Server & Datastore", "gsd-agserver1") {
    Container(agserver1, "ArcGIS Server", "")
    Container(datastore, "ArcGIS Datastore", "")
  }
  'Container(rproxy, "Reverse Proxy", "gsd-rproxy1 5.205")
}
{{< /plantuml >}}

---

<!-- include-start: howto-prerequisites-rws.md -->
## Prerequisites

Create the reverse and forward proxy `gsd-rproxy1`.

```bash
vagrant up gsd-rproxy1
```

For more information about the various roles that `gsd-rproxy1` performs in this project:

* [Setup Reverse Proxy and CA server]({{< relref path="/docs/howto/c2/reverse-proxy">}})
* [Setup SOCKS proxy]({{< relref path="/docs/howto/c2/socks-proxy">}})
* [Managing Server Certificates as a Certificate Authority]({{< relref path="/docs/howto/c2/certs">}})
* The ArcGIS Server software and license.
<!-- include-end -->

## Setup

<!-- TODO 18 minutes -->

```bash
vagrant up gsd-agserver1
```

## Verify

Select **Show** in the GUI in VirtualBox Manager for `gsd-agserver1` and then
Login and then start FireFox.

### ArcGIS Server

Using credentials below navigate to {{< external-link url="https://localhost:6443/arcgis/admin/" text="ArcGIS Server Administrator Directory" htmlproofer_ignore="true" >}} and navigate to {{< external-link url="https://localhost:6443/arcgis/manager/" text="ArcGIS Server Manager" htmlproofer_ignore="true" >}} and login.


|Property  |Value                |
|----------|---------------------|
|Username  |`siteadmin`          |
|Password  |`siteadmin123`       |

### ArcGIS Data Store

Navigate to {{< external-link
url="https://localhost:2443/arcgis/datastore/" htmlproofer_ignore="true" >}} you
should see the ArcGIS Data Store web interface with stores listed:
**Relational** and **Tile Cache**.

Select **Configure Additional Data Stores** and then login to the ArcGIS Server
instance using values below:

|Property  |Value                |
|----------|---------------------|
|GIS Server|`gsd-agserver1.local`|
|Username  |`siteadmin`          |
|Password  |`siteadmin123`       |


## TODO

[arcgis_server_initialized.yml]({{< absURL "/downloads/rws/arcgis_server_initialized.yml" >}})
[arcgis_server_not_initialized.yml]({{< absURL "/downloads/rws/arcgis_server_not_initialized.yml" >}})

## Links

*  [Welcome to the ArcGIS Server (Windows) installation guide—ArcGIS Enterprise | Documentation for ArcGIS Enterprise](https://enterprise.arcgis.com/en/server/latest/install/windows/welcome-to-the-arcgis-for-server-install-guide.htm)
