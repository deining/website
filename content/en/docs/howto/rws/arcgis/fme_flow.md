---
categories: ["How-to"]
tags: [gis, fme, windows, sysprep]
title: "Setting Up FME Flow with Ansible"
linkTitle: "FME Flow"
weight: 5
description: >
  Setup the FME Flow Core, FME Flow Database and FME Flow System Share on MS Windows using Ansible.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}),
[`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore" >}}),
[`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis" >}})

---

{{< under_construction >}}

<!-- TODO finish -->

Here's an overview of the process to set up FME Flow Core, FME Flow Database,
and FME Flow System Share using Vagrant and Ansible. For more information about
these components, refer to
{{< external-link
url="https://docs.safe.com/fme/2023.0/html/FME-Flow/ReferenceManual/architecture.htm"
text="FME Flow Architecture"
htmlproofer_ignore="false" >}}.

1. Vagrant creates two VirtualBox Windows VMs: `gsd-fme-core`, `gsd-ad`, and a
   LXD container `gsd-db1`.
2. Vagrant utilizes the
     {{< external-link
     url="https://github.com/rgl/vagrant-windows-sysprep"
     text="Vagrant Windows Sysprep Provisioner" htmlproofer_ignore="false" >}}
    on `gsd-fme-core` and `gsd-ad`.
3. Vagrant runs the Ansible provisioner in the following order on nodes:
   1. On `gsd-ad`, the `c2platform.wincore.ad` collection is used to configure
      the AD domain controller for the domain `ad.c2platform.org`.
   2. PostgreSQL 14 is installed on `gsd-db1`, along with a database and
      database user with necessary privileges.
   3. On `gsd-fme-core`, Ansible performs the following steps:
      1. Joins the node to the Windows domain `ad.c2platform.org`.
      2. Installs Java using the `c2platform.gis.java` role.
      3. Installs Tomcat using the `c2platform.gis.tomcat` role.
      4. Installs **FME Flow Core** using the `c2platform.gis.fme` role.

The diagram below illustrates the setup achieved with Vagrant, excluding the
reverse proxy `gsd-rproxy1`.

{{< plantuml id="" >}}
@startuml
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_WITH_LEGEND()
HIDE_STEREOTYPE()
!include <office/Servers/virtual_server>
!include <office/Servers/reverse_proxy>
!include <office/Devices/workstation>
!include <office/Security/active_directory>
!include <logos/kubernetes>
!include <cloudogu/tools/ansible>
!include <logos/gitlab>
!include <cloudinsight/iis>
!include <cloudinsight/tomcat>
!include <cloudinsight/postgresql>

AddSystemTag("postgresql", $sprite="postgresql", $legendText="postgresql")
AddSystemTag("reverse_proxy", $sprite="reverse_proxy", $legendText="reverse_proxy")
AddSystemTag("active_directory", $sprite="active_directory", $legendText="active_directory")

Boundary(gis, "gis.c2platform.org", $type="") {
  Boundary(dmz, "dmz", $type="") {
    System(rproxy, "Reverse Proxy", "gsd-rproxy1", "", $tags="reverse_proxy")
    note right: Out-of-scope\nfor this how-to
  }
  Boundary(app, "app", $type="") {
    System(fme_core, "FME Flow Core", "gsd-fme-core", "")
    note right: 1
    System(fme_engine, "FME Flow Engine", "gsd-fme-engine", "")
    note right: 2
  }
  Boundary(data, "data", $type="") {
    System(db, "Database", "gsd-db1", "", $tags="postgresql")
    note right: 1
  }
  Boundary(mgmt, "mgmt", $type="") {
    System(ad, "Active Directory", "gsd-ad", "", $tags="active_directory")
    note right: 1
  }
}
Rel(rproxy, fme_core, "")
Rel(rproxy, fme_engine, "")
Rel(fme_core, ad, "")
Rel(fme_engine, ad, "")
Rel(fme_core, db, "")

SHOW_LEGEND()
{{< /plantuml >}}

## Prerequisites

Before proceeding, make sure you have completed the steps to
[Setup the RWS Development Environment on Ubuntu 22]({{< relref path="/docs/howto/rws/dev-environment/setup" >}})

## Setup

Use the following commands to run the FME play and create the `gsd-ad`,
`gsd-db1`, and `gsd-core-fme` nodes. Running the `vagrant up` command will take
approximately 25 minutes to complete.

```bash
export BOX="gsd-ad gsd-db1 gsd-core-fme"
export PLAY="plays/gis/fme.yml"
vagrant up $BOX | tee provision.log
```
## Verify

### `gsd-ad`

1. Log in to `gsd-ad` and execute `systeminfo | Select-String "Domain"`. This
   should return `ad.c2platform.org`.
    <details>
      <summary><kbd>Show me</kbd></summary>

    ```bash
    PS C:\Users\vagrant> systeminfo | Select-String "Domain"

    OS Configuration:          Primary Domain Controller
    Domain:                    ad.c2platform.org


    PS C:\Users\vagrant> nslookup ad.c2platform.org
    Server:  ip6-localhost
    Address:  ::1

    Name:    ad.c2platform.org
    Addresses:  1.1.8.108
              10.0.2.15

    PS C:\Users\vagrant>
    ```

    </details>
2. Open the **DNS Manager** and check the properties of the DNS server
   **GSD-AD**. Only `1.1.8.108` should be enabled as a listening IP address.
    <details>
    <summary><kbd>Show me</kbd></summary>
    {{< image filename="/images/docs/dns-manager.png?width=1200px" >}}
    </details>
3. On your Ubuntu laptop run `dig @1.1.8.108 ad.c2platform.org`. This should
   resolve `ad.c2platform.org` to `1.1.8.108`.
    <details>
      <summary><kbd>Show me</kbd></summary>

    ```bash
    onknows@io3:~$ dig @1.1.8.108 ad.c2platform.org

    ; <<>> DiG 9.18.12-0ubuntu0.22.04.3-Ubuntu <<>> @1.1.8.108 ad.c2platform.org
    ; (1 server found)
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 27806
    ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 4000
    ;; QUESTION SECTION:
    ;ad.c2platform.org.		IN	A

    ;; ANSWER SECTION:
    ad.c2platform.org.	600	IN	A	1.1.8.108
    ad.c2platform.org.	600	IN	A	10.0.2.15

    ;; Query time: 0 msec
    ;; SERVER: 1.1.8.108#53(1.1.8.108) (UDP)
    ;; WHEN: Wed Oct 25 09:40:07 CEST 2023
    ;; MSG SIZE  rcvd: 78
    ```

    </details>

### `gsd-fme-core`

Log in to `gsd-fme-core`.

1. Verify that the computer is part of the domain `ad.c2platform.org`:
    ```bash
    vagrant ssh gsd-fme-core
    powershell
    systeminfo.exe | Select-String "Domain"
    ```
    <details>
      <summary><kbd>Show me</kbd></summary>

    ```bash
    vagrant@GSD-FME-CORE C:\Users\vagrant>powershell
    Windows PowerShell
    Copyright (C) Microsoft Corporation. All rights reserved.

    Install the latest PowerShell for new features and improvements! https://aka.ms/PSWindows

    PS C:\Users\vagrant> systeminfo.exe | Select-String "Domain"

    Domain:                    ad.c2platform.org


    PS C:\Users\vagrant>

    ```

    </details>

### Login `gis-backup-operator`

Login as `gis-backup-operator` using remote desktop to `gsd-fme-core` to confirm
that the user has been created with the correct password.

### Using pgAdmin

On `gsd-fme-core`, connect using remote desktop using the Vagrant user, start
`pgAdmin`, and import the settings to the desktop.

TODO

## Review

In the RWS Ansible Inventory project
[`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}):

1. Review the Ansible playbook `plays/gis/fme.yml`. Pay attention to the use of the `when` condition, ensuring that the installation of Java and Tomcat is restricted to `gsd-fme-core`.
2. Check the `group_vars/` for additional configuration options.


## Default Install

To perform a default installation of FME Flow, which includes deploying a
PostgreSQL database without a separate Tomcat instance, follow these steps:

1. Remove or disable the `fme_flow_install_command`.
2. Recreate the environment by executing the following commands:

```bash
vagrant destroy gsd-fme-core -f
vagrant up gsd-fme-core
```

After successfully completing these commands, you should be able to access the
FME Flow interface by visiting
{{< external-link
url="http://localhost"
htmlproofer_ignore="false" >}}
, which will redirect you to
{{< external-link
url="http://localhost/fmeserver/"
htmlproofer_ignore="false" >}}.
Log in as `admin` with the password `admin`.

## Troubleshooting

If the service does not start as expected for `gis-backup-operator`, you can try
the following command:

```
D:\apps\FME\Flow\Server\WindowsService\FMEProcessMonitorCore.exe -s "FME Server Core"
```