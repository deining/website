---
categories: ["How-to", "Example"]
tags: [ansible, environment]
title: "Create custom Ansible Execution Environment"
linkTitle: "Ansible Execution Environment"
weight: 4
description: >
  Learn how to create a custom Ansible Execution Environment with the latest
  Python and Ansible versions.
---

Projects: [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}), [`c2platform/rws/ansible-execution-environment`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-execution-environment" >}})

{{< alert title="Note:" >}}
This how-to currently uses the `c2d` and specifically the `c2d-xtop`
environment, not the RWS `gsd` development environment, to describe the
procedure for creating and maintaining this Ansible Execution Environment.
However, you can adapt the steps for any machine with Docker, Python, and
Ansible installed.
{{< /alert >}}

## Prerequisites

* [Setup a Development Environment on Ubuntu 22]({{< relref path="/docs/howto/dev-environment/setup/" >}})

## Setup Environment

Create and enter the LXD node `c2d-xtop`:

```bash
vagrant up c2d-xtop
vagrant ssh c2d-xtop
```

## Setup a Python Environment

Inside `c2d-xtop`, run the script `/vagrant/scripts/penv.sh`

```bash
source /vagrant/scripts/penv.sh
```

Install Ansible Builder and Navigator:

```bash
pip install ansible-builder
pip install ansible-navigator
```

## Clone Project

```bash
cd /tmp
git clone git@gitlab.com:c2platform/rws/ansible-execution-environment.git
cd ansible-execution-environment
```

## Build

```bash
rm -rf context
ansible-builder build --tag whatever_ee
```

## Verify

```bash
ansible-navigator run test_localhost.yml \
  --execution-environment-image whatever_ee --mode stdout
```

<details>
  <summary><kbd>Show me</kbd></summary>

```bash
(c2) vagrant@c2d-xtop:/ansible-execution-environment$ ansible-navigator run test_localhost.yml \
  --execution-environment-image whatever_ee --mode stdout
-----------------------------------------------------
Execution environment image and pull policy overview
-----------------------------------------------------
Execution environment image name:     whatever_ee:latest
Execution environment image tag:      latest
Execution environment pull arguments: None
Execution environment pull policy:    tag
Execution environment pull needed:    True
-----------------------------------------------------
Updating the execution environment
-----------------------------------------------------
Running the command: docker pull whatever_ee:latest
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that
the implicit localhost does not match 'all'

PLAY [Gather and print local Ansible / Python facts] ***************************

TASK [Gathering Facts] *********************************************************
ok: [localhost]

TASK [Print Ansible version] ***************************************************
ok: [localhost] => {
    "ansible_version": {
        "full": "2.15.0",
        "major": 2,
        "minor": 15,
        "revision": 0,
        "string": "2.15.0"
    }
}

TASK [Print Ansible Python] ****************************************************
ok: [localhost] => {
    "ansible_playbook_python": "/usr/bin/python3"
}

TASK [Print Ansible Python version] ********************************************
ok: [localhost] => {
    "ansible_python_version": "3.11.4"
}

PLAY RECAP *********************************************************************
localhost                  : ok=4    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

</details>

## Links

* {{< external-link
  url="https://ansible.readthedocs.io/en/latest/getting_started_ee/run_community_ee_image.html"
  text="Running Ansible with the community EE image - Ansible ecosystem documentation"
  htmlproofer_ignore="false" >}}
* {{< external-link
  url="https://ansible.readthedocs.io/en/latest/getting_started_ee/setup_environment.html"
  text="Setting up your environment - Ansible ecosystem documentation"
  htmlproofer_ignore="false" >}}
* {{< external-link
  url="https://ansible.readthedocs.io/en/latest/getting_started_ee/build_execution_environment.html"
  text="Building your first Execution Environment - Ansible ecosystem documentation"
  htmlproofer_ignore="false" >}}
* {{< external-link
  url="https://ansible.readthedocs.io/projects/builder/en/stable/definition/"
  text="Execution Environment Definition — Ansible Builder Documentation"
  htmlproofer_ignore="false" >}}




