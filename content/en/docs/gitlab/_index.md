---
title: "GitLab Projects"
linkTitle: "GitLab"
weight: 15
description: >
  In this section, you'll find a compilation of C2 Platform GitLab projects participating in the
  {{< external-link
  url="https://about.gitlab.com/solutions/open-source/join/" text="GitLab Open Source Program"
  htmlproofer_ignore="false" >}}
  , enabling them to leverage the full capabilities of
  [GitLab Ultimate]({{< relref path="/docs/concepts/gitlab" >}})
---
