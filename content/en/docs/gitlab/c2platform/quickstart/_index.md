---
categories: GitLab Projects
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: quickstart
tags: []
title: "Quick Start"
weight: 22
---

GitLab: <a href="https://gitlab.com/c2platform/quickstart"><code>c2platform/quickstart</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


See [Getting Started | C2 Platform]({{< relref path="/docs/getting-started" >}}).


---


<!--
id | 39321621
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/quickstart" data-proofer-ignore>c2platform/quickstart&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
