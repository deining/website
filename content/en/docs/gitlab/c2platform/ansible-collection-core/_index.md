---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-core
tags: ['ansible', 'c2platform', 'collection', 'common', 'core', 'linux', 'windows']
title: "Ansible Collection - c2platform.core"
weight: 14
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-core"><code>c2platform/ansible-collection-core</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-core/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-core/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-core/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-core/-/pipelines)

C2 Platform generic roles that are used by all or some other roles. These roles typically don't create services / processes on target node but are depedencies e.g. packages required by those roles. Or these roles help with Ansible provisioning for example offers generic Ansible modules, filters etc.

- [Roles](#roles)
- [Plugins](#plugins)
- [Filters](#filters)
  - [`groups2items`](#groups2items)
  - [`product`](#product)

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/server_update" text="server_update" htmlproofer_ignore="false" >}} update yum / apt cache and /or upgrade packages.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/bootstrap" text="bootstrap" htmlproofer_ignore="false" >}} bootstrap your nodes with os and pip packages.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/mount" text="mount" htmlproofer_ignore="false" >}} configure / manage mount points.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/secrets" text="secrets" htmlproofer_ignore="false" >}} workaround for lack of support for vault when using AWX.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/os_trusts" text="os_trusts" htmlproofer_ignore="false" >}} Manage OS trust store.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/cacerts2" text="cacerts2" htmlproofer_ignore="false" >}} create your {{< external-link url="https://docs.ansible.com/ansible/latest/collections/community/crypto/docsite/guide_ownca.html" text="own small CA" htmlproofer_ignore="false" >}}.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/apt_repo" text="apt_repo" htmlproofer_ignore="false" >}} add APT keys, repositories.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/yum" text="yum" htmlproofer_ignore="false" >}} add / remove Yum repositories.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/files" text="files" htmlproofer_ignore="false" >}} manage files, directories, ACL.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/users" text="users" htmlproofer_ignore="false" >}} manage Linux accounts.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/service" text="service" htmlproofer_ignore="false" >}} create systemd services.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/java" text="java" htmlproofer_ignore="false" >}} install java, manage keystores.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/facts" text="facts" htmlproofer_ignore="false" >}} gather facts.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/lcm" text="lcm" htmlproofer_ignore="false" >}} facts for LCM operations for other roles to build upon.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/lvm" text="lvm" htmlproofer_ignore="false" >}} manage data disks for roles using {{< external-link url="https://en.wikipedia.org/wiki/Logical_Volume_Manager_%28Linux%29" text="LVM" htmlproofer_ignore="false" >}}.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/rest" text="rest" htmlproofer_ignore="false" >}} interact with REST webservices.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/postgresql_tasks" text="postgresql_tasks" htmlproofer_ignore="false" >}} include tasks for PostgreSQL database operations.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/postgresql_client" text="postgresql_client" htmlproofer_ignore="false" >}}.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/restart" text="restart" htmlproofer_ignore="false" >}} orchestrate stop, start and restart over multiple nodes.

## Plugins

Module plugins:

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/plugins/modules/java_facts.py" text="java_facts" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/plugins/modules/lcm_info.py" text="lcm_info" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/plugins/modules/set_certificate_facts.py" text="set_certificate_facts" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/plugins/modules/set_rest_facts.py" text="set_rest_facts" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/plugins/modules/set_sprint_facts.py" text="set_sprint_facts" htmlproofer_ignore="false" >}}

Filter plugins:

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/plugins/filter/ansible_filters.py" text="ansible_filters" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/plugins/filter/cacerts_filters.py" text="cacerts_filters" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/plugins/filter/files_filters.py" text="files_filters" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/plugins/filter/java_filters.py" text="java_filters" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/plugins/filter/lvm_filters.py" text="lvm_filters" htmlproofer_ignore="false" >}}

## Filters

### `groups2items`

The `groups2items` filter in `c2platform.core` is a powerful tool to transform
dictionaries of "grouped" lists or dictionaries into flat lists. This filter is
especially valuable in the context of C2 Ansible collections, where the use of
"grouped" data structures is a common practice.

The `groups2items` filter shares similarities with the built-in
{{< external-link url="https://docs.ansible.com/ansible/2.7/user_guide/playbooks_filters.html#dict-filter" text="`dict2items`" htmlproofer_ignore="false" >}}
filter. Its primary purpose is to simplify complex data structures for easier
processing, particularly when utilizing Ansible's `loop` or `with_items`.

Consider the following scenario:

```yaml
my_grouped_lists:
 group1:
   - name: file1
   - name: file2
 group2:
   - name: file3
   - name: file4
my_list: "{{ my_grouped_lists | c2platform.core.groups2items }}"
```
In this case, `my_grouped_lists` contains groups with associated lists. Applying
the `groups2items` filter to it results in a flattened list:

```yaml
my_list:
-   group: group1
    name: file1
-   group: group1
    name: file2
-   group: group2
    name: file3
-   group: group2
    name: file4
```

The `groups2items` filter is versatile and can handle nested structures as well.
For instance:

```yaml
my_grouped_lists:
 group1:
   name: file1
   url: url1
 group2:
   name: file3
   url: url3
my_list: "{{ my_grouped_lists | c2platform.core.groups2items }}"
```

In this example, `my_grouped_lists` contains nested dictionaries within groups.
The `groups2items` filter transforms them into a flat list:

```yaml
my_list:
-   group: group1
    name: file1
    url: url1
-   group: group2
    name: file3
    url: url3
```

The `groups2items` filter simplifies complex data structures, making it easier to
work with structured data in your Ansible playbooks and roles.

### `product`

The product filter is a custom Jinja filter designed to facilitate the merging
of attributes from two lists of dictionaries at the same level. It is
particularly useful when you need to generate combinations of attributes from
these two lists. While it shares a similar purpose with
{{< external-link url="https://docs.ansible.com/ansible/latest/collections/ansible/builtin/product_filter.html" text="`ansible.builtin.product`" htmlproofer_ignore="false" >}}
filter, the `c2platform.core.product` filter is specifically tailored to work
seamlessly with lists of dictionaries that can be merged.

For example, consider the following lists:

```yaml
list1:
  - { key1: value1, key2: value2 }
  - { key1: value3, key2: value4 }

list2:
  - { key3: value5, key4: value6 }
  - { key3: value7, key4: value8 }
```

You can merge these lists using the `c2platform.core.product` filter:

```yaml
merged_lists: "{{ list1 | product(list2) }}"
```
This results in the following merged list, which includes all conceivable
combinations of attributes from `list1` and `list2`:

```yaml
merged_lists:
  - { key1: value1, key2: value2, key3: value5, key4: value6 }
  - { key1: value1, key2: value2, key3: value7, key4: value8 }
  - { key1: value3, key2: value4, key3: value5, key4: value6 }
  - { key1: value3, key2: value4, key3: value7, key4: value8 }
```

For a practical demonstration of how to use this filter, please consult the
`group_vars/windows/proxy.yml` file within the
[RWS Inventory Project ]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis" >}}).

This file provides a real-world example showcasing the filter's application in a
specific context.


---


<!--
id | 39048659
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-core" data-proofer-ignore>c2platform/ansible-collection-core&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
