---
categories: GitLab Projects
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-snippets
tags: []
title: "C2Platform Ansible Snippets for Visual Studio Code"
weight: 21
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-snippets"><code>c2platform/ansible-snippets</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-snippets/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-snippets/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-snippets/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-snippets/-/pipelines) {{< external-link url="https://vsmarketplacebadge.apphb.com/version/c2platform.c2platform-ansible-snippets.svg" text="![Marketplace Version" htmlproofer_ignore="false" >}}](https://marketplace.visualstudio.com/items?itemName=c2platform.c2platform-ansible-snippets) {{< external-link url="https://vsmarketplacebadge.apphb.com/installs/c2platform.c2platform-ansible-snippets.svg" text="![Installs" htmlproofer_ignore="false" >}}](https://marketplace.visualstudio.com/items?itemName=c2platform.c2platform-ansible-snippets) {{< external-link url="https://vsmarketplacebadge.apphb.com/downloads/c2platform.c2platform-ansible-snippets.svg" text="![Downloads" htmlproofer_ignore="false" >}}](https://marketplace.visualstudio.com/items?itemName=c2platform.c2platform-ansible-snippets)  {{< external-link url="https://vsmarketplacebadge.apphb.com/rating/c2platform.c2platform-ansible-snippets.svg" text="![Rating" htmlproofer_ignore="false" >}}](https://marketplace.visualstudio.com/items?itemName=c2platform.c2platform-ansible-snippets)

This Visual Studio Code extension has snippets to help you write your Ansible code when you are using {{< external-link url="https://gitlab.com/c2platform" text="C2 Platform Ansible collections" htmlproofer_ignore="false" >}}. To access all of the snippets press `Cntrl+Space` and type `c2`.


---


<!--
id | 39160756
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-snippets" data-proofer-ignore>c2platform/ansible-snippets&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
