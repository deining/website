---
categories: GitLab Projects
description: "This project contains the sources and content for the C2 Platform website. "
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: website
tags: ['documentation', 'hugo', 'website']
title: "Website"
weight: 24
---

GitLab: <a href="https://gitlab.com/c2platform/website"><code>c2platform/website</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/website/badges/master/pipeline.svg)](https://gitlab.com/c2platform/website/-/commits/master)
[![Latest Release](https://gitlab.com/c2platform/website/-/badges/release.svg)](https://gitlab.com/c2platform/website/-/releases)

This project contains the sources and content for the website at {{< external-link url="https://c2platform.org" htmlproofer_ignore="false" >}}. This project uses {{< external-link url="https://gohugo.io/" text="Hugo" htmlproofer_ignore="false" >}} with the {{< external-link url="https://github.com/google/docsy" text="Docsy Theme" htmlproofer_ignore="false" >}}. This project is created as a copy of the {{< external-link url="https://github.com/google/docsy-example" text="Docsy Example" htmlproofer_ignore="false" >}} project.

You can find detailed theme instructions in the {{< external-link url="https://www.docsy.dev/docs/" text="Docsy user guide" htmlproofer_ignore="false" >}}.

- [Pipeline](#pipeline)
- [Running the website locally](#running-the-website-locally)
- [Running a container locally](#running-a-container-locally)
- [Verify that the service is working.](#verify-that-the-service-is-working)
- [PlantUML support](#plantuml-support)
- [Validate HTML pre-commit](#validate-html-pre-commit)
- [Test on GitLab Runner](#test-on-gitlab-runner)
- [Snippets](#snippets)
- [Troubleshooting](#troubleshooting)
- [Links](#links)


## Pipeline

The CI/CD pipeline currently builds two versions of the website:

1. A development version on `development` branch will be published to {{< external-link url="https://c2platform.org/dev" htmlproofer_ignore="false" >}}
2. The production version on `master` branch is published to {{< external-link url="https://c2platform.org" htmlproofer_ignore="false" >}}

## Running the website locally

Building and running the site locally requires a recent `extended` version of {{< external-link url="https://gohugo.io" text="Hugo" htmlproofer_ignore="false" >}}.

```bash
sudo apt install snapd -y
sudo snap install hugo
```

```bash
curl -o- {{< external-link url="https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh" htmlproofer_ignore="false" >}} | bash
nano ~/.bashrc
source ~/.bashrc
nvm install 18
git clone git@gitlab.com:c2platform/website.git
cd website
npm install
```

Once you've made your working copy of the site repo, from the repo root folder, run:

```bash
hugo server
```

**Note**: the `hugo` snap alone is not enough. Without `npm install` you will get an error message:

> Error: error building site: POSTCSS: failed to transform "css/search.css" (text/css). Check your PostCSS installation; install with "npm install postcss-cli". See {{< external-link url="https://gohugo.io/hugo-pipes/postcss/:" htmlproofer_ignore="false" >}} this feature is not available in your current Hugo version, see {{< external-link url="https://goo.gl/YMrWcn" htmlproofer_ignore="false" >}} for more information

You can find out more about how to install Hugo for your environment in our
{{< external-link url="https://www.docsy.dev/docs/getting-started/#prerequisites-and-installation" text="Getting started" htmlproofer_ignore="false" >}} guide.

## Running a container locally

Alternatively you can run this website inside a {{< external-link url="https://docs.docker.com/" text="Docker" htmlproofer_ignore="false" >}} container, the container runs with a volume bound to the `docsy-example`
folder. This approach doesn't require you to install any dependencies other than `podman`.

```bash
sudo apt install podman -y
```

After that, you can run

```bash
podman run --rm -it   -v $(pwd):/src   -p 1313:1313   docker.io/klakegg/hugo:ext-ubuntu   server
```

For more information see the {{< external-link url="https://docs.docker.com/compose/gettingstarted/" text="Docker Compose documentation" htmlproofer_ignore="false" >}}.
To stop the container, on your terminal window, press **Ctrl + C**.

## Verify that the service is working.

Open your web browser and type `http://localhost:1313` in your navigation bar. You can now make changes to the website content and those changes will immediately show up in your browser after you save.

## PlantUML support

PlantUML diagrams are supported in website by adding {{< external-link url="https://gohugo.io/templates/shortcode-templates/" text="Hugo Shortcode" htmlproofer_ignore="false" >}} file {{< external-link url="https://gitlab.com/c2platform/website/-/blob/master/layouts/shortcodes/plantuml.html" text="layouts/shortcodes/plantuml.html" htmlproofer_ignore="false" >}} and Javascript library {{< external-link url="https://gitlab.com/c2platform/website/-/blob/master/static/js/rawdeflate.js" text="static/js/rawdeflate.js" htmlproofer_ignore="false" >}} downloaded from {{< external-link url="https://github.com/johan/js-deflate" text="johan/js-deflate" htmlproofer_ignore="false" >}}.

```
{{< plantuml id="eg" >}}
Alice -> Bob: test
{{< /plantuml >}}
```

## Validate HTML pre-commit

```bash
#!/bin/sh
rm -rf public || true
hugo
cd public
find . -name "*.html" -type f -exec sed -i -E 's/(<[^>]* id="plantuml-[^"]*")(.*?>)/\1 data-proofer-ignore\2/g' {} \;
find . -name "*.html" -type f -exec sed -i 's/<a class="td-offset-anchor"><\/a>/<a class="td-offset-anchor" data-proofer-ignore><\/a>/g' {} \;
htmlproofer --allow-missing-href --ignore-empty-alt --ignore-missing-alt --no-enforce-https 2>&1 | tee ../htmlproofer.log
if grep -q "following failures were found" ../htmlproofer.log; then
    echo "Failures found. Exiting."
    exit 1
fi
```

## Test on GitLab Runner

```bash
export IMAGE_HTML_PROOFER=registry.gitlab.com/c2platform/docker/htmlproofer:5.0.7
docker run --rm -i -v $(pwd):/src ${IMAGE_HTMLPROOFER} --allow-missing-href --ignore-empty-alt --ignore-missing-alt --no-enforce-https 2>&1 | tee ../htmlproofer.log
```

## Snippets

Via `Ctrl+Space` access markdown snippets like `relref`, and `external-link`. See {{< external-link url="https://gitlab.com/c2platform/website/-/blob/master/.vscode/markdown.code-snippets" text="markdown.code-snippets" htmlproofer_ignore="false" >}}

## Troubleshooting

See {{< external-link url="https://github.com/google/docsy-example" text="Docsy Example" htmlproofer_ignore="false" >}} project.

## Links

* {{< external-link url="https://gitlab.com/c2platform/website/-/pipelines" text="Pipelines" htmlproofer_ignore="false" >}}
* {{< external-link url="https://docsy.dev/docs" text="Docsy user guide" htmlproofer_ignore="false" >}}
* {{< external-link url="https://github.com/google/docsy" text="Docsy" htmlproofer_ignore="false" >}}
* {{< external-link url="https://example.docsy.dev" text="example.docsy.dev" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gohugo.io/hugo-modules/use-modules/#use-a-module-for-a-theme" text="Hugo theme module" htmlproofer_ignore="false" >}}


---


<!--
id | 45423570
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/website" data-proofer-ignore>c2platform/website&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
