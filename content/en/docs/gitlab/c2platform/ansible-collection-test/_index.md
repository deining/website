---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-test
tags: ['ansible', 'c2platform', 'collection', 'test']
title: "Ansible Collection - c2platform.test"
weight: 20
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-test"><code>c2platform/ansible-collection-test</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-test/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-test/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-test/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-test/-/pipelines)

C2 Platform Ansible Collection with Test Roles

- [Roles](#roles)
- [Plugins](#plugins)

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-test/-/blob/master/roles/microk8s" text="microk8s" htmlproofer_ignore="false" >}} new role for {{< external-link url="https://microk8s.io/" text="MicroK8s" htmlproofer_ignore="false" >}}.


## Plugins


---


<!--
id | 39049087
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-test" data-proofer-ignore>c2platform/ansible-collection-test&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
