---
categories: GitLab Projects
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: docker-winserver
tags: ['ansible', 'docker', 'image', 'molecule', 'podman', 'windows']
title: "docker-winserver"
weight: 15
---

GitLab: <a href="https://gitlab.com/c2platform/rws/docker-winserver"><code>c2platform/rws/docker-winserver</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
FROM microsoft/windowsservercore:latest
RUN powershell.exe -Command \
    wget {{< external-link url="https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1" htmlproofer_ignore="false" >}} -Outfile c:\remoting.ps1; \
    powershell.exe -ExecutionPolicy ByPass -File c:\remoting.ps1 -EnableCredSSP; \
    $password = ConvertTo-SecureString "Supersecure" -AsPlainText -Force; \
    Set-LocalUser -Name administrator -Password $password; \
    Enable-LocalUser -Name "Administrator"; \
    Dism.exe /online /Cleanup-Image /StartComponentCleanup /ResetBase
EXPOSE 5986



---


<!--
id | 50335523
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/rws/docker-winserver" data-proofer-ignore>c2platform/rws/docker-winserver&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
