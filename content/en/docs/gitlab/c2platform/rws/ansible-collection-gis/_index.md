---
categories: ['GitLab Projects', 'Ansible Collection']
description: "C2 Platform Ansible Collection for GIS"
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-gis
tags: ['ansible', 'ansible-collection', 'arcgis', 'c2platform', 'collection', 'fme', 'geo', 'gis', 'windows']
title: "Ansible Collection - c2platform.gis
"
weight: 11
---

GitLab: <a href="https://gitlab.com/c2platform/rws/ansible-collection-gis"><code>c2platform/rws/ansible-collection-gis</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/rws/ansible-collection-gis/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/pipelines)

C2 Platform Ansible Collection for ArcGIS Server, Pro, Portal, FME, etc

## Roles

### ArcGIS Enterprise

* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/arcgis_server" text="arcgis_server" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/arcgis_datastore" text="arcgis_datastore" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/arcgis_portal" text="arcgis_portal" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/arcgis_webadaptor" text="arcgis_webadaptor" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/arcgis_federation" text="arcgis_federation" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/arcgis_pro" text="arcgis_pro" htmlproofer_ignore="false" >}}

### Checkmk

* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/bridge" text="bridge" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/cmk_agent" text="cmk_agent" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/cmk_server" text="cmk_server" htmlproofer_ignore="false" >}}

### FME

* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/fme_flow" text="fme_flow" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/tomcat" text="tomcat" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/java" text="java" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/roles/pgadmin" text="pgadmin" htmlproofer_ignore="false" >}}


---


<!--
id | 46084992
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/rws/ansible-collection-gis" data-proofer-ignore>c2platform/rws/ansible-collection-gis&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
