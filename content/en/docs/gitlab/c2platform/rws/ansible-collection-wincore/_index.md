---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-wincore
tags: ['ansible', 'c2platform', 'chocolatey', 'collection', 'core', 'windows']
title: "Ansible Collection - c2platform.wincore
"
weight: 12
---

GitLab: <a href="https://gitlab.com/c2platform/rws/ansible-collection-wincore"><code>c2platform/rws/ansible-collection-wincore</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/rws/ansible-collection-wincore/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/commits/master) [![Latest Release](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/releases)

C2 Platform generic roles for MS Windows that are used by all or some other roles. These roles typically don't create services / processes on target node but are dependencies e.g. packages required by those roles. Or these roles help with Ansible provisioning for example offers generic Ansible modules, filters etc.

- [Roles](#roles)
- [Plugins](#plugins)
  - [`win_certificate`](#win_certificate)
  - [`win_package_facts`](#win_package_facts)
- [Filters](#filters)


## Roles

* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/blob/master/roles/download" text="download" htmlproofer_ignore="false" >}} manage downloads.
* {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/blob/master/roles/win" text="win" htmlproofer_ignore="false" >}} manage users, features, services, scheduled_tasks, packages ( chocolatey), files, directories etc.

## Plugins

Module plugins:

### `win_certificate`

The `c2platform.wincore.win_certificate` module allows you to create TLS/SSL
certificates. It provides four variables: `dns_name`, `validity_days` (default:
`365`), `store_location` (default: `Cert:\LocalMachine\My`), and `state`
(default: `present`). The example below demonstrates how to create a TLS/SSL
certificate and use it to configure an IIS HTTPS Binding on port 443 using
module `community.windows.win_iis_webbinding`.

```yaml
- name: Manage certificate
  c2platform.wincore.win_certificate:
    dns_name: "{{ ansible_hostname }}"
    validity_days: 3650
    state: present
  register: _cert

- name: Add a HTTPS binding
  community.windows.win_iis_webbinding:
    name: "{{ arcgis_webadaptor_ssl_certificate_iis_web_site_name }}"
    protocol: https
    port: 443
    ip: '*'
    certificate_hash: "{{ _cert.thumbPrint }}"
    state: present
  when: '"thumbPrint" in _cert'
```

### `win_package_facts`

To retrieve a list of installed packages and store it in the Ansible fact
`installed_packages`, you can utilize the `c2platform.wincore.win_package_facts`
module. Below is an example that demonstrates how to use this module to install
Microsoft Web Deploy, but only if it is not already installed.

```yaml
- name: Get Installed packages
  c2platform.wincore.win_package_facts:

- name: Display Installed Packages
  debug:
    var: ansible_facts.installed_packages

- name: Install WebDeploy for IIS
  ansible.windows.win_package:
    path: '{{ arcgis_webadaptor_temp_dir["path"] }}\{{ arcgis_webadaptor_versions[arcgis_webadaptor_version]["webdeploy"]["url"] | basename }}'
    arguments: 'ADDLOCAL=ALL LicenseAccepted="0"'
    state: present
  when: '"Microsoft Web Deploy 3.6" not in ansible_facts.installed_packages'
```

In this example, the `win_package_facts` module is used to collect information
about installed packages on the Windows system and store it in the
`installed_packages` fact. Additionally, it demonstrates how to conditionally
install Microsoft Web Deploy only if it is not already present in the list of
installed packages.


## Filters

*


---


<!--
id | 46695745
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/rws/ansible-collection-wincore" data-proofer-ignore>c2platform/rws/ansible-collection-wincore&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
