---
categories: GitLab Projects
description: "Ansible Inventory project for RWS GIS Platform reference implementation."
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-gis
tags: ['ansible', 'ansible-inventory', 'ansible-playbook', 'arcgis', 'gis', 'lxd', 'vagrant', 'virtualbox']
title: "RWS GIS Platform Ansible Project
"
weight: 14
---

GitLab: <a href="https://gitlab.com/c2platform/rws/ansible-gis"><code>c2platform/rws/ansible-gis</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


The RWS GIS Platform Ansible Project is hosted on GitLab and functions as the central
[Ansible Inventory Project]({{< relref path="/docs/concepts/ansible/projects" >}})
dedicated to supporting the Rijkswaterstaat (RWS) GIS Platform.

- [Purpose](#purpose)
- [Overview](#overview)
- [How-to](#how-to)
- [Variables](#variables)
  - [`gs_download_temp_dir`](#gs_download_temp_dir)
  - [TODO](#todo)


## Purpose

This project has two primary objectives:

1. **Supporting RWS Development Environment:** The RWS GIS Platform Ansible
   Project is an integral part of the RWS development environment, serving as a
   cornerstone for the RWS Ansible collections. It provides a structured
   inventory and automation framework that streamlines the development and
   management of various Ansible-based tasks and workflows within the RWS GIS
   Platform.
1. **Open-Source Reference Implementation:** Beyond its role within RWS, this
   project also operates as an open-source reference implementation. It offers a
   valuable resource for showcasing diverse automation solutions and
   exemplifying industry best practices. By doing so, it encourages
   collaboration and knowledge sharing among the wider automation community.

In summary, the RWS GIS Platform Ansible Project plays a pivotal role in
enhancing the efficiency of RWS operations while contributing to the broader
open-source ecosystem by serving as a model for effective automation practices.


## Overview

```plantuml
@startuml
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "")

Boundary(laptop, "", $type="") {
  Boundary(gis, "gis.c2platform.org", $type="") {
    System(agpro1, "ArcGIS Portal & Web Adaptors", "gsd-agportal1 8.103", "") {
      Container(wa1, "Web Adaptor", "")
      Container(wa2, "Web Adaptor ", "")
      Container(portal1, "ArcGIS Portal", "")
    }
    System(server_datastore, "ArcGIS Server & Datastore", "gsd-server1 8.100") {
      Container(agserver1, "ArcGIS Server", "")
      Container(datastore, "ArcGIS Datastore", "")
    }
    'Container(rproxy, "Reverse Proxy", "gsd-rproxy1 5.205")
  }
}

Rel(engineer, wa1, "", "https\n443")
Rel(engineer, wa2, "", "https\n443")
Rel(wa1, portal1, "", "")
Rel(wa2, agserver1, "", "")
Rel(portal1, agserver1, "", "")
Rel(agserver1, datastore, "", "")
@enduml
```

## How-to

Step-by-step instructions on what you can do with this project are published on
C2 Platform website, see
[How-to Rijkswaterstaat | C2 Platform]({{< relref path="/docs/howto/rws" >}})

## Variables

This section provides documentation for project-specific variables utilizing the
`gs_` prefix, which are introduced and employed within this inventory project.
To better understand the conventions for variable prefixes, you can refer to the
[Variable prefix | C2 Platform]({{< relref path="/docs/guidelines/coding/var-prefix" >}})
documentation.

### `gs_download_temp_dir`

In this project, several roles utilized within the plays rely on the
`c2platform.wincore.download`. One key variable, `gs_download_temp_dir`, is defined
in `group_vars/all/main.yml`, as illustrated below:

```yaml
gs_download_temp_dir:
  path: C:\vagrant\tmp
  # create: yes
  # delete: no
  # recursive: true
  delete_files: false
```
This variable is declared at the global level (in `all.yml`) to ensure that its
settings can be conveniently reused throughout the inventory project. As an
example, within `group_vars/fme/main.yml`, you can find the following line:

```yaml
fme_flow_temp_dir: "{{ gs_download_temp_dir }}"
```
###  TODO
.sync_folders.yml voor software repo

---


<!--
id | 46084829
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/rws/ansible-gis" data-proofer-ignore>c2platform/rws/ansible-gis&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
