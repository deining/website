---
categories: GitLab Projects
description: "Ansible Execution Environment for Rijkswaterstaat (RWS)"
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-execution-environment
tags: ['Kerberos', 'ansible', 'awx', 'python3', 'windows']
title: "Ansible Execution Environment for Rijkswaterstaat (RWS)"
weight: 13
---

GitLab: <a href="https://gitlab.com/c2platform/rws/ansible-execution-environment"><code>c2platform/rws/ansible-execution-environment</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/rws/ansible-execution-environment/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/rws/ansible-execution-environment/-/commits/master) [![Latest Release](https://gitlab.com/c2platform/rws/ansible-execution-environment/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/rws/ansible-execution-environment/-/releases)

This GitLab project provides an Ansible Execution Environment for Rijkswaterstaat (RWS) that is suitable for Ansible Automation Platform (AAP) and AWX. The purpose of this execution environment is to have more control over Python and Ansible versions, addressing limitations with the default Python version of CentOS 8.

- [Features](#features)
- [Getting Started](#getting-started)
- [Creating a New Image](#creating-a-new-image)
- [Creating a New Release](#creating-a-new-release)
- [Repository Files](#repository-files)
- [Verify](#verify)


## Features

* Enhanced control over Python and Ansible versions using {{< external-link url="https://github.com/pyenv/pyenv" text="`pyenv`" htmlproofer_ignore="false" >}}.
* Support for Kerberos and WinRM, which are not available by default in CentOS 8.
* Easy customization and contribution of new versions of the execution environment.

> Note: This image is also seamlessly integrated into GitLab pipelines. As an
> example, the Ansible collection
> {{< external-link url="https://gitlab.com/c2platform/rws/ansible-collection-wincore" text="`c2platform.wincore`" htmlproofer_ignore="false" >}}
> utilizes this image. Refer to the `.gitlab-ci.yml` for detailed integration
> instructions.

## Getting Started

1. Clone the GitLab repository to your local machine.
1. Modify the `execution-environment.yml` file to make the necessary changes to the environment. This file is used by ansible-builder.
1. Run `ansible-builder create` to update the `context` folder with a `Dockerfile` and a `_build` directory containing all the build artifacts.
1. Perform local testing by running `ansible-builder build` to create and test the image locally.

## Creating a New Image

To create a new image, you can push your changes to the GitLab repository. This will trigger a pipeline that automatically creates a new image based on the latest changes.

## Creating a New Release

If you want to create a new release of the execution environment, update the version in the `VERSION` file and push the changes to the repository. Once the changes are pushed, you can manually trigger the pipeline step to create the GitLab release. The release will include the updated version of the execution environment.

## Repository Files

1. `.gitlab-ci.yml`: This file contains the CI/CD pipeline configuration. It defines the stages and jobs required for building, releasing, and deploying the execution environment.
1. `execution-environment.yml`: This file specifies the dependencies and additional build steps for creating the execution environment. Modify this file to make changes to the environment.
1. `VERSION`: This file contains the version information of the execution environment. Update this file when creating a new release.

Please refer to the respective files for detailed configuration and usage instructions.


## Verify

```bash
docker pull registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.0
docker run -it registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.0 bash
python --version
pip3 list
```

<details>
  <summary><kbd>Show me</kbd></summary>

```bash
(c2d) vagrant@c2d-xtop:/ansible-execution-environment/context$ docker run -it registry.gitlab.com/c2platform/rws/ansible-execution-environment:0.1.0 bash
(rws) bash-4.4# python --version
Python 3.10.6
(rws) bash-4.4# pip3 list | grep ansible
ansible-compat     4.1.2
ansible-core       2.15.0
ansible-lint       6.17.2
(rws) bash-4.4#
```

</details>


---


<!--
id | 47388157
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/rws/ansible-execution-environment" data-proofer-ignore>c2platform/rws/ansible-execution-environment&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
