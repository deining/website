---
categories: GitLab Projects
description: "Ansible Inventory project for C2 Platform reference implementation."
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible
tags: ['ansible', 'ansible-inventory', 'ansible-playbook', 'lxd', 'vagrant', 'virtualbox']
title: "C2 Ansible Inventory"
weight: 10
---

GitLab: <a href="https://gitlab.com/c2platform/ansible"><code>c2platform/ansible</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![pipeline status](https://gitlab.com/c2platform/ansible/badges/master/pipeline.svg)](https://gitlab.com/c2platform/ansible/-/commits/master) [![Latest Release](https://gitlab.com/c2platform/ansible/-/badges/release.svg)](https://gitlab.com/c2platform/ansible/-/releases)

The C2 Ansible Inventory project is a GitLab project that serves as the
[Ansible Inventory project]({{< relref path="/docs/concepts/ansible/projects" >}}) for the
{{< external-link url="https://c2platform.org" text="C2 Platform" htmlproofer_ignore="false" >}}.
## Purpose

The primary purpose of this project is to serve as an essential component of the
C2 Platform development environment for the
[C2 Platform Ansible Collections]({{< relref path="/docs/concepts/ansible/projects/collections" >}}).
Additionally, it also acts as a
[reference implementation]({{< relref path="/docs/concepts/ri" >}}),
showcasing various automation solutions and demonstrating best practices.

## Features

* Pre-configured inventory structure: The project includes a well-organized
  inventory structure with pre-defined host and group configurations, making it
  easier to manage and scale the Ansible deployments.
* Group variables and vault files: The project incorporates group variables and
  vault files to securely store sensitive data, ensuring that confidential
  information remains protected during provisioning and deployment.
* Extensive Ansible collections and roles: The project demonstrates a wide range
  of Ansible collections and roles specifically developed for the C2 Platform.
  These can be used as a reference to understand best practices and
  implementation details for various tasks.
* The project has configuration for the creation of all kinds of services using
  Ansible. For a better understanding of its scope, capabilities refer to the
  [how-to]({{< relref path="/docs/howto/c2" >}}) section.

## Getting Started

To get started with this project, please follow the
[Getting Started]({{< relref path="/docs/howto/dev-environment" >}})
guide. It provides detailed instructions on setting up the necessary prerequisites,
configuring the inventory, and deploying desired services using Ansible.

<!-- TODO contributions -->

## How-to

See [how-to]({{< relref path="/docs/howto/c2" >}}) section on C2 Platform website.

## Guidelines

For software development guidelines related to this project, please refer to the
[C2 Platform Website]({{< relref path="/docs/guidelines" >}}).


## License

This project is licensed under the {{< external-link url="https://gitlab.com/c2platform/ansible/-/blob/master/LICENSE" text="MIT License" htmlproofer_ignore="false" >}}, granting users the
freedom to use, modify, and distribute the codebase as per the license terms.

## Support

<!-- TODO test, verify -->

If you encounter any issues or have questions regarding this project, please
create an {{< external-link url="https://gitlab.com/c2platform/ansible/-/issues/service_desk" text="issue" htmlproofer_ignore="false" >}}
in the GitLab repository. We will strive to address and resolve any queries or
problems as promptly as possible.


---


<!--
id | 39438186
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible" data-proofer-ignore>c2platform/ansible&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
