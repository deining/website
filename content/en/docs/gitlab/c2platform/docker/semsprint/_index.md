---
categories: GitLab Projects
description: "None"
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: semsprint
tags: []
title: "semsprint"
weight: 15
---

GitLab: <a href="https://gitlab.com/c2platform/docker/semsprint"><code>c2platform/docker/semsprint</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
### Node Express template project

This project is based on a GitLab {{< external-link url="https://docs.gitlab.com/ee/gitlab-basics/create-project.html" text="Project Template" htmlproofer_ignore="false" >}}.

Improvements can be proposed in the {{< external-link url="https://gitlab.com/gitlab-org/project-templates/express" text="original project" htmlproofer_ignore="false" >}}.

### CI/CD with Auto DevOps

This template is compatible with {{< external-link url="https://docs.gitlab.com/ee/topics/autodevops/" text="Auto DevOps" htmlproofer_ignore="false" >}}.

If Auto DevOps is not already enabled for this project, you can {{< external-link url="https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops" text="turn it on" htmlproofer_ignore="false" >}} in the project settings.

### Developing with Gitpod

This template has a fully-automated dev setup for {{< external-link url="https://docs.gitlab.com/ee/integration/gitpod.html" text="Gitpod" htmlproofer_ignore="false" >}}.

If you open this project in Gitpod, you'll get all Node dependencies pre-installed and Express will open a web preview.


---


<!--
id | 44200724
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/docker/semsprint" data-proofer-ignore>c2platform/docker/semsprint&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
