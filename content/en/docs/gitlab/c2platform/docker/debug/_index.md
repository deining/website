---
categories: GitLab Projects
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: debug
tags: []
title: "Debug Container with Oracle Instantclient"
weight: 10
---

GitLab: <a href="https://gitlab.com/c2platform/docker/debug"><code>c2platform/docker/debug</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


This Dockerfile sets up a Debug Container based on the UBI7 image from the Red Hat registry with Oracle Instantclient and some other useful tools installed. This container is intended to be used for debugging purposes in OpenShift. The project is hosted on {{< external-link url="https://gitlab.com/c2platform/docker/debug/" text="GitLab" htmlproofer_ignore="false" >}} and has a {{< external-link url="https://gitlab.com/c2platform/docker/debug/-/pipelines" text="pipeline" htmlproofer_ignore="false" >}} that creates an image in the {{< external-link url="https://gitlab.com/c2platform/docker/debug/container_registry" text="Container Registry" htmlproofer_ignore="false" >}} of the project each time a change is pushed. Commits on the master branch are published as "latest", and git tags are published as image tags.

The provided code snippet below shows how the container can be used in an OpenShift environment created using CodeReady Containers (CRC). It logs in to the OpenShift instance, creates a new project, pulls the image from the GitLab Container Registry, tags it, pushes it to the OpenShift instance, and starts a new container, allowing the user to access it with a bash terminal.

See also {{< external-link url="https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-crc.md" text="How-to CodeReady Containers (CRC)" htmlproofer_ignore="false" >}}.

```bash
export HOST=default-route-openshift-image-registry.apps-crc.testing
export CRC_NAMESPACE=debug-test
eval $(crc oc-env)
oc login -u developer -p developer {{< external-link url="https://api.crc.testing:6443" htmlproofer_ignore="true" >}}
oc project $CRC_NAMESPACE
docker login -u $(oc whoami) -p $(oc whoami -t) $HOST
docker pull registry.gitlab.com/c2platform/docker/debug
docker tag registry.gitlab.com/c2platform/docker/debug:latest $HOST/$CRC_NAMESPACE/debug:latest
docker push $HOST/$CRC_NAMESPACE/debug:latest
oc run debug --image=$HOST/$CRC_NAMESPACE/debug:latest
oc exec -it debug -- /bin/bash
```

<details>
  <summary>Show me</summary>

```bash
user@ubuntu22:~$ export HOST=default-route-openshift-image-registry.apps-crc.testing
export CRC_NAMESPACE=debug-test
user@ubuntu22:~$ eval $(crc oc-env)
oc login -u developer -p developer {{< external-link url="https://api.crc.testing:6443" htmlproofer_ignore="true" >}}
Login successful.

You have access to the following projects and can switch between them with 'oc project <projectname>':

  * debug-test
    openshift

Using project "debug-test".
user@ubuntu22:~$ oc project $CRC_NAMESPACE
docker login -u $(oc whoami) -p $(oc whoami -t) $HOST
Already on project "debug-test" on server "https://api.crc.testing:6443".
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
WARNING! Your password will be stored unencrypted in /home/ostraaten/.docker/config.json.
Configure a credential helper to remove this warning. See
{{< external-link url="https://docs.docker.com/engine/reference/commandline/login/#credentials-store" htmlproofer_ignore="false" >}}

Login Succeeded
user@ubuntu22:~$ docker pull registry.gitlab.com/c2platform/docker/debug
Using default tag: latest
latest: Pulling from c2platform/docker/debug
Digest: sha256:c0b0b5c8ddbc513be538eeb752a1bac8956b4248121d4576f7f3016c5dec6872
Status: Image is up to date for registry.gitlab.com/c2platform/docker/debug:latest
registry.gitlab.com/c2platform/docker/debug:latest
user@ubuntu22:~$ docker tag registry.gitlab.com/c2platform/docker/debug:latest $HOST/$CRC_NAMESPACE/debug:latest
user@ubuntu22:~$ docker push $HOST/$CRC_NAMESPACE/debug:latest
The push refers to repository [default-route-openshift-image-registry.apps-crc.testing/debug-test/debug]
df3bed598496: Layer already exists
e2b3bc13e9a7: Layer already exists
087497746b72: Layer already exists
a6d542167482: Layer already exists
latest: digest: sha256:c0b0b5c8ddbc513be538eeb752a1bac8956b4248121d4576f7f3016c5dec6872 size: 1158
user@ubuntu22:~$ oc run debug --image=$HOST/$CRC_NAMESPACE/debug:latest
pod/debug created
user@ubuntu22:~$ oc exec -it debug -- /bin/bash
bash-4.2$ sqlplus

SQL*Plus: Release 21.0.0.0.0 - Production on Wed Jan 25 09:45:25 2023
Version 21.8.0.0.0

Copyright (c) 1982, 2022, Oracle.  All rights reserved.

Enter user-name:
```

</details>




---


<!--
id | 40942382
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/docker/debug" data-proofer-ignore>c2platform/docker/debug&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
