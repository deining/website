---
categories: GitLab Projects
description: "Docker-in-docker image based on `docker:20.10.16-dind` for the development environment. This image is intended to be used on the GitLab instance running on `c2d-gitlab`."
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: dind
tags: ['dind', 'docker', 'gitlab', 'image']
title: "Dind image"
weight: 10
---

GitLab: <a href="https://gitlab.com/c2platform/docker/c2d/dind"><code>c2platform/docker/c2d/dind</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


Dind image based on `docker:20.10.16-dind` for {{< external-link url="https://gitlab.c2platform.org" htmlproofer_ignore="true" >}} in the development environment. This image is intended to be used for GitLab instance running on `c2d-gitlab`.


---


<!--
id | 44719278
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/docker/c2d/dind" data-proofer-ignore>c2platform/docker/c2d/dind&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
