---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-azure_devops
tags: ['agent', 'ansible', 'azure', 'build', 'c2platform', 'cd', 'ci', 'collection', 'deployment', 'devops', 'linux']
title: "Ansible Collection - c2platform.azure_devops"
weight: 13
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-azure_devops"><code>c2platform/ansible-collection-azure_devops</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-azure_devops/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-azure_devops/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-azure_devops/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-azure_devops/-/pipelines)

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-azure_devops/-/blob/master/roles/build_agent" text="build_agent" htmlproofer_ignore="false" >}} create build or deployment agent


---


<!--
id | 39049117
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-azure_devops" data-proofer-ignore>c2platform/ansible-collection-azure_devops&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
