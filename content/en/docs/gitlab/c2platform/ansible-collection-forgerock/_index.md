---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-forgerock
tags: ['am', 'ansible', 'c2platform', 'collection', 'ds', 'forgerock', 'ig']
title: "Ansible Collection - c2platform.forgerock"
weight: 16
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-forgerock"><code>c2platform/ansible-collection-forgerock</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-forgerock/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-forgerock/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-forgerock/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-forgerock/-/pipelines)

Roles for {{< external-link url="https://www.forgerock.com/" text="ForgeRock" htmlproofer_ignore="false" >}} platform.

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-forgerock/-/blob/master/roles/ds" text="ds" htmlproofer_ignore="false" >}} ForgeRock Directory Services.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-forgerock/-/blob/master/roles/am" text="am" htmlproofer_ignore="false" >}} ForgeRock Access Management.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-forgerock/-/blob/master/roles/ig" text="ig" htmlproofer_ignore="false" >}} ForgeRock Identity Gateway.

## Plugins

Module plugins:

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-forgerock/-/blob/master/plugins/modules/amster_script_prepare.py" text="amster_scripts_prepare" htmlproofer_ignore="false" >}} facts for Amster scripts to create
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-forgerock/-/blob/master/plugins/modules/amster_script_prepare_execute.py" text="amster_scripts_prepare_execute" htmlproofer_ignore="false" >}} determine if scripts should run by setting `execute` fact

Filter plugins:

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-forgerock/-/blob/master/plugins/filter/am_filters.py" text="am_filters" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-forgerock/-/blob/master/plugins/filter/ds_filters.py" text="ds_filters" htmlproofer_ignore="false" >}}


---


<!--
id | 39808379
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-forgerock" data-proofer-ignore>c2platform/ansible-collection-forgerock&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
