---
categories: GitLab Group
description: "Example / template / reference projects that showcase the power and versatility of Ansible, GitOps, and Kubernetes. These projects are part of the esteemed GitLab Open Source Program, and they make full use of GitLab.
"
linkTitle: c2platform
tags: 
title: c2platform
weight: 1
---
