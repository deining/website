---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-dev
tags: ['ansible', 'c2platform', 'collection', 'common', 'dev', 'linux', 'windows']
title: "Ansible Collection - c2platform.dev"
weight: 15
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-dev"><code>c2platform/ansible-collection-dev</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-dev/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-dev/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-dev/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-dev/-/pipelines)

C2 Platform Ansible Collection with development roles. These roles are not intended to be used for production systems!

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-dev/-/blob/master/roles/oracle_database/README.md" text="oracle_database" htmlproofer_ignore="false" >}}.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-dev/-/blob/master/roles/desktop/README.md" text="desktop" htmlproofer_ignore="false" >}}.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-dev/-/blob/master/roles/graylog/README.md" text="graylog" htmlproofer_ignore="false" >}}.

## Plugins


---


<!--
id | 40905588
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-dev" data-proofer-ignore>c2platform/ansible-collection-dev&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
