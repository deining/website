---
categories: ['GitLab Projects', 'Ansible Collection']
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: ansible-collection-oracle
tags: ['ansible', 'c2platform', 'collection', 'oracle']
title: "Ansible Collection - c2platform.oracle"
weight: 19
---

GitLab: <a href="https://gitlab.com/c2platform/ansible-collection-oracle"><code>c2platform/ansible-collection-oracle</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


[![Pipeline Status](https://gitlab.com/c2platform/ansible-collection-oracle/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/ansible-collection-oracle/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/ansible-collection-oracle/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/ansible-collection-oracle/-/pipelines)

C2 Platform Ansible Collection for Oracle products.

## Roles

* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-oracle/-/blob/master/roles/sqplus" text="sqlplus" htmlproofer_ignore="false" >}}.
* {{< external-link url="https://gitlab.com/c2platform/ansible-collection-oracle/-/blob/master/roles/oracle" text="oracle" htmlproofer_ignore="false" >}}.

## Plugins


---


<!--
id | 40905725
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/ansible-collection-oracle" data-proofer-ignore>c2platform/ansible-collection-oracle&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
