---
categories: GitLab Projects
description: "None"
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: nodejs-openshift
tags: []
title: "NodeJS OpenShift Example Application and Pipeline ( NJ )"
weight: 10
---

GitLab: <a href="https://gitlab.com/c2platform/examples/nodejs-openshift"><code>c2platform/examples/nodejs-openshift</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


This project demonstrates a GitLab CI/CD pipeline to OpenShift. This project assumes you already have an OpenShift instance available similar to  {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/howto-crc.md" text="How-to CodeReady Containers (CRC)" htmlproofer_ignore="false" >}}. The pipeline builds and publishes images to the {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/container_registry" text="Container Registry" htmlproofer_ignore="false" >}} of this project. This project also contains OpenShift manifest files for managing two environments / projects / namespaces in OpenShift for this simple application. This project also has a simple {{< external-link url="https://robotframework.org/" text="Robot Framework" htmlproofer_ignore="false" >}} test.

- [Overview](#overview)
- [GitOps](#gitops)
- [Robot](#robot)
- [Gitlab Agent](#gitlab-agent)
  - [Install Helm](#install-helm)
  - [Register GitLab agent](#register-gitlab-agent)
  - [Install GitLab agent](#install-gitlab-agent)
- [Gitlab Runner](#gitlab-runner)
  - [Operator Hub](#operator-hub)
  - [Commandline](#commandline)
- [Verify](#verify)
  - [Create release](#create-release)
  - [Deploy](#deploy)
- [Build and test image locally](#build-and-test-image-locally)
- [Pulling images](#pulling-images)

## Overview

The pipeline, as defined in {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/.gitlab-ci.yml" text=".gitlab-ci.yml" htmlproofer_ignore="false" >}}, builds and publishes images to the {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/container_registry" text="Container Registry" htmlproofer_ignore="false" >}} of this project. The image contains a very simple NodeJS application that responds with a "hello" message that includes the image version. For example when accessing the "production" environment using URL {{< external-link url="https://frontend-njp.apps-crc.testing/" htmlproofer_ignore="true" >}} it will respond with message similar to:

> Hello World! Version: 0.1.3

The pipeline has five stages:

1. **Prepare**: This stage retrieves the version number from `package.json` and adds it to the `variables.env` file as variable `C2_VERSION`. The `variables.env` file is then stored as an artifact so it can be used by later stages.
2. **Build**: This stage pulls the latest image from the GitLab registry and builds a new image with the current commit information. The built image is then tagged with the commit SHA and pushed to the GitLab registry.
3. **Release** ( manual on master ): This stage creates a release in GitLab only when triggered manually on the master branch. The name, description, tag name, and ref of the release are determined using the variables `C2_VERSION`, `CI_COMMIT_SHA`, and `CI_PROJECT_TITLE`. Note: the creation of a Git tag, will run the pipeline again.
4. **Docker**: This stage tags the master branch as "latest" and any Git tag as the corresponding "tag name".
5. **Deploy** ( manual on tags ): This stage deploys the tagged image to production only on Git tags. This technically done by tagging the image with a `production` label. The "production" environment is configured to only use the image tagged with this label see {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/manifests/production.yml" text="manifests/production.yml" htmlproofer_ignore="false" >}}.
6. **Test**: this stage runs a simple {{< external-link url="https://robotframework.org/" text="Robot Framework" htmlproofer_ignore="false" >}} test of the NodeJS application running in OpenShift / CRC. See {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/test/chrome.robot" text="test/chrome.robot" htmlproofer_ignore="false" >}} and {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/test/firefox.robot" text="test/firefox.robot" htmlproofer_ignore="false" >}}.

{{< plantuml id="crc-remote" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")

Boundary(gitlab, "gitlab.com/c2platform", $type="") {
    Boundary(nj, "examples/nodejs-openshift", $type="") {
        Container(registry, "Registry", "")
        Container(pipeline, "Pipeline", "")
        Container(repo, "Repository", "git")
        Container(releases, "Releases", "")
    }
}

Rel(engineer, repo, "Push code", "")
Rel_Left(repo, pipeline, "trigger", "")
Rel(engineer, pipeline, "Create release", "")
Rel(engineer, pipeline, "Deploy", "")
Rel(pipeline, releases, "Create release", "")
Rel(pipeline, registry, "Push latest / tag", "")
Rel(pipeline, registry, "Deploy ( production tag )", "")
{{< /plantuml >}}

## GitOps

This example project uses **GitOps workflow** to manage OpenShift resources in two namespaces `nja` and `njp`. The first namespace is for is the "acceptance" / "staging" environment, the second for "production". See {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/manifests/staging.yml" text="manifests/staging.yml" htmlproofer_ignore="false" >}} and {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/manifests/production.yml" text="manifests/production.yml" htmlproofer_ignore="false" >}}. The diagram below only shows `njp`. The setup for `nja` and `njp` is identical with the exception of the fact that `njp` is configured to pull only images with tag `production`.

{{< plantuml id="crc-remote" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")
Person(user, "User")

System_Boundary(openshift, "CodeReady Containers ( CRC )", "openshift") {
    Container(agent, "Gitlab Agent", "openshift")
    Container(njp, "njp", "openshift project")
    'Container(nja, "nja", "openshift project")
}
Boundary(gitlab, "gitlab.com/c2platform", $type="") {
    Boundary(nj, "examples/nodejs-openshift", $type="") {
        Container(repo, "Repository", "git")
        Container(registry, "Registry", "registry")
    }
}

Rel(engineer, repo, "Push manifest changes", "")
Rel_Right(agent, repo, "Pull manifest changes", "")
' Rel_Up(agent, nja, "Create / manage project ( resources )", "")
Rel_Up(agent, njp, "Manage project / namespace", "")
Rel_Down(user, openshift, "", "")
Rel_Left(njp, registry, "Pull production image", "")
' Rel(nja, registry, "", "")
' Rel_Up(repo, registry, "", "")
{{< /plantuml >}}

The sequence diagram below illustrates the **GitOps workflow** in more detail. The setup in this project is based on the example project {{< external-link url="https://gitlab.com/gitlab-examples/ops/gitops-demo/k8s-agents" text="GitLab-examples / ops / GitOps demo / k8s-agents - GitLab Agent demo configuration · GitLab" htmlproofer_ignore="false" >}} and on the guidance in {{< external-link url="https://docs.gitlab.com/ee/user/clusters/agent/gitops.html" text="Using GitOps with a Kubernetes cluster" htmlproofer_ignore="false" >}}.


{{< plantuml id="participant D as" >}}
"Developer"
' participant A as "Application repository"
' participant M as "Manifest repository"
participant G as "Git repository"
participant K as "GitLab agent"
participant O as "OpenShift"

' D -> A : Pushing code changes
D -> G : Pushing manifest changes
' A -> M : Updating manifest
loop Regularly
K -> G : Watching changes
G -> K : Pulling changes
K -> O : Applying changes
end
{{< /plantuml >}}

Note: This example uses a single repository for both OpenShift manifest files and application code ( including the `Dockerfile` ). In real world setup it would make more sense to have a separate repository for the manifest files and than to have one or more repositories with pipelines push manifest changes. It would also make sense to use a tool like {{< external-link url="https://kustomize.io/" text="Kustomize" htmlproofer_ignore="false" >}} as is the case in the {{< external-link url="https://gitlab.com/gitlab-examples/ops/gitops-demo/k8s-agents" text="GitLab-examples / ops / GitOps demo / k8s-agents - GitLab Agent demo configuration · GitLab" htmlproofer_ignore="false" >}}.

## Robot

{{< plantuml id="crc-remote" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")
Person(user, "User")

System_Boundary(openshift, "CodeReady Containers ( CRC )", "openshift") {
    Container(runner, "Gitlab Runner", "openshift")
    Container(njp, "njp", "openshift project")
    'Container(nja, "nja", "openshift project")
}
Boundary(gitlab, "gitlab.com/c2platform", $type="") {
    Boundary(nj, "examples/nodejs-openshift", $type="") {
        Container(repo, "Repository", "git")
        Container(registry, "Registry", "registry")
        Container(pipeline, "Pipeline", "")
        Container(runner_gitlab, "Gitlab Runner", "gitlab")
    }
}

Rel(engineer, repo, "Push change", "")
Rel(repo, pipeline, "Trigger", "")
Rel(pipeline, runner, "Test", "stage")
Rel(pipeline, runner_gitlab, "Other", "stage")
Rel_Up(runner, njp, "Test requests", "robot")
Rel_Down(user, openshift, "", "")
Rel_Left(runner, registry, "Pull robot \nimage", "")
{{< /plantuml >}}

## Gitlab Agent

When using OpenShift CodeReady Containers ( CRC ) as described in {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/howto-crc.md" text="How-to CodeReady Containers (CRC)" htmlproofer_ignore="false" >}} you can install the Gitlab Agent by using the operator that comes pre-installed. This section however describes the manual install. After installation the agent will create two namespaces and deploy the application.

### Install Helm

To install the agent in OpenShift we will need {{< external-link url="https://helm.sh/" text="Helm" htmlproofer_ignore="false" >}}. This can be installed with the following command:

```bash
curl {{< external-link url="https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3" htmlproofer_ignore="false" >}} | bash
```

See also Helm {{< external-link url="https://helm.sh/docs/intro/quickstart/" text="Quickstart Guide" htmlproofer_ignore="false" >}}
### Register GitLab agent

See {{< external-link url="https://docs.gitlab.com/ee/user/clusters/agent/install/index.html" text="Installing the agent for Kubernetes | GitLab" htmlproofer_ignore="false" >}}.

First step is to create an agent file. See {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/.gitlab/agents/c2-openshift/config.yaml" text="gitlab/agents/c2-openshift/config.yaml" htmlproofer_ignore="false" >}}.

With `config.yaml` created and pushed to the repository, the agent can be registered. In Gitlab.com navigate to **Infrastructure** → {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/clusters" text="Kubernetes cluster" htmlproofer_ignore="false" >}} and select **Connect a cluster**. Select the agent ( `c2-openshift` in our example ) and register it. This will output some `helm` commands that we need to execute to install the agent in OpenShift.

### Install GitLab agent

The agent is then installed by running commands similar to below. Note: the exact command and the token are shown when you register the agent. On OpenShift we require some extra settings that are passed using a YAML file `gitlab-agent.yml`.


```bash
helm repo add gitlab {{< external-link url="https://charts.gitlab.io" htmlproofer_ignore="false" >}}
helm repo update
helm upgrade --install $AGENT_NAME gitlab/gitlab-agent \
    --namespace gitlab-agent-$AGENT_NAME> \
    --create-namespace \
    --set image.tag=v15.9.0-rc1 \
    --set config.token=$TOKEN> \
    --set config.kasAddress=wss://kas.gitlab.com \
    -f gitlab-agent.yml
```

The file `gitlab-agent.yml`

```yaml
securityContext:
  allowPrivilegeEscalation: false
  capabilities:
    drop:
    - ALL
  runAsNonRoot: true
  seccompProfile:
    type: RuntimeDefault
```

## Gitlab Runner

Navigate to **Settings** → **CI/CD** → {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/settings/ci_cd" text="Runners" htmlproofer_ignore="false" >}} and get the **registration token**.

### Operator Hub

```
onknows@io1:~$ oc apply -f gitlab-runner-secret.yml
secret/gitlab-runner-secret created
onknows@io1:~$
onknows@io1:~$ oc project nja
Now using project "nja" on server "https://api.crc.testing:6443".
onknows@io1:~$ oc apply -f gitlab-runner.yml
runner.apps.gitlab.com/gitlab-runner created
```

### Commandline

For Helm3

```bash
export NAMESPACE=nja
export GITLAB_RUNNER_CONFIG=gitlab-runner-config.yaml
oc login -u kubeadmin -p whatever {{< external-link url="https://api.crc.testing:6443" htmlproofer_ignore="true" >}}
helm repo add gitlab {{< external-link url="https://charts.gitlab.io" htmlproofer_ignore="false" >}}
helm repo update gitlab
helm search repo -l gitlab/gitlab-runner  # available versions to install
helm install --namespace $NAMESPACE gitlab-runner -f $GITLAB_RUNNER_CONFIG gitlab/gitlab-runner
```

See {{< external-link url="https://docs.gitlab.com/runner/install/kubernetes.html" text="GitLab Runner Helm Chart | GitLab" htmlproofer_ignore="false" >}}.


```bash
onknows@io1:~/git/gitlab/c2/examples/nodejs-openshift$ helm install --namespace $NAMESPACE gitlab-runner -f $GITLAB_RUNNER_CONFIG gitlab/gitlab-runner
W0220 09:50:36.216589 2988057 warnings.go:70] would violate PodSecurity "restricted:v1.24": seccompProfile (pod or container "gitlab-runner" must set securityContext.seccompProfile.type to "RuntimeDefault" or "Localhost")
NAME: gitlab-runner
LAST DEPLOYED: Mon Feb 20 09:50:36 2023
NAMESPACE: nj-gitlab-runners
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Your GitLab Runner should now be registered against the GitLab instance reachable at: "http://gitlab.com"

Runner namespace "nj-gitlab-runners" was found in runners.config template.
```

## Verify

In this project the technical abbreviation used for this project is `nj`. The two projects / namespaces / environments are derived from this. Acceptance / staging is `nja` and production is `njp`. The manifest files in this project will allow the application to be accessed using {{< external-link url="https://frontend-nja.apps-crc.testing/" htmlproofer_ignore="false" >}} and {{< external-link url="https://frontend-njp.apps-crc.testing/" htmlproofer_ignore="true" >}}. See {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/manifests/staging.yml" text="manifests/staging.yml" htmlproofer_ignore="false" >}} and {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/manifests/production.yml" text="manifests/production.yml" htmlproofer_ignore="false" >}}

The environments should show something like

> Hello World! Version: 0.1.3

### Create release

To create a "release" change the `version` in {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/blob/master/package.json" text="package.json" htmlproofer_ignore="false" >}} to the version that you want to build for example `0.1.4`. This will trigger a pipeline that you monitor via {{< external-link url="https://gitlab.com/c2platform/examples/nodejs-openshift/-/pipelines" text="pipelines" htmlproofer_ignore="false" >}}.

{{< external-link url="https://frontend-nja.apps-crc.testing/" htmlproofer_ignore="false" >}}

### Deploy

## Build and test image locally

The project can build and tested locally by executing the following command:

```bash
docker build -t nodejs:latest . && docker run -p 3000:3000 --rm nodejs:latest
```

The output can be verified by using the following command in the terminal or navigating to {{< external-link url="http://localhost:3000" htmlproofer_ignore="true" >}} in a browser.

```bash
curl {{< external-link url="http://localhost:3000" htmlproofer_ignore="true" >}}  # returns Hello World!
```

## Pulling images

Images can be pulled using the following command:

```bash
docker pull registry.gitlab.com/c2platform/examples/nodejs-openshift:latest
```




---


<!--
id | 43044318
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/examples/nodejs-openshift" data-proofer-ignore>c2platform/examples/nodejs-openshift&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
