---
categories: GitLab Projects
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: gitlab-docker-build
tags: []
title: "Build Docker image on Kubernetes using GitLab"
weight: 10
---

GitLab: <a href="https://gitlab.com/c2platform/examples/kubernetes/gitlab-docker-build"><code>c2platform/examples/kubernetes/gitlab-docker-build</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


This project demonstrates how we can build and release Docker image `registry.gitlab.com/c2platform/examples/kubernetes/gitlab-docker-build` on {{< external-link url="https://microk8s.io/" text="MicroK8s" htmlproofer_ignore="false" >}} Kubernetes cluster running on `c2d-ks1` using **GitLab Runner** running in the cluster. In this project the **GitLab Runner** is installed in the namespace `nja` where the `nj` "helloworld" application is running.

- [Overview](#overview)
- [Create Kubernetes cluster](#create-kubernetes-cluster)
- [Install GitLab Runner](#install-gitlab-runner)
- [Run pipeline](#run-pipeline)

## Overview

{{< plantuml id="crc-remote" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")
'Person(user, "User")

Boundary(local, "local", $type="high-end dev laptop") {
    'Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1")
    Boundary(c2d_ks, "Kubernetes", $type="c2d-ks1") {
        Boundary(c2d_k8s_nja, "nja", $type="namespace") {
            Container(runner, "Gitlab Runner", "")
            ' Container(nj, "helloworld", "application")
        }
    }
}
Boundary(gitlab, "gitlab.com/c2platform", $type="") {
    Boundary(njx, "examples/kubernetes/gitlab-docker-build", $type="") {
        Container(registry, "Registry", "docker")
        Container(pipeline, "Pipeline", ".gitlab-ci.yml")
        Container(repo, "Repository", "git")
    }
}

Rel(engineer, repo, "Push code changes", "")
Rel(engineer, pipeline, "Create release \nor deploy", "")
Rel_Right(repo, pipeline, "Trigger", "")
Rel(pipeline, runner, "Docker  \nbuild", "")
Rel(runner, registry, "Push \nimage", "")
{{< /plantuml >}}

## Create Kubernetes cluster

Create local Kubernetes cluster on `c2d-ks`.

* {{< external-link url="https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes.md" text="How-to Kubernetes" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-k8s-dashboard.md" text="How-to Kubernetes Dashboard" htmlproofer_ignore="false" >}} ( optional but recommended)

## Install GitLab Runner

In this project **Shared Runners** are of course turned off. The stages / jobs defined in {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-docker-build/-/blob/master/.gitlab-ci.yml" text=".gitlab-ci.yml" htmlproofer_ignore="false" >}} in this project will be delegated to Kubernetes running locally which we can do by installing a **GitLab Runner** inside the cluster.

* {{< external-link url="https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes-gitlab-runner.md" text="How-to Install Gitlab Runner" htmlproofer_ignore="false" >}}

## Run pipeline

Navigate to {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-docker-build/-/pipelines" text="gitlab-docker-build pipelines" htmlproofer_ignore="false" >}} and select **Run pipeline** to run the pipeline and create and release a Docker image.

The following jobs are defined:

* Build: Builds the Docker image for the application
* Prepare: Prepares the environment for the pipeline
* Create release: Creates a release for the application (manual)
* Push latest: Pushes the Docker image with the latest tag to the registry
* Push tag: Pushes the Docker image with the tag name to the registry
* deploy: Deploys the application to production (manual)
* Test: Runs automated tests on the application
* sast: Runs security tests on the application

---


<!--
id | 44143434
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/examples/kubernetes/gitlab-docker-build" data-proofer-ignore>c2platform/examples/kubernetes/gitlab-docker-build&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
