---
categories: GitLab Projects
description: ""
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: gitlab-gitops
tags: []
title: "GitLab GitOps workflow for Kubernetes"
weight: 11
---

GitLab: <a href="https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops"><code>c2platform/examples/kubernetes/gitlab-gitops</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


This project contain configuration that allows you to register a GitLab Agent for this project. See {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops/-/blob/master/.gitlab/agents/c2d-mk8s/config.yaml" text=".gitlab/agents/c2d-mk8s/config.yaml" htmlproofer_ignore="false" >}}. It also contains manifests files for Kubernetes that create two namespaces with a simple application. See {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops/-/blob/master/manifests/" text="manifests" htmlproofer_ignore="false" >}}.

This project was created to be used by the project {{< external-link url="https://gitlab.com/c2platform/ansible" text="c2platform/ansible" htmlproofer_ignore="false" >}} see {{< external-link url="https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes-gitlab-gitops.md" text="How-to GitLab GitOps workflow for Kubernetes" htmlproofer_ignore="false" >}}.



---


<!--
id | 44071435
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops" data-proofer-ignore>c2platform/examples/kubernetes/gitlab-gitops&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
