---
categories: GitLab Projects
description: "None"
#icon: fa-brands fa-square-gitlab
icon: fa-brands fa-git-alt
linkTitle: gitlab-robot
tags: []
title: "Run Robot tests on Kubernetes using GitLab"
weight: 12
---

GitLab: <a href="https://gitlab.com/c2platform/examples/kubernetes/gitlab-robot"><code>c2platform/examples/kubernetes/gitlab-robot</code>&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>


This project demonstrates how we can run {{< external-link url="https://robotframework.org/" text="Robot Framework" htmlproofer_ignore="false" >}} tests on  {{< external-link url="https://microk8s.io/" text="MicroK8s" htmlproofer_ignore="false" >}} Kubernetes cluster running on `c2d-ks1` using **GitLab Runner** running in the cluster. In this project the **GitLab Runner** is installed in the namespace `nja` where the `nj` "helloworld" application is running. This allows us to create tests that use internal endpoints. For example {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-robot/-/blob/master/test/chrome.robot" text="test/chrome.robot" htmlproofer_ignore="false" >}} has two tests, one using the internal endpoint, one using the external endpoint.

- [Overview](#overview)
- [Create Kubernetes cluster](#create-kubernetes-cluster)
- [Deploy hellworld app](#deploy-hellworld-app)
- [Install GitLab Runner](#install-gitlab-runner)
- [Run test](#run-test)

## Overview

{{< plantuml id="crc-remote" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(engineer, "Engineer")
'Person(user, "User")

Boundary(local, "local", $type="high-end dev laptop") {
    Container(c2d_rproxy1, "Reverse Proxy", "c2d-rproxy1")
    Boundary(c2d_ks, "Kubernetes", $type="c2d-ks1") {
        Boundary(c2d_k8s_nja, "nja", $type="namespace") {
            Container(runner, "Gitlab Runner", "")
            Container(nj, "helloworld", "application")
        }
    }
}
Boundary(gitlab, "gitlab.com/c2platform", $type="") {
    Boundary(njx, "examples/kubernetes/gitlab-robot", $type="") {
        Container(repo, "Repository", "git")
        Container(pipeline, "Pipeline", "")
    }
    Boundary(robot_project, "docker/robot", $type="") {
        Container(robot_registry, "Registry", "registry")
    }

}

Rel(engineer, repo, "Push test code", "")
Rel(engineer, c2d_rproxy1, "")
Rel(engineer, pipeline, "Run tests")
Rel(c2d_rproxy1, nj, "frontend-nja.k8s.c2platform.org")
Rel_Left(repo, pipeline, "Trigger", "")
Rel(pipeline, runner, "Test", "stage")
Rel(runner, nj, "Test requests", "robot")
'Rel_Down(user, nj, "", "")
Rel_Left(runner, robot_registry, "Pull robot \nimage", "")
{{< /plantuml >}}

## Create Kubernetes cluster

Create local Kubernetes cluster on `c2d-ks`.

* {{< external-link url="https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes.md" text="How-to Kubernetes" htmlproofer_ignore="false" >}}
* {{< external-link url="https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-k8s-dashboard.md" text="How-to Kubernetes Dashboard" htmlproofer_ignore="false" >}} ( optional but recommended)

## Deploy hellworld app

* See project {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-gitops" text="GitLab GitOps workflow for Kubernetes" htmlproofer_ignore="false" >}}

## Install GitLab Runner

In this project **Shared Runners** are of course turned off. The {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-robot/-/blob/master/.gitlab-ci.yml" text=".gitlab-ci.yml" htmlproofer_ignore="false" >}} in this project only contains one stage and job, which we want to delegate to Kubernetes running locally which we can do by installing a **GitLab Runner** inside the cluster.

* {{< external-link url="https://gitlab.com/c2platform/ansible/-/blob/master/doc/howto-kubernetes-gitlab-runner.md" text="How-to Install Gitlab Runner" htmlproofer_ignore="false" >}}

## Run test

Navigate to {{< external-link url="https://gitlab.com/c2platform/examples/kubernetes/gitlab-robot/-/pipelines" text="gitlab-robot pipelines" htmlproofer_ignore="false" >}} and select **Run pipeline** to start the Robot tests.


---


<!--
id | 44166225
default_branch | master
web_url | <a href="https://gitlab.com/c2platform/examples/kubernetes/gitlab-robot" data-proofer-ignore>c2platform/examples/kubernetes/gitlab-robot&nbsp;<i class="fa-regular fa-arrow-up-right-from-square"></i></a>
-->
