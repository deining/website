---
categories: ["Example"]
tags: [rws, odm]
title: "Driving Innovation: RWS's Open Approach to Ansible Automation"
linkTitle: "ODM"
weight: 6
description: >
    Rijkswaterstaat embraces an open approach to Ansible engineering, promoting the reuse of ideas and code. This enables increased flexibility and productivity, particularly in the open source/internet domain.
---

For more information on this approach, refer to [ODM / OSS]({{< relref path="/docs/concepts/oss/" >}}). This page provides a comprehensive explanation of how this approach is used to manage the **Rijkswaterstaat (RWS) GIS Platform**. It covers three distinct Ansible disciplines, accompanied by text and diagrams:

## Ansible engineering

This discipline focuses on creating reusable fundamental automation building blocks through [Ansible Collections]({{< relref path="/docs/concepts/ansible/projects/collections" >}}) and [Ansible Roles]({{< relref path="/docs/concepts/ansible/projects/roles" >}}). These activities primarily take place in the open source/internet domain, maximizing flexibility and productivity. The configuration/playbook project for this [reference implementation]({{< relref path="/docs/concepts/examples/ri/" >}}) is the [`c2platform/rws/ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis/" lang="en" >}}) project. This project enables the local deployment of a complete and functional environment of the RWS GIS Platform. For this purpose, the project utilizes the Ansible collections [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis/" lang="en" >}}) and [`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore/" lang="en" >}}). These projects are public, open-source projects that are part of the [GitLab Open Source Program]({{< relref path="/docs/concepts/gitlab" >}}).

The diagram below illustrates the provisioning of a local instance of the RWS GIS Platform by an open-source Ansible engineer using [Vagrant]({{< relref path="/docs/concepts/dev/vagrant/" >}}). The example showcases four local nodes:

1. `gsd-rproxy1`: An Apache2 instance running in an [LXD]({{< relref path="/docs/concepts/dev/lxd" >}}) container. For more information on this type of node, refer to the guide on [Setting up a Reverse Proxy and CA server]({{< relref path="/docs/howto/c2/reverse-proxy" >}}).
2. `gsd-agwat1`,`gsd-adserver1`, `gsd-agportal1`: These represent ArcGIS Webadapter, ArcGIS Server, and ArcGIS Portal, respectively.

The engineer manages the GitLab projects stored in the {{< external-link url="https://gitlab.com/c2platform/rws/" text="c2platform/rws/" htmlproofer_ignore="false" >}} folder on {{< external-link url="https://gitlab.com" text="gitlab.com" htmlproofer_ignore="false" >}} by pushing and pulling changes.

Additionally, the [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis/" lang="en" >}}) and [`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore/" lang="en" >}}) projects are periodically pushed to the {{< external-link url="https://dev.azure.com/Rijkswaterstaat" text="RWS Azure DevOps Namespace" htmlproofer_ignore="true" >}} to facilitate development on the {{< external-link url="https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html#control-node" text="Ansible Control Node" htmlproofer_ignore="false" >}}. Refer to [Ansible ad-hoc]({{< relref path="#ansible-ad-hoc" >}}) for more details.

Open-source projects that are part of the [GitLab Open Source Program]({{< relref path="/docs/concepts/gitlab" >}}) benefit from free and unlimited CI/CD workloads. As a result, the [`c2platform.gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-gis/" lang="en" >}}) and [`c2platform.wincore`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-collection-wincore/" lang="en" >}}) projects include a CI/CD pipeline that publishes to the [Galaxy]({{< relref path="/docs/concepts/ansible/galaxy" >}}) website.

{{< plantuml id="rws-ansible-engineering" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Person(ok, "Ansible Engineer")

Boundary(local, "local", $type="high-end dev laptop") {
    Container(git_gis_local, "ansible-gis\nansible-collection-gis\nansible-collection-wincore", "git repo")
    Boundary(gis_local, "gis platform", $type="") {
        Container(gsd_rproxy1, "gsd-rproxy1", "lxd")
        Container(gsd_agserver1, "gsd-agserver1", "virtualbox")
        Container(gsd_agwa1, "gsd-agwa1", "virtualbox")
        Container(gsd_agportal1, "gsd-agportal1", "virtualbox")
    }
    Container(vagrant, "Vagrant", "")
}

Boundary(internet, "internet", $type="") {
    Boundary(rws_saas, "rws", $type="saas") {
        Boundary(azure_devops, "dev.azure.com/Rijkswaterstaat", $type="") {
            Container(git_gis_rws, "nansible-collection-gis\nansible-collection-wincore", "git repo")
        }
    }
    Boundary(oss, "oss", $type="saas") {
        Boundary(gitlab, "gitlab.com/c2platform/rws", $type="") {
            Container(git_gis, "ansible-gis\nansible-collection-gis\nansible-collection-wincore", "git repo")
        }
        Boundary(galaxy, "galaxy.ansible.com/c2platform", $type="") {
            Container(galaxy_collections, "c2platform.gis\nc2platform.wincore", "galaxy collection")
        }
    }
}


Rel(ok, git_gis_local, "Push &\npull changes", "")
Rel(ok, vagrant, "Vagrant up\nprovision\ndestroy", "")
Rel_Right(vagrant, git_gis_local, "", "")
Rel_Down(vagrant, gis_local, "Ansible\nprovision", "")
Rel_Right(git_gis_local, git_gis, "Push & pull\nremote changes", "")
Rel_Right(git_gis, git_gis_rws, "Push & pull\nremote changes", "")

Rel(gsd_rproxy1, gsd_agwa1, "", "https")
Rel(gsd_agwa1, gsd_agserver1, "", "https")
Rel(gsd_agwa1, gsd_agportal1, "", "https")

Rel_Down(git_gis, galaxy_collections, "Release", "gitlab-pipeline")
{{< /plantuml >}}

## Ansible configuration

This discipline involves the creation of an Ansible configuration project, commonly referred to as an Ansible playbook project. It encompasses inventory, plays, and environment configurations for the RWS GIS Platform environments located within the RWS data center. It is important to note that this discipline differs significantly from [Ansible engineering]({{< relref path="#ansible-engineering" >}}). Therefore, the primary role involved is that of an **Ansible User** rather than an **Ansible Engineer**.

The tasks involved in this discipline are generally straightforward and can be accomplished using web interfaces: the {{< external-link url="https://www.ansible.com/products/automation-platform" text="Ansible Automation Platform (AAP)" htmlproofer_ignore="false" >}} and the **Azure Devops** web interface. However, it is highly recommended to utilize [Visual Studio Code]({{< relref path="/docs/concepts/dev/vscode/" >}}) as the preferred tool for managing the Ansible configuration/playbook project.  This is especially true when dealing with larger and more complex projects such as {{< external-link url="https://dev.azure.com/Rijkswaterstaat/IVP-TB-Geo/_git/ansible-arcgis" text="ansible-arcgis" htmlproofer_ignore="true" >}} and its open-source upstream reference project [`ansible-gis`]({{< relref path="/docs/gitlab/c2platform/rws/ansible-gis/" lang="en" >}}), as navigating and managing them without an IDE can become challenging.

{{< plantuml id="rws-ansible-configuration" >}}
!include  https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

Boundary(internet, "internet", $type="") {
    Boundary(rws_saas, "rws", $type="saas") {
        Boundary(azure_devops, "dev.azure.com/Rijkswaterstaat", $type="") {
            Container(git_gis_rws_config, "ansible-arcgis", "git repo")
        }
    }
    Boundary(oss, "oss", $type="saas") {
        Boundary(galaxy, "galaxy.ansible.com/c2platform", $type="") {
            Container(galaxy_collections, "c2platform.gis\nc2platform.wincore", "galaxy collection")
        }
    }
}

Boundary(rws, "rws.nl", $type="dc") {
    Person(ansible_user, "Ansible User")
    Boundary(rws_lab, "gis platform", $type="lab") {
        Container(netscaler, "Netscaler", "")
        Container(agwebadaptor, "ArcGIS Webadaptor", "")
        Container(agserver, "ArcGIS Server", "")
        Container(agportal, "ArcGIS Portal")
    }
    Boundary(rws_mngt, "management", $type="") {
        Container(rws_ansible, "Ansible", "ansible-dev-control-node")
        Container(rws_aap, "AAP", "ansible-control-node")
    }
}

Rel(netscaler, agwebadaptor, "", "https")
Rel(agwebadaptor, agserver, "", "https")
Rel(agwebadaptor, agportal, "", "https")

Rel_Right(rws_aap, galaxy_collections, "Download Ansible\nroles & collections", "https")
Rel_Down(rws_aap, git_gis_rws_config, "Download\nGIS Platform\nconfiguration", "https")
Rel_Down(rws_aap, rws_lab, "Provision", "winrm\nssh")

Rel_Down(ansible_user, rws_aap, "Start / configure\nJob templates", "https")
Rel_Down(ansible_user, rws_ansible, "", "rdp\nssh")

Rel_Down(rws_ansible, git_gis_rws_config, "Push & pull\nconfig changes", "https")

{{< /plantuml >}}

## Ansible ad-hoc

This discipline revolves around utilizing Ansible on an {{< external-link url="https://docs.ansible.com/ansible/latest/network/getting_started/basic_concepts.html#control-node" text="Ansible Control Node" htmlproofer_ignore="false" >}} to perform ad-hoc technical maintenance tasks using Ansible. See also [How-to: Setup Ansible Control Node]({{< relref path="/docs/howto/rws/control-node" >}}).
