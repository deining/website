import os
import re

def replace_include_md():
    include_start = "<!-- include-start:"
    include_end = "<!-- include-end -->"
    content_dir = os.path.join(os.getcwd(), "content")
    include_dir = os.path.join(os.getcwd(), "scripts", "md-snippets")

    for root, _, files in os.walk(content_dir):
        for file in files:
            # print('file'+file)
            if file.endswith(".md"):
                include_file = os.path.join(root, file)
                with open(include_file, "r") as f:
                    include_content = f.readlines()

                filepath = os.path.join(root, file)
                with open(filepath, "r") as f:
                    lines = f.readlines()

                new_lines = []
                include_line = None

                for line in lines:
                    if include_start in line:
                        include_line = line
                        print('include_dir'+include_dir)
                        print('filename'+extract_include_file(line))
                        include_file = os.path.join(include_dir, extract_include_file(line))
                        print('include_file'+include_file)
                        if include_file:
                            include_content = read_include_file(include_file)
                        continue
                    if include_end in line:
                        if include_line is not None:
                            new_lines.append(include_line)
                            new_lines.extend(include_content)
                            new_lines.append(line)
                        include_line = None
                        continue
                    if include_line is None:
                        new_lines.append(line)

                with open(filepath, "w") as f:
                    f.writelines(new_lines)

def extract_include_file(line):
    match = re.search(r"<!-- include-start: (.*) -->", line)
    if match:
        return match.group(1).strip()
    else:
        return None

def read_include_file(include_file):
    with open(include_file, "r") as f:
        return f.readlines()

# Run the script
replace_include_md()
