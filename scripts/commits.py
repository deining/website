import os
import csv
import datetime
import yaml
from git import Repo

def get_author_alias(author, author_aliases):
    return author_aliases.get(author, author)

def is_documentation_file(file_path):
    return file_path.endswith(".md")

def calculate_semsprint(commit_date):
    year = commit_date.year
    week_number = commit_date.strftime('%U')
    sequence_number = (int(week_number) + 1) // 2  # Adjust for 0-based indexing
    semsprint = f"{year}-{sequence_number:02d}"
    return semsprint

def calculate_size_indicator(files_modified, lines_modified):
    weight_files = 0.8
    weight_lines = 0.2
    size_indicator = (files_modified * weight_files) + (lines_modified * weight_lines)
    return size_indicator

def get_common_path(file_paths):
    if not file_paths:
        return "No Files Modified"
    elif len(file_paths) == 1:
        return file_paths[0]
    else:
        common_prefix = os.path.commonprefix(file_paths)
        return common_prefix

def write_csv_header(csv_filename):
    with open(csv_filename, mode='w', newline='') as csv_file:
        fieldnames = [
            'Repo URL', 'Repo Name', 'Commit Hash', 'Date', 'Author', 'Year', 'Month',
            'Week', 'Day', 'Sprint', 'Common Code Path',
            'Code Files Modified', 'Code Lines Modified', 'Code Size Indicator',
            'Common Doc Path',
            'Doc Files Modified', 'Doc Lines Modified', 'Doc Size Indicator', 'Doc', 'Code'
        ]
        writer = csv.writer(csv_file)
        writer.writerow(fieldnames)

def write_csv_row(csv_filename, row):
    with open(csv_filename, mode='a', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(row)

def commit_stats(commit):
    doc = False
    code = False
    doc_files_count = 0
    doc_lines_modified = 0
    code_files_count = 0
    code_lines_modified = 0

    for file in commit.stats.files:
        if file.endswith('.md'):
            doc = True
            doc_files_count += 1
            doc_lines_modified += commit.stats.files[file]['lines']
        else:
            code = True
            code_files_count += 1
            code_lines_modified += commit.stats.files[file]['lines']

    return doc, code, doc_files_count, doc_lines_modified, code_files_count, code_lines_modified

def process_repository(repo_url, output_dir, author_aliases, author_aliases_ignored, author_aliases_unknown, csv_filename, sprints, doc_repos):
    repo_name = repo_url.split('/')[-1].split('.')[0]
    repo_path = os.path.join(output_dir, repo_name)
    if not os.path.exists(repo_path):
        Repo.clone_from(repo_url, repo_path)

    repo = Repo(repo_path)

    for commit in repo.iter_commits():
        author = commit.author.name
        if author not in author_aliases:
            if author not in author_aliases_ignored:
                if author not in author_aliases_unknown:
                    author_aliases_unknown.append(author)
            continue

        commit_date = datetime.datetime.fromtimestamp(commit.committed_date)
        author = get_author_alias(commit.author.name, author_aliases)
        year = commit_date.year
        month = commit_date.month
        week = commit_date.strftime('%U')
        day = commit_date.strftime('%A')
        semsprint = calculate_semsprint(commit_date)

        if semsprint not in sprints:
            continue  # Skip processing this commit

        if commit.parents:
            code_file_paths = [item.a_path for item in commit.diff(commit.parents[0]) if item.a_path is not None and not is_documentation_file(item.a_path)]
            doc_file_paths = [item.a_path for item in commit.diff(commit.parents[0]) if item.a_path is not None and is_documentation_file(item.a_path)]
        else:
            code_file_paths = []
            doc_file_paths = []

        doc, code, doc_files_count, doc_lines_modified, code_files_count, code_lines_modified = commit_stats(commit)
        doc_int = 0
        code_int = 0
        if doc:
            doc_int  = 1
        if code:
            code_int = 1

        # doc repo have only doc changes
        if repo_url in doc_repos:
            # doc_file_paths += code_file_paths
            # doc_files_count += code_files_count
            # doc_lines_modified += code_lines_modified
            code_file_paths: []
            code_files_count = 0
            code_lines_modified = 0

        common_code_path = get_common_path(code_file_paths)
        common_doc_path = get_common_path(doc_file_paths)

        code_size_indicator =  int(round(calculate_size_indicator(code_files_count, code_lines_modified)))
        doc_size_indicator = int(round(calculate_size_indicator(doc_files_count, doc_lines_modified)))



        row = [
            repo_url, repo_name, commit.hexsha, commit_date, author, year, month, week, day, semsprint,
            common_code_path, code_files_count, code_lines_modified, code_size_indicator,
            common_doc_path, doc_files_count, doc_lines_modified, doc_size_indicator, doc_int, code_int
        ]

        write_csv_row(csv_filename, row)

if __name__ == "__main__":
    with open("scripts/commits.yml", "r") as yaml_file:
        config = yaml.load(yaml_file, Loader=yaml.FullLoader)

    author_aliases = config.get('author_aliases', {})
    sprints = config.get('sprints', {})
    author_aliases_ignored = config.get('author_aliases_ignored', {})
    output_dir = config.get('output_dir', './')
    csv_filename = "scripts/commits.csv"
    author_aliases_unknown = []

    write_csv_header(csv_filename)

    repositories = config.get('repositories', [])
    doc_repos = config.get('doc_repos', [])
    for repo_url in repositories:
        process_repository(repo_url, output_dir, author_aliases, author_aliases_ignored, author_aliases_unknown, csv_filename, sprints, doc_repos)

    if author_aliases_unknown:
        print("Unknown Authors:", author_aliases_unknown)
