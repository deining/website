## Prerequisites

Before setting up the **RWS Development Environment**, it's important to have the C2
Development Environment ready.

* [C2 Dev Environment Setup Guide]({{< relref path="/docs/howto/dev-environment/setup" >}}).
